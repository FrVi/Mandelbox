# Mandelbox

<br />
<img src="https://gitlab.com/FrVi/Mandelbox/raw/master/images/mandelboxbulb.png" alt="Drawing" width="512"/>
<br />

This program allows exploration of the Mandelbox and Mandelbulb sets.

The first version of it was made in Java by Hamza Chouh and I as our software project at Supelec in 2011.<br />

This version was done by me in C++/CUDA for the rendering project of Supelec (2011-2012).<br />

<br />
## Principle

The Mandelbox and Mandelbulb sets are 3D fractal objects inspired by the 2D classical Mandelbrot set.

The easiest way to render them is using raycasting as a distance estimator can be derived from the set membership test.

<br />
## The Program

It allows customization of the fractal parameters (menu are done with Qt).

Exploration can be done by left clicking on the object or using the menus.

Computation uses CPU multithreading or GPGPU depending on the mode chosen.

<br />
