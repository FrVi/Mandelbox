#ifndef INFPANEL3_H
#define INFPANEL3_H

#include <QHBoxLayout>

#include "VideoPanel.h"

class InfPanel3 : public QWidget
{
public:
	InfPanel3(Window* window);

private:
	VideoPanel videoPanel;
	QHBoxLayout layout;

};

#endif//INFPANEL3_H
