#include "Onglets.h"
#include "WindowConst.h"
#include "WindowConst2.h"

Onglets::Onglets(PointOfView *pov, FractalView *fview, Window* window) :infPanel(pov), infPanel2(pov, fview), infPanel4(fview), infPanel3(window), infPanel5(fview)
{
	setMinimumSize(windows_x_ini, 310);
	setMaximumSize(windows_x_ini, 310);

	setAutoFillBackground(true);
	setPalette(QPalette(background_color));

	addTab(&infPanel, "Standard");
	addTab(&infPanel2, "Advanced");
	addTab(&infPanel3, "Video");
	addTab(&infPanel4, "Color");
	addTab(&infPanel5, "Mode");
}

void Onglets::update()
{
	infPanel2.update();
}

void Onglets::setFractalView(FractalView *fview)
{
	infPanel2.setFractalView(fview);
	infPanel4.resetColors();
}
