#include "FractalThread.h"
#include "FractalView.h"
#include "KernelConst.h"

void Semaphore::swap1()
{
	idX = 1 - idX;
}

void Semaphore::swap2()
{
	idX2 = 1 - idX2;
}

void Semaphore::closeThreads()
{
	for (int i = 0; i < thread_number_max; i++)
	{
		normalThreads[i].end();
		highThreads[i].end();
	}
}

void Semaphore::set(FractalView *fview)
{

	this->fview = fview;
	fview->setSemaphore(this);

	//Initialisation des threads et semaphores.
	normalThreads = new NormalThread[thread_number_max];
	highThreads = new HighThread[thread_number_max];

	for (int i = 0; i < thread_number_max; i++)
	{
		normalThreads[i].set(fview, i/*,perfMon*/);
		highThreads[i].set(fview, i/*,perfMon*/);
	}

	//On demarre sur le groupe de semaphores 0.
	idX = 0;
	idX2 = 0;
	//Lancement des threads.
	for (int i = 0; i < thread_number_max; i++)
	{
		normalThreads[i].start();
		highThreads[i].start();
	}
}


Semaphore::Semaphore()
{
	idX2 = 0;
	idX = 0;
}

void Semaphore::destruct()
{
	delete[] normalThreads;
	delete[] highThreads;
}

Semaphore::~Semaphore()
{
	destruct();
}

void Semaphore::relNormalSemaEnd(int idX)
{
	sNormalEnd[idX].release();
}

void Semaphore::acqNormalSemaStart(int idX)
{
	sNormalStart[idX].acquire();
}

void Semaphore::acqNormalSemaEnd()
{
	sNormalEnd[idX].acquire(thread_number_max);
}

void Semaphore::relNormalSemaStart()
{
	sNormalStart[idX].release(thread_number_max);
}

void Semaphore::relHighSemaEnd(int idX)
{
	sHighEnd[idX].release();
}

void Semaphore::acqHighSemaStart(int idX)
{
	sHighStart[idX].acquire();
}

void Semaphore::acqHighSemaEnd()
{
	sHighEnd[idX2].acquire(thread_number_max);
}

void Semaphore::relHighSemaStart()
{
	sHighStart[idX2].release(thread_number_max);
}

FractalThread::FractalThread()
{

}

void FractalThread::set(FractalView *fView, int i)
{
	this->i = i;
	this->fView = fView;
}

void FractalThread::end()
{
	continueTh = false;
}

void NormalThread::run()
{
	continueTh = true;
	int idX = 0; //On commence a travailler sur le premier groupe de semaphores.
	while (continueTh)
	{
		fView->acqNormalSemaStart(idX); //Obtention d'un jeton.
		if (continueTh && i < fView->getThread_numbers()){ fView->work(i); } //Travail.
		fView->relNormalSemaEnd(idX); //On rend un jeton.
		idX = 1 - idX; //On change de groupe de semaphores.
	}
}

void HighThread::run()
{
	continueTh = true;
	int idX = 0; //On commence a travailler sur le premier groupe de semaphores.
	while (continueTh)
	{
		fView->acqHighSemaStart(idX); //Obtention d'un jeton.
		if (continueTh && i < fView->getThread_numbers())
		{
			fView->highQualityWork(i);
		} //Travail.
		fView->relHighSemaEnd(idX); //On rend un jeton.
		idX = 1 - idX; //On change de groupe de semaphores.
	}
}
