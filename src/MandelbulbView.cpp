#include "MandelbulbView.h"	
#include <math.h>	
#include "PointOfView.h"	
#include "KernelPBO.h"

MandelbulbView::MandelbulbView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon) :FractalView(pov, screen, perfMon)		/*, PerfMonitor perfMon*/
{
	setS(8.0);//appelle define();
}

int MandelbulbView::getIncrement(const Coord &P, Coord &P2, int i)
{
	return (this->*function)(P, i);
}

//permet de recourir a des expressions optimisee (4 a 10 fois plus rapides)
//expressions sans trigonometrie 
//et sans racines pour les ordres impaires
void MandelbulbView::define()
{
	int t = (int)s;

	if (abs(((double)t) - s) > 0.01)
	{
		function = &MandelbulbView::getIncrementGenerique;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionGenerique;
		GPUfunctionHD = &MandelbulbView::GPUFunctionGeneriqueHD;
#endif
		return;
	}

	switch (t)
	{
	case 8:
	{
		function = &MandelbulbView::getIncrementHuit;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionHuit;
		GPUfunctionHD = &MandelbulbView::GPUFunctionHuitHD;
#endif
		break;
	}
	case 7:
	{
		function = &MandelbulbView::getIncrementSept;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionSept;
		GPUfunctionHD = &MandelbulbView::GPUFunctionSeptHD;
#endif
		break;
	}
	case 5:
	{
		function = &MandelbulbView::getIncrementCinq;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionCinq;
		GPUfunctionHD = &MandelbulbView::GPUFunctionCinqHD;
#endif
		break;
	}
	case 4:
	{
		function = &MandelbulbView::getIncrementQuatre;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionQuatre;
		GPUfunctionHD = &MandelbulbView::GPUFunctionQuatreHD;
#endif
		break;
	}
	case 2:
	{
		function = &MandelbulbView::getIncrementDeux;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionDeux;
		GPUfunctionHD= &MandelbulbView::GPUFunctionDeuxHD;
#endif
		break;
	}
	default:
	{
		function = &MandelbulbView::getIncrementGenerique;
#ifdef USE_CUDA
		GPUfunction = &MandelbulbView::GPUFunctionGenerique;
		GPUfunctionHD = &MandelbulbView::GPUFunctionGeneriqueHD;
#endif
		break;
	}
	}
}

int MandelbulbView::getIncrementGenerique(const Coord &P,/*Coord &P2,*/ int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R = 0.0;

	double Rq;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double theta;
	double phi;

	while (incr < incrLimit)
	{
		R = sqrt(x*x + y*y + z*z);

		if (R > L)
		{
			break;
		}

		Rq = pow(R, s - 1);//R^(q-1)

		DEfactor = s*Rq*DEfactor + 1.0;

		Rq *= R;//R^(q)

		theta = acos(z / R);
		phi = atan2(y, x);

		theta *= s;
		phi *= s;

		x = Rq * sin(theta)*cos(phi) + x0;
		y = Rq * sin(theta)*sin(phi) + y0;
		z = Rq * cos(theta) + z0;

		incr++;
	}

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}

int MandelbulbView::getIncrementDeux(const Coord &P, int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R = 0.0;

	double Rq;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2, y2, z2;

	double tmp;

	while (incr < incrLimit)
	{
		Rq = x*x + y*y + z*z;
		R = sqrt(Rq);

		if (R > L)
		{
			break;
		}

		DEfactor = 2 * R*DEfactor + 1.0;


		x2 = x*x;
		y2 = y*y;
		z2 = z*z;

		tmp = 1 / sqrt(x2 + y2);

		y = 4 * x*y*z*tmp + y0;
		x = -2 * z *(y2*(y2 + z2) - x2*(x2 + z2))*tmp / Rq + x0;

		z = z2 - y2 - x2 + z0;

		incr++;
	}

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}

int MandelbulbView::getIncrementQuatre(const Coord &P, int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R = 0.0;

	double Rq;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;
	double z2;

	double k4, k3, k2, k1;

	while (incr < incrLimit)
	{
		R = sqrt(x*x + y*y + z*z);

		if (R > L)
		{
			break;
		}


		Rq = R*R*R;
		DEfactor = 4 * Rq*DEfactor + 1.0;


		x2 = x*x;
		y2 = y*y;
		z2 = z*z;

		k1 = x2 + y2;
		k4 = -4 * (k1 - z2)*z / pow(k1, 1.5);
		k3 = x2 - y2;
		k2 = 2 * x*y;

		y = 2 * k3*k2*k4 + y0;
		x = (k3 + k2)*(k3 - k2)*k4 + x0;
		z = k1*(k1 - 6 * z2) + z2*z2 + z0;

		incr++;
	}

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}

int MandelbulbView::getIncrementCinq(const Coord &P, int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R2 = 0.0;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;
	double z2;

	double k6, k5, k4, k3, k2, k1;

	while (incr < incrLimit)
	{
		R2 = x*x + y*y + z*z;

		if (R2 > L*L)
		{
			break;
		}

		DEfactor = 5 * R2*R2*DEfactor + 1.0;

		x2 = x*x;
		y2 = y*y;
		z2 = z*z;

		k4 = x2 + y2;
		k6 = k4*k4;

		k1 = k6 + 5 * z2*(z2 - 2 * k4);
		k2 = 4 * R2*(k4 - z2);
		k3 = k6 + 4 * y2*(y2 - 3 * x2);
		k5 = k1 / k6;

		x = x*k3*k5 + x0;
		y = y*(k3 + 4 * k4*(x2 - y2))*k5 + y0;
		z = z*(k1 + k2) + z0;

		incr++;
	}

	double R = sqrt(R2);

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}

int MandelbulbView::getIncrementSept(const Coord &P, int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R2 = 0.0;


	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;
	double z2;

	double k1, k2, k3, k4;

	while (incr < incrLimit)
	{
		R2 = x*x + y*y + z*z;

		if (R2 > L*L)
		{
			break;
		}

		DEfactor = 7 * R2*R2*R2*DEfactor + 1.0;

		x2 = x*x;
		y2 = y*y;
		z2 = z*z;



		k2 = x2 + y2;
		k3 = k2*k2;
		k4 = k3*k2;
		k1 = 7 * z2*((k2 * 5 - z2)*z2 - k3 * 3) / k4 + 1;

		x = x*(7 * y2*(3 * x2*x2 + y2*(y2 - 5 * x2)) - x2*x2*x2)*k1 + x0;
		y = y*(y2*y2*y2 - 7 * x2*(3 * y2*y2 + (x2 - 5 * y2)*x2))*k1 + y0;
		z = z*(((z2 - 21 * k2)*z2 + 35 * k3)*z2 - 7 * k4) + z0;




		incr++;
	}

	double R = sqrt(R2);

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}

int MandelbulbView::getIncrementHuit(const Coord &P, int i)
{
	double x0 = P.get_x() / 10.0;
	double y0 = P.get_y() / 10.0;
	double z0 = P.get_z() / 10.0;

	double R = 0.0;

	double R8;
	double R2;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double x4;
	double y2;
	double y4;
	double z2;
	double z4;

	double k3;
	double k2;
	double k1;
	double k4;

	while (incr < incrLimit)
	{
		R2 = x*x + y*y + z*z;
		R = sqrt(R2);

		if (R > L)
		{
			break;
		}

		R8 = R2*R2;//R4 pour le moment
		DEfactor = 8 * R8*R2*R*DEfactor + 1.0;

		R8 = R8*R8;//R8 maintenant

		x2 = x*x;
		x4 = x2*x2;
		y2 = y*y;
		y4 = y2*y2;
		z2 = z*z;
		z4 = z2*z2;

		k3 = y2 + x2;

		k2 = k3*k3;
		k2 = k2*k2*k2*k3; // k3^7
		k2 = 1 / sqrt(k2);
		k1 = z4 + k3*(k3 - 6.0*z2);
		k4 = z*(k3 - z2);

		y = 64.0*y*x*(y2 - x2)*k4*(y4 - 6.0*y2*x2 + x4)*k1*k2 + y0;
		x = -8.0*k4*k1*k2*(y4*y4 - 28.0*y2*x2*(y4 + x4) + (70.0*y4 + x4)*x4) + x0;
		z = -16.0*k3*k4*k4 + k1*k1 + z0;

		incr++;
	}

	distance[i] = 10 * 0.5 * R * log(R) / DEfactor;

	if (distance[i] < pov->getNearDist())
	{
		incr = incrLimit;
	}

	return incr;
}
