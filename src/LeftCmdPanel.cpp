#include "LeftCmdPanel.h"
#include "KernelConst.h"
#include "WindowConst.h"
#include "WindowConst2.h"

LeftCmdPanel::LeftCmdPanel(PointOfView *pov) :bouton1("images/top.png"), bouton2("images/left.png"), bouton3("images/right.png"), bouton4("images/bottom.png"), bouton5("images/front.png"), bouton6("images/back.png"), caseCocher("X3"), right(pov), left(pov), back(pov), front(pov), up(pov), down(pov), mvtSwitch(pov, fact)
{
	setMinimumSize(150, 150);
	setMaximumSize(150, 150);
	layout.setContentsMargins(0, 0, 0, 0);

	setAutoFillBackground(true);
	setPalette(QPalette(LeftPanelColor));
	layout.setSpacing(0);

	layout.addWidget(&caseCocher, 0, 0);
	layout.addWidget(&bouton1, 0, 1);
	layout.addWidget(&bouton2, 1, 0);
	layout.addWidget(&bouton3, 1, 2);
	layout.addWidget(&bouton4, 2, 1);
	layout.addWidget(&bouton5, 0, 2);
	layout.addWidget(&bouton6, 2, 0);

	setLayout(&layout);

	QObject::connect(&bouton1, SIGNAL(clicked()), &up, SLOT(actionPerformed()));
	QObject::connect(&bouton2, SIGNAL(clicked()), &left, SLOT(actionPerformed()));
	QObject::connect(&bouton3, SIGNAL(clicked()), &right, SLOT(actionPerformed()));
	QObject::connect(&bouton4, SIGNAL(clicked()), &down, SLOT(actionPerformed()));
	QObject::connect(&bouton5, SIGNAL(clicked()), &front, SLOT(actionPerformed()));
	QObject::connect(&bouton6, SIGNAL(clicked()), &back, SLOT(actionPerformed()));
	QObject::connect(&caseCocher, SIGNAL(toggled(bool)), &mvtSwitch, SLOT(actionPerformed()));

	bouton1.setShortcut(QKeySequence(Up));
	bouton2.setShortcut(QKeySequence(Left));
	bouton3.setShortcut(QKeySequence(Right));
	bouton4.setShortcut(QKeySequence(Down));
	bouton5.setShortcut(QKeySequence(Front));
	bouton6.setShortcut(QKeySequence(Back));
}
