#ifndef SCREEN_H
#define SCREEN_H

//#include <SDL/SDL.h>
//#undef _main

//#include "PointOfView.h"

#include "simplePBO.h"

#include <QGLWidget> 


#include <stdlib.h> 
#include <stdio.h>

#include <QApplication>

#include <QtGui>
#define NOMINMAX
#include <windows.h>
#include <QGLBuffer>

#include "KernelConst.h"

// for AppGLWidget::getGLError()
#ifndef GLERROR
#define GLERROR(e) case e: exception=#e; break;
#endif//



class PointOfView;
class Color;
class FractalView;

class Screen : public QGLWidget {

	Q_OBJECT // must include this if you use Qt signals/slots

public:
	Screen(PointOfView* pov, QWidget *parent = NULL);
	~Screen();
	void setPixel(int x, int y, int r, int g, int b);
	void resize22(int w, int h);
	void writeImage(std::string path, std::string type);


	void Screen::runCuda();
#ifdef USE_CUDA
	QGLBuffer* getPixelBuffer();
#endif//

	void setFview(FractalView* fview);

	void setMode(bool mode);

	void setPaintMode(bool gpu_mode);

	void setResizeMode(bool gpu_mode);

	void resizeGLScreen(int w, int h);

protected:

	void initializeGL();
	void paintGL();
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);
	QString getGLError();

private:
	PointOfView* pov;
	void display();
	void resizeGPU_GL(int w, int h);
	void resizeCPU_GL(int w, int h);

	void PixelDraw(unsigned int x, unsigned int y, Color &color);

	void GPUpaint();
	void CPUpaint();
	void Paint();

	void (Screen::*paintModePtr) (void);
	void (Screen::*resizeModePtr) (int, int);

	GLenum  glError;// error opengl enum type

#ifdef USE_CUDA
	SimplePBO spbo;// SimplePBO object as pixel buffer object manager
#endif//


	GLuint Nom;
	GLubyte *Texture;
};

#endif//SCREEN_H
