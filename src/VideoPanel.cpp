#include <sstream>
#include<iostream>
#include <QProgressDialog>
#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include "VideoPanel.h"
#include "WindowConst.h"
#include "Window.h"
#include "WindowConst2.h"

VideoPanel::VideoPanel(Window *window) :bouton1("images/add.png"), bouton2("images/compute.png"), bouton3("images/reset.png"), bouton4("images/save.png"), bouton5("images/delete.png")
{
	this->window = window;
	setMinimumSize(150, 150);
	setMaximumSize(150, 150);
	layout.setContentsMargins(0, 0, 0, 0);

	setAutoFillBackground(true);
	setPalette(QPalette(CenterPanelColor));

	layout.setSpacing(0);

	layout.addWidget(&bouton1, 0, 0);
	layout.addWidget(&bouton2, 0, 2);
	layout.addWidget(&bouton3, 1, 1);
	layout.addWidget(&bouton4, 2, 0);
	layout.addWidget(&bouton5, 2, 2);

	setLayout(&layout);

	QObject::connect(&bouton1, SIGNAL(clicked()), this, SLOT(add()));
	QObject::connect(&bouton2, SIGNAL(clicked()), this, SLOT(compute()));
	QObject::connect(&bouton3, SIGNAL(clicked()), this, SLOT(reset()));
	QObject::connect(&bouton4, SIGNAL(clicked()), this, SLOT(save()));
	QObject::connect(&bouton5, SIGNAL(clicked()), this, SLOT(delete_pile()));

	bouton1.setShortcut(QKeySequence(Add));
	bouton2.setShortcut(QKeySequence(Compile));
	bouton3.setShortcut(QKeySequence(Reset_points));
}

void VideoPanel::reset()
{
	window->reset_pile();
}

void VideoPanel::add()
{
	window->add_pile();
}

void VideoPanel::delete_pile()
{
	window->delete_pile();
}

void VideoPanel::compute()
{
	int newTime = QInputDialog::getInt(this, "Time", "How many seconds of video?");

	if (newTime > 0)//pas de protection max...
	{
		window->pile_set_time(newTime);
		window->compute_pile();
	}
	else
	{
		QMessageBox::critical(this, "Error", "Negative or too big number");
	}
}

void VideoPanel::save()
{
	QString fichier = QFileDialog::getSaveFileName(this, "Save images as", QString(), "Images (*.bmp)");

	if (fichier.size() != 0)
	{
		fichier.remove(".bmp");

		std::string s;
		std::stringstream out;

		std::string name2;
		std::string name = toUtf8(fichier);


		QProgressDialog progress("Generating pictures...", "Abort", 0, window->pile_getSize(), this);

		progress.setWindowModality(Qt::WindowModal);

		for (int i = 0; i < window->pile_getSize(); i++)
		{
			progress.setValue(i);

			if (progress.wasCanceled()) break;
			//... generate one picture

			window->pile_setConfig(i);
			if (i < 10)
			{
				out.str("");
				out << name << "000" << i << ".bmp";
				name2 = out.str();
			}
			else if (i < 100)
			{
				out.str("");
				out << name << "00" << i << ".bmp";
				name2 = out.str();
			}
			else if (i < 1000)
			{
				out.str("");
				out << name << "0" << i << ".bmp";
				name2 = out.str();
			}
			else
			{
				out.str("");
				out << name << i << ".bmp";
				name2 = out.str();
			}

			window->ScreenWriteImage(name2, "bmp");
		}
	}
}
