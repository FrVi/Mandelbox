#ifndef doubleINFOSET_H
#define doubleINFOSET_H

#include <QHBoxLayout>
#include <QLabel>
#include <QDoubleSpinBox>

class DoubleInfoSet : public QWidget
{
public:
	//DoubleInfoSet(std::string name);
	DoubleInfoSet();
	void set_text(std::string name);
	double value();
	void set(double value);
private:
	QLabel nameLabel;
	QDoubleSpinBox infoText;
	QHBoxLayout layout;
};

#endif//doubleINFOSET_H
