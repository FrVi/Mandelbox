#include "Component.h"

Component::Component(Coord* coords, double *doubles, int *ints)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		this->coords[i] = coords[i];
	}

	for (i = 0; i < 8; i++)
	{
		this->doubles[i] = doubles[i];
	}

	for (i = 0; i < 10; i++)
	{
		this->ints[i] = ints[i];
	}
}

Component::Component(const Component& C)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		this->coords[i] = C.getCoord(i);
	}

	for (i = 0; i < 8; i++)
	{
		this->doubles[i] = C.getdouble(i);
	}

	for (i = 0; i < 10; i++)
	{
		this->ints[i] = C.getInt(i);
	}
}

void Component::set(int i, double val)
{
	if (i < 12)
	{
		int j = i / 3;
		coords[j].set(i - 3 * j, val);
	}
	else if (i < 20)
	{
		doubles[i - 12] = val;
	}
	else
	{
		ints[i - 20] = (int)val;
	}
}

Coord Component::getCoord(int i) const
{
	return Coord(coords[i]);
}

double Component::getdouble(int i) const
{
	return doubles[i];
}

int Component::getInt(int i) const
{
	return ints[i];
}

void Component::afficher(FILE* fichier)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		fprintf(fichier, "%.12lf %.12lf %.12lf ", coords[i].get_x(), coords[i].get_y(), coords[i].get_z());
	}

	for (i = 0; i < 8; i++)
	{
		fprintf(fichier, "%.12lf ", doubles[i]);
	}

	for (i = 0; i < 9; i++)
	{
		fprintf(fichier, "%ld ", ints[i]);
	}
	fprintf(fichier, "%ld\n", ints[9]);
}
