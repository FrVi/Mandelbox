#ifndef CUDAFRACTAL_H
#define CUDAFRACTAL_H

#include "Coord.h"
#include "PointOfView.h"

#ifdef USE_CUDA
void CPUaccessGPU(Coord* test, int i, Coord* tab);

//void CudaRecolorize(int screensize,Coord D2,Coord* tab_d,uchar3 *devPtr);

void GPUColorKernelLauncherHD(dim3 numBlocks2,dim3 threadsPerBlock2,Coord D2,Coord* tab_d,uchar3 *devPtr, Coord* normales,float* couleurs);
void GPUColorKernelLauncher(dim3 numBlocks2,dim3 threadsPerBlock2,Coord D2,Coord* tab_d,uchar3 *devPtr);

//http://msdn.microsoft.com/en-us/library/windows/hardware/ff569918%28v=vs.85%29.aspx
//http://social.technet.microsoft.com/Forums/en-US/w7itproinstall/thread/0cd5ec8b-df03-4560-952b-8c7d10e67241
//pour enlever l'erreur de GPU


//__global__ void testOrbit(Coord H, Coord V, Coord D, Coord pos, float mR2, float fR2mR2, float fR2, float L, float correction, Coord *tab, Coord *tab2);
__global__ void GPUMandelboxKernelHD(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__device__ int getIncrementMandelbox(float NearDist,const Coord &P, float &distance) ;
__global__ void	initia(Coord pos,Coord *positions, Coord *normales,Coord *horizontales,Coord *verticales,Coord *directions,Coord H, Coord V);
__global__ void GPUMandelboxKernelHD2(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void	GPUColorSettingKernel(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr,int dec_x, int dec_y);
__global__ void	GPURecolorizeKernelHD(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,uchar3 *devPtr,int dec_x, int dec_y);

__device__ int getIncrementMandelbulbHuit(float NearDist, const Coord &P, float &distance);
__device__ int getIncrementMandelbulbSept(float NearDist, const Coord &P, float &distance);
__device__ int getIncrementMandelbulbCinq(float NearDist, const Coord &P, float &distance);
__device__ int getIncrementMandelbulbQuatre(float NearDist, const Coord &P, float &distance);
__device__ int getIncrementMandelbulbDeux(float NearDist, const Coord &P, float &distance);
__device__ int getIncrementMandelbulbGenerique(float NearDist, const Coord &P, float &distance); 


//Function pointers and function template parameters are not supported in sm_1x
//oblige a multiplier le code...
//hors de question de faire un switch dnas une fonction utilisee des miliions de fois...

__global__ void	GPUColorSettingKernel2(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr,int dec_x, int dec_y);
void GPUColorSettingKernelLauncher(dim3 numBlocks2,dim3 threadsPerBlock2,Coord *normales_d,Coord *normales_d2,float *doubleTab_d,float *doubleTab2_d,Color *ColorTab_d,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr);
void GPUColorSettingKernelLauncher2(dim3 numBlocks2,dim3 threadsPerBlock2,Coord *normales_d,Coord *normales_d2,float *doubleTab_d,float *doubleTab2_d,Color *ColorTab_d,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr);

__device__ void setColorNorm(int i,const Coord &X, float c,const Color &Y0,const Color &Y1,const Color &Y2,const Color &Y3,const Color &Y4,uchar3 *devPtr);

__global__ void GPUMandelbulbKernelGenerique(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUMandelbulbKernelHuit(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUMandelbulbKernelSept(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUMandelbulbKernelCinq(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUMandelbulbKernelQuatre(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUMandelbulbKernelDeux(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);

__global__ void GPUMandelbulbKernelGeneriqueHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelbulbKernelHuitHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelbulbKernelSeptHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelbulbKernelCinqHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelbulbKernelQuatreHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelbulbKernelDeuxHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);

__device__ void set(uchar3 *devPtr,int i,int c1,int c2,int c3);
__device__ void pseudoOrth(Coord &C, float &c, int x, int y, Coord *tab1,Coord *tab2,Coord *tab3,Coord *tab4,Coord *tab5,Coord depthRef);
__device__ void setColorNorm(const Coord &X, float c, Color &Y);
__device__ void setColorNorm(int i,const Coord &X, float c,const Color &Y,uchar3 *devPtr);
__global__ void GPUMandelboxKernelHD2(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b);
__global__ void GPUMandelboxKernelHD(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab);
__global__ void GPUColorKernel(Coord D2, Coord* tab_d, uchar3* devPtr,int dec_x, int dec_y);
__global__ void GPUColorKernelHD(Coord D2, Coord* tab_d, uchar3* devPtr,int dec_x, int dec_y,Coord* normales,float *doubleTab);
__device__ int getIncrementMandelbox(float NearDist,const Coord &P, float &distance);

__global__ void GPUaccessKernel(Coord* test, int i, Coord* tab);
#endif//USE_CUDA
#endif//CUDAFRACTAL_H
