#ifndef ONGLETS_H
#define ONGLETS_H

#include "InfPanel.h"
#include "InfPanel2.h"
#include "InfPanel3.h"
#include "InfPanel4.h"
#include "InfPanel5.h"

#include <QTabWidget>

class Onglets : public QTabWidget
{
public:
	Onglets(PointOfView *pov, FractalView *fview, Window* window);
	void update();
	void setFractalView(FractalView *fview);

private:

	InfPanel infPanel;
	InfPanel2 infPanel2;
	InfPanel3 infPanel3;
	InfPanel4 infPanel4;
	InfPanel5 infPanel5;
};

#endif\\ONGLETS_H
