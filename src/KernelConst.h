#ifndef KERNELCONST_H
#define KERNELCONST_H

#include <math.h>
#include "Matrix3.h"

// Classe reunissant des constantes.

//Distance limite de visibilite. (Definition a revoir)
#define limitDist_ini 300.0		//50.0
#define nearDist_ini 0.01			//Distance limite de distinction entre deux pos.
#define incrLimit_ini  30			//50 	//Profondeur maximale de recursion.
#define tan35 0.700207538		//Tangente de l'angle 35 degre
#define sInit 2.0				//Constante liee au po de vue.
#define minRadiusInit 0.5		// Rayon d'observation minimal.
#define fixedRadiusInit 1.0	//Rayon d'observation.
#define lInit 10.0			//Limite d'observation.
#define thread_numbers_ini 2		//Nombre de threads.
#define thread_number_max 16	//Nombre de threads maximal autorise par le programme.
#define mvt_fact_ini 5.0			//Defini la distance de deplacement lors de l'utilisation d'un bouton de mouvement.
#define rot_fact_ini 1			//Defini l'angle de rotation lors de l'utilisation d'un bouton de rotation.
#define correction_ini 0.6		//Facteur de correction applique a l'estimateur de distance.
#define fact 3				//facteur de multiplication du mouvement utilise par le bouton de multiplication.
#define factRotation 5		//facteur de multiplication de la rotation utilise par le bouton de multiplication.
#define scalLimit 0.4			//facteur de modification du produit scalaire
#define ZoomInFactor 0.5
#define ZoomOutFactor 2

#define MAX_VALUE 4294967296.0

const double theta = 3.1415926535897932384626433832795 / 64; //Angle pas de des rotation de l'objet.
const double c_t = cos(theta); 	//Cosinus de theta.
const double s_t = sin(theta); 	//Sinus de theta.


#define p0_x 45 //Position initiale de la camera.
#define p0_y (-44)
#define p0_z (-76)

#define r0_x (-0.46)//Vecteur regard initial de la camera.
#define r0_y (0.40)
#define r0_z (0.79)

#define h0_x (0.88)//Vecteur horizontal pour la camera.
#define h0_y (0.13)
#define h0_z (0.45)

#define v0_x (0.07)//Vecteur vertical pour la camera.
#define v0_y (0.91)
#define v0_z (-0.41)

#define Infini_x (MAX_VALUE)//Coordonnees des pos trop eloignes de nous
#define Infini_y (MAX_VALUE)
#define Infini_z (MAX_VALUE)

#define pointsPerThread 1

//Matrice de rotation pour les rotations verticales.
const  Matrix3 R_x(1.0, 0.0, 0.0,
	0.0, c_t, -s_t,
	0.0, s_t, c_t);

//Matrice de rotation pour les rotations horizontales.
const  Matrix3 R_y(c_t, 0.0, -s_t,
	0.0, 1.0, 0.0,
	s_t, 0.0, c_t);

//Matrice de rotation suivant l'axe Z.
const Matrix3 R_z(c_t, -s_t, 0.0,
	s_t, c_t, 0.0,
	0.0, 0.0, 1.0);

//Inverse de la matrice de rotation verticale.
const  Matrix3 R_x_inv(1.0, 0.0, 0.0,
	0.0, c_t, s_t,
	0.0, -s_t, c_t);

//Inverse de la matrice de rotation horizontale.
const  Matrix3 R_y_inv(c_t, 0.0, s_t,
	0.0, 1.0, 0.0,
	-s_t, 0.0, c_t);

//Inverse de la matrice de rotation d'axe Z.
const  Matrix3 R_z_inv(c_t, s_t, 0.0,
	-s_t, c_t, 0.0,
	0.0, 0.0, 1.0);

void accessGPU(Coord* test, int i, Coord* tab);

#endif//KERNELCONST_H
