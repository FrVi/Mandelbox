#include "InfPanel3.h"
#include "WindowConst.h"
#include "WindowConst2.h"

InfPanel3::InfPanel3(Window* window) :videoPanel(window)
{
	setMinimumSize(windows_x_ini, 300);
	setMaximumSize(windows_x_ini, 300);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	layout.addWidget(&videoPanel);

	setLayout(&layout);
}

