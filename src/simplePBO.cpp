
#include "SimplePBO.h"
#include "FractalView.h"
#ifdef USE_CUDA
#include <cuda_runtime_api.h>
#endif
#include "WindowConst.h"

#ifndef GL_BGRA
#define GL_BGRA 0x80E1
#endif

#ifdef USE_CUDA
// Constructor
SimplePBO::SimplePBO() :pixelBuffer(0), textureID(0)
{
	image_width = Taille;
	image_height = Taille;
}

// Destructor
SimplePBO::~SimplePBO()
{
	cleanupCuda();
}

// initCuda
void SimplePBO::initCuda()
{
	//HANDLE_ERROR(cudaGLSetGLDevice(0));
	//cudaSetDeviceFlags(cudaDeviceBlockingSync);
}

// resize
void SimplePBO::resize(int w, int h)
{
	image_width = w;   // sizes must be a multiple of 16
	image_height = h;
	deletePBO();  // delete pixelBuffer and textures if they already exist
	deleteTexture();
	createPBO();  // create pixel buffer object and register to cude
	createTexture();  // create and allocate 2d texture buffer
	release(); // deactive pixelbuffer and texture object
}

QGLBuffer* SimplePBO::getPixelBuffer()
{
	return pixelBuffer;
}

// runCuda -  Run the Cuda part of the computation
void SimplePBO::runCuda()
{
	uchar3 *dptr = NULL;

	Q_ASSERT(pixelBuffer);

	fview->gpu_work();
}

void  SimplePBO::setFview(FractalView* fview)
{
	this->fview = fview;
}

// createPBO
void SimplePBO::createPBO()
{
	// set up vertex data parameter
	int size_tex_data = sizeof(GLubyte) * image_width * image_height * 3;

	if (!pixelBuffer)
	{
		pixelBuffer = new QGLBuffer(QGLBuffer::PixelUnpackBuffer);

		//FROM Qt Doc:The data will be modified repeatedly and used many times for reading data back from the GL server for use in further drawing operations.
		pixelBuffer->setUsagePattern(QGLBuffer::DynamicCopy);
		pixelBuffer->create();
	}
	pixelBuffer->bind();
	pixelBuffer->allocate(size_tex_data);

	HANDLE_ERROR(cudaGLRegisterBufferObject(pixelBuffer->bufferId()));
}

// deletePBO
void SimplePBO::deletePBO()
{
	if (pixelBuffer) {
		// unregister this buffer object with CUDA
		HANDLE_ERROR(cudaGLUnregisterBufferObject(pixelBuffer->bufferId()));

		delete pixelBuffer;
		pixelBuffer = 0;
	}
}

// createTexture
void SimplePBO::createTexture()
{
	deleteTexture(); // delete texture object if necessary for reallocating tex mem, e.g. at different size
	textureID = new GLuint[1];// Generate a texture identifier
	glGenTextures(1, textureID);
	glBindTexture(GL_TEXTURE_2D, textureID[0]);  // Make this the current texture (remember that GL is state-based)
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, image_width, image_height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, NULL);  // Allocate the texture memory. The last parameter is NULL since we only want to allocate memory, not initialize it
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  // Note: GL_TEXTURE_RECTANGLE_ARB may be used instead of GL_TEXTURE_2D
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);   //Replace GL_LINEAR with GL_NEAREST
	// Must set the filter mode, GL_LINEAR enables interpolation when scaling
}

// deleteTexture
void SimplePBO::deleteTexture()
{
	if (textureID)
	{
		glDeleteTextures(1, textureID);
		delete[] textureID;
		textureID = 0;
	}
}

// bind
void SimplePBO::bind()
{
	Q_ASSERT(pixelBuffer);
	pixelBuffer->bind();  // Create a texture from the buffer
	glBindTexture(GL_TEXTURE_2D, textureID[0]);  // bind texture from PBO
	// Note: glTexSubImage2D will perform a format conversion if the buffer is a different format GL_RGBA8 and (GL_BGRA + GL_UNSIGNED_BYTE)

	// Note: NULL indicates the data resides in device memory
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image_width, image_height,
		GL_RGB, GL_UNSIGNED_BYTE, NULL);
}

// release
void SimplePBO::release()
{
	Q_ASSERT(pixelBuffer);
	pixelBuffer->release();  // deactivate pixelbuffer object
	glBindTexture(GL_TEXTURE_2D, 0);  // deactivate texture object
}

// cleanupCuda
void SimplePBO::cleanupCuda()
{
	deletePBO();
	deleteTexture();
	HANDLE_ERROR(cudaThreadExit());
}

#endif
