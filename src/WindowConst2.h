#ifndef WINDOWCONST2_H
#define WINDOWCONST2_H

#include <QColor>

const QColor background_color(238, 238, 238);	//Couleur du fond du programme
const QColor menu_color(220, 220, 220);		//Couleur du fond du menu
const QColor LeftPanelColor(200, 200, 200);	//Couleur du panneau de commande des translations
const QColor RightPanelColor(200, 200, 200);	//Couleur du panneau de commande du zoom
const QColor CenterPanelColor(200, 200, 200);	//Couleur du panneau de commande des rotations et du panneau video et du panneau couleur
const QColor InfoPanelColor(220, 220, 220);	//Couleur du panneau d'information et du fond des autres
const QColor InfoSetColor(220, 220, 220);		//Couleur des infoSet

static inline std::string toUtf8(const QString& s)
{
	QByteArray sUtf8 = s.toUtf8();
	return std::string(sUtf8.constData(), sUtf8.size());
}

//http://qt-project.org/doc/qt-4.8/Qt.html

const int ZoomIn = Qt::Key_Plus;
const int ZoomOut = Qt::Key_Minus;
const int Reset = Qt::Key_5;
const int Up = Qt::Key_Z;
const int Down = Qt::Key_S;
const int Front = Qt::Key_E;
const int Back = Qt::Key_A;
const int Right = Qt::Key_D;
const int Left = Qt::Key_Q;
const int TurnUp = Qt::Key_8;
const int TurnDown = Qt::Key_2;
const int TurnRight = Qt::Key_6;
const int TurnLeft = Qt::Key_4;
const int TurnTrig = Qt::Key_7;
const int TurnHor = Qt::Key_9;
const int Add = Qt::Key_0;
const int Compile = Qt::Key_1;
const int Reset_points = Qt::Key_Period;

#endif//WINDOWCONST2_H
