#ifndef VIDEOPANEL_H
#define VIDEOPANEL_H

#include "CommandButton.h"

#include <QGridLayout>

class Window;
class VideoPanel : public QWidget
{
	Q_OBJECT

public:
	VideoPanel(Window *window);

	public slots:
	void reset();
	void add();
	void delete_pile();
	void compute();
	void save();
private:
	CommandButton bouton1, bouton2, bouton3, bouton4, bouton5;
	QGridLayout layout;
	Window *window;
};

#endif//VIDEOPANEL_H

