#ifndef COMPONENT_H
#define COMPONENT_H

#include "Coord.h"

class Component
{
private:
	Coord coords[4];

	double doubles[8];

	int ints[10];

public:
	Component(const Component& C);
	Component(Coord* coords, double *doubles, int *ints);
	void set(int i, double val);

	Coord getCoord(int i) const;

	double getdouble(int i) const;

	int getInt(int i) const;
	void afficher(FILE* fichier);
};

#endif//COMPONENT_H
