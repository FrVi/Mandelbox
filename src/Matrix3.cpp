#include "Matrix3.h"

Matrix3::Matrix3(const Coord &A, const Coord &B, const Coord &C)
{
	tab = new Coord[3];
	tab[0] = A;
	tab[1] = B;
	tab[2] = C;
}

Matrix3::~Matrix3()
{
	delete[] tab;
}

Matrix3::Matrix3(const Matrix3& M)
{
	tab = new Coord[3];
	tab[0] = M.tab[0];
	tab[1] = M.tab[1];
	tab[2] = M.tab[2];
}

void Matrix3::operator=(const Matrix3 &M)
{
	int i, j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			set(i, j, M.get(i, j));
		}
	}
}

//Construit une matrice nulle.
Matrix3::Matrix3()
{
	tab = new Coord[3];
	tab[0].set(0.0, 0.0, 0.0);
	tab[1].set(0.0, 0.0, 0.0);
	tab[2].set(0.0, 0.0, 0.0);
}

Matrix3::Matrix3(double a, double b, double c, double d, double e, double f, double g, double h, double i)
{
	tab = new Coord[3];
	tab[0].set(a, d, g);
	tab[1].set(b, e, h);
	tab[2].set(c, f, i);
}

Matrix3 operator*(const Matrix3 &M, const Matrix3 &A)
{
	int i, j, k;
	Matrix3 B;

	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			for (k = 0; k < 3; k++)
				B.set(i, j, B.get(i, j) + M.get(i, k)*A.get(k, j));
	return B;
}

Coord operator*(const Matrix3 &M, const Coord &X)
{
	int i, k;
	Coord B;
	B.set(0.0, 0.0, 0.0);

	for (i = 0; i < 3; i++)
		for (k = 0; k < 3; k++)
			B.set(i, B.get(i) + M.get(i, k)*X.get(k));
	return B;
}

double Matrix3::get(int i, int j) const
{
	return tab[j].get(i);
}

void Matrix3::set(int i, int j, double val)
{
	tab[j].set(i, val);
}

//suppose que inversible
void Matrix3::oppThis()
{
	double det = determinant();

	if (det != 0)
	{
		double mineur[9];

		mineur[0] = get(1, 1) * get(2, 2) - get(2, 1) * get(1, 2);
		mineur[1] = get(1, 0) * get(2, 2) - get(2, 0) * get(1, 2);
		mineur[2] = get(1, 0) * get(2, 1) - get(2, 0) * get(1, 1);
		mineur[3] = get(0, 1) * get(2, 2) - get(2, 1) * get(0, 2);
		mineur[4] = get(0, 0) * get(2, 2) - get(2, 0) * get(0, 2);
		mineur[5] = get(0, 0) * get(2, 1) - get(2, 0) * get(0, 1);
		mineur[6] = get(0, 1) * get(1, 2) - get(1, 1) * get(0, 2);
		mineur[7] = get(0, 0) * get(1, 2) - get(1, 0) * get(0, 2);
		mineur[8] = get(0, 0) * get(1, 1) - get(1, 0) * get(0, 1);

		set(0, 0, mineur[0] / det);
		set(1, 0, -mineur[1] / det);
		set(2, 0, mineur[2] / det);
		set(0, 1, -mineur[3] / det);
		set(1, 1, mineur[4] / det);
		set(2, 1, -mineur[5] / det);
		set(0, 2, mineur[6] / det);
		set(1, 2, -mineur[7] / det);
		set(2, 2, mineur[8] / det);
	}
}

Matrix3 Matrix3::operator-() const
{
	Matrix3 M(*this);
	M.oppThis();
	return M;
}

void Matrix3::afficher() const
{
	printf("| %lf %lf %lf |\n", get(0, 0), get(0, 1), get(0, 2));
	printf("| %lf %lf %lf |\n", get(1, 0), get(1, 1), get(1, 2));
	printf("| %lf %lf %lf |\n", get(2, 0), get(2, 1), get(2, 2));
}

double Matrix3::determinant() const
{
	return get(0, 0) * (get(1, 1)*get(2, 2) - get(1, 2)*get(2, 1))
		- get(1, 0) * (get(0, 1)*get(2, 2) - get(2, 1)*get(0, 2))
		+ get(2, 0) * (get(0, 1)*get(1, 2) - get(1, 1)*get(0, 2));
}
