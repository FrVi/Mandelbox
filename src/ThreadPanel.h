#ifndef THREADPANEL_H
#define THREADPANEL_H

//TOUT NE SERT PLUS, DANS PERFMONITOR NON PLUS
#include <QGridLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>

#include <QGraphicsView>
#include <QColorDialog>
#include <QLabel>

#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>

class FractalView;

class ThreadPanel : public QWidget
{
	Q_OBJECT

public:
	void set(FractalView* fview);
	ThreadPanel(FractalView* fview);
	void setLastTime(int time);
	int getThread_numbers();

	public slots:
	void decrThread_numbers();
	void incrThread_numbers();
	void modeCPU(bool checked);
	void modeGPU(bool checked);
	void modeLD(bool checked);
	void modeHD(bool checked);

private:
	QLabel threadLabel;
	QPushButton plus, moins;

	int thread_numbers;
	void setStringThread();

private:

	QGroupBox groupe, groupe2;
	QRadioButton boutonRadio1, boutonRadio2, boutonRadio3, boutonRadio4;

	QGridLayout layout;
	QGridLayout box, box2;
	FractalView* fview;
};

#endif//THREADPANEL_H
