#include "PointOfView.h"
#include "WindowConst.h"
#include "KernelConst.h"
#include "Window.h"

//Sauvegarde la distance du point d'observation au point de la figure projete en (x,y) sur l'ecran.
void PointOfView::saveToTable(Coord P, int x, int y)
{
	//throw 0;
	int i = x + y*screensize;
	window->setCoord(i, P);
	//coordTab[i]=P;
}

//Renvoie les coordonnees stockees en CoordTab en un point precis donne par les indices passes en argument.
Coord PointOfView::getCoord(int x, int y)
{
	int i = x + y*screensize;
	return window->getCoord(i);
}

//Construit un point de vue.
PointOfView::PointOfView(Window *w)
{
	this->screensize = screensize_ini;

	initScreenSize(screensize_ini);

	mvt_fact = mvt_fact_ini;
	rot_fact = rot_fact_ini;

	window = w;
	switch10 = false;
	switch10rot = false;

	set();
}

int PointOfView::getScreensize()
{
	return screensize;
}

int PointOfView::getHalfSize()
{
	return halfSize;
}

double PointOfView::getAlpha()
{
	return alpha;
}

void PointOfView::setScreensize(int screensize)
{
	//		freeTab();
	initScreenSize(screensize);
}

PointOfView::~PointOfView()
{
	//freeTab();
}

void PointOfView::set()
{
	limitDist = limitDist_ini;
	nearDist = nearDist_ini;
	position.set(p0_x, p0_y, p0_z);
	depthRef.set(r0_x, r0_y, r0_z);//r
	horizontalRef.set(h0_x, h0_y, h0_z);//h
	verticalRef.set(v0_x, v0_y, v0_z);//v
	zoomFactor = 1;
}

void PointOfView::switchFactor(int a)
{
	if (switch10)
	{
		switch10 = false;
		mvt_fact /= a;
	}
	else
	{
		switch10 = true;
		mvt_fact *= a;
	}
}

void PointOfView::switchFactorRotation(int a)
{
	if (switch10rot)
	{
		switch10rot = false;
		rot_fact /= a;
	}
	else
	{
		switch10rot = true;
		rot_fact *= a;
	}
}

Coord PointOfView::getPosition()
{
	return position;
}

Coord PointOfView::getVerticalRef()
{
	return verticalRef;
}

Coord PointOfView::getHorizontalRef()
{
	return horizontalRef;
}

Coord PointOfView::getDepthRef()
{
	return depthRef;
}

void PointOfView::setPosition(Coord coord)
{
	position = coord;
}

void PointOfView::setHorizontalRef(Coord coord)
{
	horizontalRef = coord;
}

void PointOfView::setVerticalRef(Coord coord)
{
	verticalRef = coord;
}

void PointOfView::setDepthRef(Coord coord)
{
	depthRef = coord;
}

double PointOfView::getZoomFactor()
{
	return zoomFactor;
}

void PointOfView::setZoomFactor(double z)
{
	zoomFactor = z;
}

void PointOfView::up()
{
	position += verticalRef*(-zoomFactor*mvt_fact);
}

void PointOfView::down()
{
	position += verticalRef*(zoomFactor*mvt_fact);
}

void PointOfView::right()
{
	position += horizontalRef*(zoomFactor*mvt_fact);
}

void PointOfView::left()
{
	position += horizontalRef*(-1 * zoomFactor*mvt_fact);
}

void PointOfView::rot(const Matrix3 *M)
{
	Matrix3 P(horizontalRef, verticalRef, depthRef);
	Matrix3 P_1 = -P;

	P = P * (*M) * P_1;//sale mais bon...

	for (int i = 0; i<rot_fact; i++)
	{
		Coord temp = P*depthRef;
		if ((temp - depthRef).norm2()>0.00001)//d n'est pas l'axe de rotation
		{
			depthRef = temp;

			//on recalcule pour avoir une tres bonne base, meilleur que par simple produit matriciel

			Coord d3 = Coord::normalize(depthRef + horizontalRef*(-horizontalRef.scal(depthRef)));
			double cosTheta = d3.scal(depthRef);
			double sinTheta = d3.scal(verticalRef);

			verticalRef = Coord::normalize(depthRef*(-sinTheta) + verticalRef*cosTheta);
			horizontalRef = verticalRef^depthRef;
		}
		else//si d est l'axe de rotation, on ne peut pas reclaculer a partir de lui, on prend h comme reference
		{
			horizontalRef = P*horizontalRef;

			Coord h3 = Coord::normalize(horizontalRef + verticalRef*(-verticalRef.scal(horizontalRef)));
			double cosTheta = h3.scal(horizontalRef);
			double sinTheta = h3.scal(depthRef);

			depthRef = Coord::normalize(horizontalRef*(-sinTheta) + (depthRef*cosTheta));
			verticalRef = depthRef^horizontalRef;
		}
	}
}

void PointOfView::front()
{
	position += depthRef*(zoomFactor*mvt_fact);
}

void PointOfView::back()
{
	position += depthRef*(-1 * zoomFactor*mvt_fact);
}

void PointOfView::zoom(double factor)
{
	ChangePointView(halfSize, halfSize, factor);
	//Mise a  jour de l'affichage
	//window.refresh();
}

void PointOfView::update()
{
	window->refresh();
}

double PointOfView::getLimitDist()
{
	return limitDist;
}

void PointOfView::setNearDist(double d)
{
	nearDist = d;
}

double PointOfView::getNearDist()
{
	return nearDist;
}

void PointOfView::ChangePointView(int x, int y, double echelle)
{
	if (getCoord(x, y).norm() < MAX_VALUE)
	{
		//alpha*=echelle;
		//alpha*=echelle;
		//virtualDist*=echelle;
		zoomFactor *= echelle;
		nearDist *= echelle;
		limitDist *= echelle;
		//Changement des vecteurs de bases
		Coord newDepthRef = Coord::normalize(getCoord(x, y) - position); // (Cible-pos) normalise, nouvelle reference de profondeur
		//d3 = d2 - (h.d2)h
		//d3 projete dans le plan (v,d)
		Coord d3 = Coord::normalize(newDepthRef + horizontalRef*(-horizontalRef.scal(newDepthRef)));
		double cosTheta = d3.scal(depthRef);
		double sinTheta = d3.scal(verticalRef);
		Coord newVerticalRef = Coord::normalize(depthRef*(-sinTheta) + verticalRef*cosTheta);
		Coord newHorizontalRef = newVerticalRef^newDepthRef;
		depthRef = newDepthRef;
		verticalRef = newVerticalRef;
		horizontalRef = newHorizontalRef;

		//Changement de la position	
		//pos = position + (cible - position)*(echelle)
		Coord pos = position + (getCoord(x, y) - position)*(1 - echelle);
		position = pos;
	}
	else if (echelle < 1)//CORRECTION TEMPORAIRE pour le zoom en dehors de l'objet, en attendant mieux
		front();
	else if (echelle > 1)
		back();
}

void PointOfView::pseudoOrth(Coord &C, double &c, int x, int y, Coord *tab)
{
	if (tab[x + y*screensize].get_x() == Infini_x && tab[x + y*screensize].get_y() == Infini_y && tab[x + y*screensize].get_z() == Infini_z)
	{
		c = -1;
		return;
	}

	int x1 = x - 1;
	int x2 = x;
	int x3 = x + 1;

	int y1 = y - 1;
	int y2 = y;
	int y3 = y + 1;

	//Bords de l'ecran
	if (x == 0 || y == 0 || y == screensize - 1 || x == screensize - 1)
	{
		if (x == 0)
			x1 = x;
		else if (x == screensize - 1)
			x3 = x;
		if (y == 0)
			y1 = y;
		else if (y == screensize - 1)
			y3 = y;
	}
	//Cas Generique
	C.pseudoOrth(tab[x1 + y2*screensize], tab[x2 + y3*screensize], tab[x3 + y2*screensize], tab[x2 + y1*screensize]);

	c = depthRef.modifiedScal(C); //fait une copie mais ne change rien en terme de vitesse en vrai
	c = std::max(0.0, c);
}


void PointOfView::pseudoOrth(Coord &C, double &c, int x, int y, Coord *tab1, Coord *tab2, Coord *tab3, Coord *tab4, Coord *tab5)
{
	if (tab1[x + y*screensize].get_x() == Infini_x && tab1[x + y*screensize].get_y() == Infini_y && tab1[x + y*screensize].get_z() == Infini_z)
	{
		c = -1;
		return;
	}

	int x1 = x - 1;
	int x2 = x;
	int x3 = x + 1;

	int y1 = y - 1;
	int y2 = y;
	int y3 = y + 1;

	//Bords de l'ecran
	if (x == 0 || y == 0 || y == screensize - 1 || x == screensize - 1)
	{
		if (x == 0)
			x1 = x;
		else if (x == screensize - 1)
			x3 = x;
		if (y == 0)
			y1 = y;
		else if (y == screensize - 1)
			y3 = y;
	}
	//Cas Generique
	C.pseudoOrth(tab3[x1 + y2*screensize], tab4[x2 + y3*screensize], tab2[x3 + y2*screensize], tab5[x2 + y1*screensize]);

	c = depthRef.modifiedScal(C); //fait une copie mais ne change rien en terme de vitesse en vrai
	c = std::max(0.0, c);

	c *= 1.7;
}

void PointOfView::pseudoOrth(Coord &C, double &c, int x, int y)
{
	if (getCoord(x, y).get_x() == Infini_x/* && getCoord(x,y).get_y()== Infini_y && getCoord(x,y).get_z()== Infini_z*/)//suffit...
	{
		c = -1;
		return;
	}

	int x1 = x - 1;
	int x2 = x;
	int x3 = x + 1;

	int y1 = y - 1;
	int y2 = y;
	int y3 = y + 1;

	//Bords de l'ecran
	if (x == 0 || y == 0 || y == screensize - 1 || x == screensize - 1)
	{
		if (x == 0)
			x1 = x;
		else if (x == screensize - 1)
			x3 = x;
		if (y == 0)
			y1 = y;
		else if (y == screensize - 1)
			y3 = y;
	}
	//Cas Generique

	Coord P1 = getCoord(x1, y2);
	Coord P2 = getCoord(x2, y3);
	Coord P3 = getCoord(x3, y2);
	Coord P4 = getCoord(x2, y1);

	/*if(P1.get_x() == Infini_x )
	{
	P1 = getCoord(x,y);
	}

	if(P2.get_x() == Infini_x )
	{
	P2 = getCoord(x,y);
	}

	if(P3.get_x() == Infini_x )
	{
	P3 = getCoord(x,y);
	}

	if(P4.get_x() == Infini_x )
	{
	P4 = getCoord(x,y);
	}*/

	C.pseudoOrth(P1, P2, P3, P4);

	c = depthRef.modifiedScal(C); //fait une copie mais ne change rien en terme de vitesse en vrai
	c = std::max(0.0, c);

	c *= 1.7;
}
