#include "RightSCmdPanel.h"
#include "KernelConst.h"
#include "WindowConst.h"
#include "WindowConst2.h"

RightSCmdPanel::RightSCmdPanel(PointOfView *pov) :bouton1("images/zoomin.png"), bouton2("images/zoomout.png"), bouton3("images/reset.png"), bouton4("images/rotup.png"), bouton5("images/rotdown.png"),
zoomIn(pov, ZoomInFactor), zoomOut(pov, ZoomOutFactor), turnTrig(pov, &R_z), turnHor(pov, &R_z_inv), reset(pov)
{
	setMinimumSize(150, 150);
	setMaximumSize(150, 150);
	layout.setContentsMargins(0, 0, 0, 0);

	setAutoFillBackground(true);
	setPalette(QPalette(RightPanelColor));

	layout.setSpacing(0);

	layout.addWidget(&bouton1, 0, 0);
	layout.addWidget(&bouton2, 0, 2);
	layout.addWidget(&bouton3, 1, 1);
	layout.addWidget(&bouton4, 2, 0);
	layout.addWidget(&bouton5, 2, 2);

	setLayout(&layout);

	QObject::connect(&bouton1, SIGNAL(clicked()), &zoomIn, SLOT(actionPerformed()));
	QObject::connect(&bouton2, SIGNAL(clicked()), &zoomOut, SLOT(actionPerformed()));
	QObject::connect(&bouton3, SIGNAL(clicked()), &reset, SLOT(actionPerformed()));
	QObject::connect(&bouton4, SIGNAL(clicked()), &turnTrig, SLOT(actionPerformed()));
	QObject::connect(&bouton5, SIGNAL(clicked()), &turnHor, SLOT(actionPerformed()));

	bouton1.setShortcut(QKeySequence(ZoomIn));
	bouton2.setShortcut(QKeySequence(ZoomOut));
	bouton3.setShortcut(QKeySequence(Reset));
	bouton4.setShortcut(QKeySequence(TurnTrig));
	bouton5.setShortcut(QKeySequence(TurnHor));
}
