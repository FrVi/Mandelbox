#include "Coord.h"
#include "KernelConst.h"

float Coord::get(int i) const
{
	switch (i)
	{
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default:return 0;
	}
}

void Coord::set(int i, float val)
{
	switch (i)
	{
	case 0: x = val; break;
	case 1: y = val; break;
	case 2: z = val; break;
	}
}

Coord Coord::normalize(const Coord &P)
{
	Coord Q(P);
	Q.normalized();
	return Q;
}

Coord operator^(const Coord &A, const Coord &B)
{
	float x = A.y*B.z - A.z*B.y;
	float y = A.z*B.x - A.x*B.z;
	float z = A.x*B.y - A.y*B.x;

	Coord tmp;
	tmp.set(x, y, z);
	return tmp;
}

float Coord::eltBend(float a) const
{
	//global++;
	if (a > 1)
		a = 2 - a;
	else if (a < -1)
		a = -2 - a;
	return a;
}
