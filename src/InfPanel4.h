#ifndef INFPANEL4_H
#define INFPANEL4_H

#include <QHBoxLayout>

#include "ColorPanel.h"

class InfPanel4 : public QWidget
{
public:
	InfPanel4(FractalView* fview);
	void resetColors();

private:
	ColorPanel colorPanel;
	QHBoxLayout layout;

};

#endif//INFPANEL4_H
