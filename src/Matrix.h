#ifndef MATRIX_H
#define MATRIX_H

class LUDecomposition;
class QRDecomposition;

double hypot_(double a, double b);

class Matrix
{
public:
	~Matrix();
	Matrix(int m, int n);
	Matrix(const Matrix &M);
	void set(int i, int j, double s);
	double at(int i, int j) const;
	Matrix(int m);
	//void afficher();

	void operator=(const Matrix &M);
	Matrix operator*(double s);
	Matrix operator*(const Matrix &B);

private:
	void clear();
	double** A;

	int m, n;
};

#endif//MATRIX_H
