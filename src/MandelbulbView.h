#ifndef MANDELBULBVIEW_H
#define MANDELBULBVIEW_H

#include "FractalView.h"
#include "Coord.h"


class MandelbulbView :public FractalView
{

private:

	int (MandelbulbView::*function)(const Coord&, int);
#ifdef USE_CUDA
	void (MandelbulbView::*GPUfunction) (dim3,dim3,float,Coord,Coord,Coord,Coord,Coord*);
	void (MandelbulbView::*GPUfunctionHD) (dim3,dim3 ,float ,Coord* _d,Coord* ,Coord* ,Coord* ,Coord* ,Coord* ,Coord* ,Coord* ,Coord* );

protected:

	void recolorize(uchar3 *devPtr);
#endif//

public:
	MandelbulbView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon);	//Construit une MandelboxView.
	//void test();
	//permet de recourir a des expressions optimisee (4 a 10 fois plus rapides)
	//expressions sans trigonometrie 
	//et sans racines pour les ordres impaires
	void define();
#ifdef USE_CUDA
	void GPUComputeKernel(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUComputeKernelLauncherHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionHuit(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUFunctionSept(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUFunctionCinq(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUFunctionQuatre(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUFunctionDeux(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUFunctionGenerique(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);

	void GPUFunctionHuitHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionSeptHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionCinqHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionQuatreHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionDeuxHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
	void GPUFunctionGeneriqueHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
#endif//_H

protected:
	int getIncrement(const Coord &P, Coord &P2, int i);//Applique des iterations du processus de la Mandelbox sur un point donne jusqu'aux conditions limites.
	int getIncrementGenerique(const Coord &P,/*Coord &P2,*/ int i);
	int getIncrementDeux(const Coord &P,/*Coord &P2,*/ int i);
	int getIncrementQuatre(const Coord &P,/*Coord &P2,*/ int i);
	int getIncrementCinq(const Coord &P,/*Coord &P2,*/ int i);
	int getIncrementSept(const Coord &P,/*Coord &P2,*/ int i);
	int getIncrementHuit(const Coord &P,/*Coord &P2,*/ int i);
};

#endif//MANDELBULBVIEW_H
