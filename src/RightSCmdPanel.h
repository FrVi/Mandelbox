#ifndef RIGHTSCMDPANEL_H
#define RIGHTSCMDPANEL_H

#include "CommandButton.h"

#include <QGridLayout>

class RightSCmdPanel : public QWidget
{
public:
	RightSCmdPanel(PointOfView *pov);

private:
	CommandButton bouton1, bouton2, bouton3, bouton4, bouton5;
	QGridLayout layout;
	Turn turnTrig;
	Turn turnHor;
	Zoom zoomIn;
	Zoom zoomOut;
	Reset reset;
};

#endif //RIGHTSCMDPANEL_H
