#ifndef HELP_H
#define HELP_H

#include <string>

const std::string Help_Up = "Make the camera go up following its own references.";
const std::string Help_Down = "Make the camera go down following its own references.";
const std::string Help_Right = "Make the camera step right following its own references.";
const std::string Help_Left = "Make the camera step left following its own references.";
const std::string Help_Front = "Make the camera move forward following its own references.";
const std::string Help_Back = "Make the camera move backward following its own references.";

const std::string Help_TurnUp = "Turn up the camera.";
const std::string Help_TurnLeft = "Turn left the camera.";
const std::string Help_TurnDown = "Turn down the camera.";
const std::string Help_TurnRight = "Turn right the camera.";
const std::string Help_TurnHor = "Turn the camera following the depth axis, clockwise.";
const std::string Help_TurnTrig = "Turn the camera following the depth axis, counter-clockwise.";

const std::string Help_Reset = "Reset parameters to their default values.";
const std::string Help_ZoomOut = "Increase the distance between the camera and the object.";
const std::string Help_ZoomIn = "Decrease the distance between the camera and the object.";
const std::string Help_threadPlus = "Increments the number of threads.";
const std::string Help_threadLess = "Decrements the number of threads.";
const std::string Help_fact = "Move faster.";
const std::string Help_factRotation = "Turn faster.";


const std::string Help_Compute = "Compute all the points from the checkpoints in the list.";
const std::string Help_Add = "Add a checkpoint to the list.";
const std::string Help_Reset2 = "Reset the list.";
const std::string Help_Save = "Create the images for the video.";
const std::string Help_Delete = "Delete the last image in the list.";

const std::string Help_x = "(abscisse)";
const std::string Help_y = "(ordonnee)";
const std::string Help_z = "(profondeur)";

#endif//HELP_H
