#include "CommandButton.h"

CommandButton::CommandButton(std::string Name)
{
	setMinimumSize(50, 50);
	setMaximumSize(50, 50);

	QPixmap pixmap(Name.c_str());
	QIcon ButtonIcon(pixmap);
	setIcon(ButtonIcon);
	setIconSize(pixmap.rect().size());
	setFixedSize(size());
}
