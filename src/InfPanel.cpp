#include "InfPanel.h"
#include "WindowConst.h"
#include "WindowConst2.h"

InfPanel::InfPanel(PointOfView *pov) :rightCmdPanel(pov), leftCmdPanel(pov), rightSCmdPanel(pov)
{
	setMinimumSize(windows_x_ini, 300);
	setMaximumSize(windows_x_ini, 300);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	layout.addWidget(&leftCmdPanel);
	layout.addWidget(&rightCmdPanel);
	layout.addWidget(&rightSCmdPanel);

	setLayout(&layout);
}
