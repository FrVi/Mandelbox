#include "InfPanel5.h"
#include "WindowConst.h"
#include "WindowConst2.h"

InfPanel5::InfPanel5(FractalView* fview) :threadPanel(fview)
{
	setMinimumSize(windows_x_ini, 300);
	setMaximumSize(windows_x_ini, 300);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	layout.addWidget(&threadPanel, Qt::AlignCenter);

	setLayout(&layout);
}

