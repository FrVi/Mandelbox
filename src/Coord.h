#ifndef COORD2_H
#define COORD2_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Color.h"

#ifndef USE_CUDA
#define __device__ 
#define __host__ 
#endif//USE_CUDA

class Coord
{
private:
	float x;
	float y;
	float z;
	float eltBend(float a) const;

public:
	__host__ __device__ int Round(int colorA, int colorD, int colorG) const;
	__host__ __device__ void operator=(const Coord &C);
	__host__ __device__	void operator+=(const Coord &A);
	__host__ __device__ void operator*=(float a);

	__host__ __device__ void set_z(float z);
	__host__ __device__ void set_y(float y);
	__host__ __device__ void set_x(float x);
	__host__ __device__ void pseudoOrth(const Coord &X1, const Coord &X2, const  Coord &X3, const  Coord &X4);
	__host__ __device__ void set(float x, float y, float z);
	__host__ __device__ void MultiAddToThis(const Coord &, float v);
	__host__ __device__ void bendThis();
	__host__ __device__ void normalized();

	__host__ __device__ float get_z() const;
	__host__ __device__ float get_y() const;
	__host__ __device__ float get_x() const;
	__host__ __device__ float modifiedScal(const Coord &A) const;
	__host__ __device__ float norm2() const;
	__host__ __device__ float norm() const;
	__host__ __device__ float scal(const Coord &) const;

	__host__ __device__ Coord(const Coord &C);
	__host__ __device__ Coord(float x, float y, float z);
	__host__ __device__ Coord();

	friend __host__ __device__ Coord operator+(Coord const& A, Coord const& B);
	friend __host__ __device__ Coord operator-(Coord const& A, Coord const& B);
	friend __host__ __device__ Coord operator*(Coord const& A, float a);
	void set(int i, float val);
	float get(int i) const;
	Color toColor() const;

	friend Coord operator^(Coord const& A, const Coord &B);
	static Coord normalize(const Coord &P);
};


Coord operator^(Coord const& A, const Coord &B);

#endif//COORD2_H
