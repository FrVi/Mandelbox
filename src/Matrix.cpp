#include "Matrix.h"
#include <iostream>
#include <math.h>

Matrix::Matrix(int m, int n)
{
	this->m = m;
	this->n = n;
	A = new double*[m];

	for (int i = 0; i < m; i++)
	{
		A[i] = new double[n];
	}
}

Matrix::Matrix(int m)
{
	if (m < 3) throw 0;

	this->m = m;
	this->n = m;
	A = new double*[m];

	int i, j;

	double *a = new double[m];
	double *b = new double[m];

	a[0] = 1;//m tjrs plus grand que 3
	a[1] = 2;

	b[0] = 0;//m tjrs plus grand que 3
	b[1] = 3;

	for (i = 2; i < m; i++)
	{
		a[i] = 4 * a[i - 1] - a[i - 2];
		b[i] = 4 * b[i - 1] - b[i - 2];
	}

	for (i = 0; i < m; i++)
	{
		A[i] = new double[m];
	}

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < m; j++)
		{
			A[j][i] = 0.0;
		}
	}

	double d;

	for (j = 0; j < m; j++)
	{
		for (i = 0; i <= j; i++)
		{
			if (j == m - 1)
			{
				A[i][j] = a[i];
			}
			else if (i == 0)
			{
				A[i][j] = a[n - 1 - j];
			}
			else
			{
				A[i][j] = a[n - 1 - j] * a[i];
			}
		}
	}

	for (j = 0; j < m; j++)
	{
		for (i = 0; i <= j; i++)
		{
			A[j][i] = A[i][j];
		}
	}

	int k;

	for (j = 0; j < m; j++)
	{
		for (i = 0; i < m; i++)
		{
			k = i + j;
			if (((k >> 1) << 1) != k)
			{
				A[i][j] = -A[i][j];
			}
		}
	}

	for (j = 0; j < m; j++)
	{
		for (i = 0; i < m; i++)
		{
			A[i][j] /= b[m - 1];
		}
	}

	delete[] b;
	delete[] a;
}

void Matrix::clear()
{
	for (int i = 0; i < m; i++)
	{
		delete[] A[i];
	}

	delete[] A;
}

void Matrix::operator=(const Matrix &M)
{
	clear();

	this->m = M.m;
	this->n = M.n;
	A = new double*[m];

	int j;

	for (int i = 0; i < m; i++)
	{
		A[i] = new double[n];
		for (int j = 0; j < n; j++)
		{
			A[i][j] = M.at(i, j);
		}
	}

}

Matrix::Matrix(const Matrix &M)
{
	this->m = M.m;
	this->n = M.n;
	A = new double*[m];

	int j;

	for (int i = 0; i < m; i++)
	{
		A[i] = new double[n];
		for (int j = 0; j < n; j++)
		{
			A[i][j] = M.at(i, j);
		}
	}
}

Matrix::~Matrix()
{
	clear();
}

void Matrix::set(int i, int j, double s)
{
	A[i][j] = s;
}

double Matrix::at(int i, int j) const
{
	return A[i][j];
}

Matrix Matrix::operator*(double s)
{
	Matrix X(m, n);
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			X.set(i, j, s*A[i][j]);
		}
	}
	return X;
}


Matrix Matrix::operator*(const Matrix &B)
{
	if (B.m != n) {
		throw "Matrix inner dimensions must agree.";
	}
	Matrix X(m, B.n);


	int i, j, k;

	double *Bcolj = new double[n];
	for (j = 0; j < B.n; j++)
	{
		for (k = 0; k < n; k++)
		{
			Bcolj[k] = B.A[k][j];
		}
		for (i = 0; i < m; i++)
		{
			double s = 0;
			for (k = 0; k < n; k++)
			{
				s += A[i][k] * Bcolj[k];
			}
			X.set(i, j, s);
		}
	}

	delete[] Bcolj;

	return X;
}
