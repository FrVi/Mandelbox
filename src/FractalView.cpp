#include "FractalView.h"

#include "KernelConst.h"
#include "WindowConst.h"

#include "PointOfView.h"
#include "Screen.h"
#include "PerfMonitor.h"

#include "FractalThread.h"
#include "ThreadPanel.h"

#include "KernelPBO.h"

void accessGPU(Coord* test, int i, Coord* tab)
{
#ifdef USE_CUDA
	CPUaccessGPU(test, i, tab);
#endif
}

Coord* FractalView::getTab_d()
{
	return tab_d;
}

void FractalView::setThreadPanel(ThreadPanel *threadPanel)
{
	this->threadPanel = threadPanel;
}

#ifdef USE_CUDA
void FractalView::recolorizeCPU(uchar3 *devPtr)
{
	(this->*drawQualityPtr)();
}
#else
void FractalView::recolorizeCPU()
{
	(this->*drawQualityPtr)();
}
#endif

FractalView::FractalView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon)
{
	HDmode = !iniLowQualityMode;

	setDrawQuality(iniLowQualityMode);
	screen->setFview(this);

	correction = correction_ini;
	incrLimit = incrLimit_ini;

	initColors();

	this->pov = pov;
	this->perfMon = perfMon;
	this->screen = screen;

	distance = new double[thread_number_max];

	s = sInit;
	minRadius = minRadiusInit;
	fixedRadius = fixedRadiusInit;
	fR2 = fixedRadius * fixedRadius;
	mR2 = minRadius *minRadius;
	fR2mR2 = fR2 / mR2;
	L = lInit;

	setGPU_CPUmode(iniGPUMode);

	iniTabCPU();
}

void FractalView::iniTabCPU()
{
	int cond = pov->getScreensize();

	horizontales = new Coord[cond*cond];
	verticales = new Coord[cond*cond];
	directions = new Coord[cond*cond];
	positions = new Coord[cond*cond];
	tab1 = new Coord[cond*cond];
	tab2 = new Coord[cond*cond];
	tab3 = new Coord[cond*cond];
	tab4 = new Coord[cond*cond];
	tab5 = new Coord[cond*cond];

	ColorTab = new Color[cond*cond];

	normales = new Coord[cond*cond];
	normales2 = new Coord[cond*cond];

	doubleTab = new double[cond*cond];
	doubleTab2 = new double[cond*cond];

	tab_h = new Coord[cond*cond];
}

void FractalView::iniTabGPU()
{
	int screensize = pov->getScreensize();
	size_t size4 = screensize*screensize* sizeof(float);
	size_t size2 = screensize*screensize* sizeof(Coord);
	size_t size5 = screensize*screensize* sizeof(Color);
#ifdef	USE_CUDA
	cudaMalloc((void**)&normales_d, size2);
	cudaMalloc((void**)&horizontales_d, size2);
	cudaMalloc((void**)&verticales_d, size2);
	cudaMalloc((void**)&directions_d, size2);
	cudaMalloc((void**)&normales2_d, size2);
	cudaMalloc((void**)&ColorTab_d, size5);
	cudaMalloc((void**)&doubleTab_d, size4);
	cudaMalloc((void**)&doubleTab2_d, size4);
	cudaMalloc((void**)&tab_2d1, size2);
	cudaMalloc((void**)&tab_2d2, size2);
	cudaMalloc((void**)&tab_2d3, size2);
	cudaMalloc((void**)&tab_2d4, size2);
	cudaMalloc((void**)&tab_2d5, size2);
	cudaMalloc((void**)&tab_d, size2);
#endif
}
void FractalView::deleteTabGPU()
{
#ifdef	USE_CUDA
	cudaFree(normales_d);
	cudaFree(horizontales_d);
	cudaFree(verticales_d);
	cudaFree(directions_d);
	cudaFree(tab_2d1);
	cudaFree(tab_2d2);
	cudaFree(tab_2d3);
	cudaFree(tab_2d4);
	cudaFree(tab_2d5);
	cudaFree(normales2_d);
	cudaFree(ColorTab_d);
	cudaFree(doubleTab_d);
	cudaFree(doubleTab2_d);
	cudaFree(tab_d);
#endif
}

void FractalView::deleteTabCPU()
{
	delete[] positions;
	delete[] verticales;
	delete[] horizontales;
	delete[] directions;

	delete[] tab1;
	delete[] tab2;
	delete[] tab3;
	delete[] tab4;
	delete[] tab5;

	delete[] ColorTab;
	delete[] normales;
	delete[] normales2;
	delete[] doubleTab;
	delete[] doubleTab2;
	delete[]  tab_h;
}

FractalView::~FractalView()
{
	semaphore->closeThreads();
	semaphore->relNormalSemaStart();
	semaphore->acqNormalSemaEnd();
	semaphore->relHighSemaStart();
	semaphore->acqHighSemaEnd();

	delete[] distance;
	deleteTabCPU();
	deleteTabGPU();
}

void FractalView::setCoord(int i, const Coord& val)
{
	tab_h[i] = val;
}

void FractalView::colorize()
{
	colorize_function();
	setColor();
	screen->repaint();
}

void FractalView::initColors()
{
	colorA = (int)colorX_x;
	colorB = (int)colorX_y;
	colorC = (int)colorX_z;
	colorD = (int)colorY_x;
	colorE = (int)colorY_y;
	colorF = (int)colorY_z;
	colorG = (int)colorZ_x;
	colorH = (int)colorZ_y;
	colorI = (int)colorZ_z;
}

void FractalView::setColors(int i, int val)
{
	val = std::min(255, std::max(val, 0));

	switch (i)
	{
	case 0:
	{
		colorA = val;
		break;
	}
	case 1:
	{
		colorB = val;
		break;
	}
	case 2:
	{
		colorC = val;
		break;
	}
	case 3:
	{
		colorD = val;
		break;
	}
	case 4:
	{
		colorE = val;
		break;
	}
	case 5:
	{
		colorF = val;
		break;
	}
	case 6:
	{
		colorG = val;
		break;
	}
	case 7:
	{
		colorH = val;
		break;
	}
	default:
	{
		colorI = val;
		break;
	}
	}
}

void FractalView::init()
{
	GPU_const_init();
	iniTabGPU();
}

int FractalView::getColors(int i)
{
	switch (i)
	{
	case 0:
	{
		return colorA;
	}
	case 1:
	{
		return colorB;
	}
	case 2:
	{
		return colorC;
	}
	case 3:
	{
		return colorD;
	}
	case 4:
	{
		return colorE;
	}
	case 5:
	{
		return colorF;
	}
	case 6:
	{
		return colorG;
	}
	case 7:
	{
		return colorH;
	}
	default:
	{
		return colorI;
	}
	}
}


//Relance le calcul et actualise l'affichage de la vue sur l'ecran.
void FractalView::update()
{
	compute();
}

int FractalView::getThread_numbers()
{
	return threadPanel->getThread_numbers();
}

int fun(int colorA, double x)
{
	if (x + 1 < 0)
	{
		return 0;
	}
	else if (1 > x)
	{
		return (int)((x + 1) / 2 * colorA);
	}
	else
	{
		return colorA;
	}
}

void FractalView::setColorNorm(int x, int y, const Coord &X, double c, const Color &Y0, const Color &Y1, const Color &Y2, const Color &Y3, const Color &Y4)
{

	if (c == -1)
	{
		screen->setPixel(x, y, 255, 255, 255);
		return;
	}

	int c1 = (int)((c*X.Round(colorA, colorD, colorG)));
	int c2 = (int)((c*X.Round(colorB, colorE, colorH)));
	int c3 = (int)((c*X.Round(colorC, colorF, colorI)));

	double alp = 0.7;
	double bet = 1.0 - alp;

	Color Y;
	Y.r = Y0.r + Y1.r + Y2.r + Y3.r + Y4.r;
	Y.g = Y0.g + Y1.g + Y2.g + Y3.g + Y4.g;
	Y.b = Y0.b + Y1.b + Y2.b + Y3.b + Y4.b;

	int norme[5];

	norme[0] = Y0.r + Y0.g + Y0.b;
	norme[1] = Y1.r + Y1.g + Y1.b;
	norme[2] = Y2.r + Y2.g + Y2.b;
	norme[3] = Y3.r + Y3.g + Y3.b;
	norme[4] = Y4.r + Y4.g + Y4.b;

	double nbre = 0;

	for (int i = 0; i < 5; i++)
	{
		if (norme[i] != 0)
		{
			nbre++;
		}
	}

	if (Y.r == 0 && Y.g == 0 && Y.b == 0)
	{
		screen->setPixel(x, y, c1, c2, c3);
	}
	else
	{
		c1 = c1*alp + (Y.r / nbre)*bet;
		c2 = c2*alp + (Y.g / nbre)*bet;
		c3 = c3*alp + (Y.b / nbre)*bet;
		screen->setPixel(x, y, c1, c2, c3);
	}
}

void FractalView::setColorNorm(int x, int y, const Coord &X, double c, const Color &Y)
{
	if (c == -1)
	{
		screen->setPixel(x, y, 255, 255, 255);
		return;
	}

	int c1 = (int)((c*X.Round(colorA, colorD, colorG)));
	int c2 = (int)((c*X.Round(colorB, colorE, colorH)));
	int c3 = (int)((c*X.Round(colorC, colorF, colorI)));

	double alp = 0.5;
	double bet = 1.0 - alp;

	if (Y.r == 255 && Y.g == 255 && Y.b == 255)
	{
		screen->setPixel(x, y, c1, c2, c3);
	}
	else
	{
		c1 = c1*alp + Y.r*bet;
		c2 = c2*alp + Y.g*bet;
		c3 = c3*alp + Y.b*bet;
		screen->setPixel(x, y, c1, c2, c3);
	}
}


void FractalView::setColorNorm(const Coord &X, double c, Color &Y)
{
	if (c == -1)
	{
		Y = Color(0, 0, 0);
		return;
	}

	Y = Color((int)(c*(X.Round(colorA, colorD, colorG))), (int)(c*(X.Round(colorB, colorE, colorH))), (int)(c*(X.Round(colorC, colorF, colorI))));
}


void FractalView::setColorNorm(int x, int y, const Coord &X, double c)
{
	if (c == -1)
	{
		screen->setPixel(x, y, 255, 255, 255);
		return;
	}

	screen->setPixel(x, y, (int)(c*(X.Round(colorA, colorD, colorG))), (int)(c*(X.Round(colorB, colorE, colorH))), (int)(c*(X.Round(colorC, colorF, colorI))));
}

void FractalView::setColor()
{
	redraw();

}

//Renvoye la limite d'incrementation.
int FractalView::getIncrLimit()
{
	return incrLimit;
}

// Renvoye le facteur de correction.
double FractalView::getCorrection()
{
	return correction;
}

#ifdef USE_CUDA
void FractalView::recolorize(uchar3 *devPtr)
{
	(this->*RedrawModePtr)(devPtr);
}
#endif

void FractalView::redraw()
{
#ifdef USE_CUDA
	uchar3 *dptr = NULL;
	Q_ASSERT(screen->getPixelBuffer());

	// map OpenGL buffer object for writing from CUDA		on a single GPU no data is moved (Win & Linux).		OpenGL should not use this buffer
	HANDLE_ERROR(cudaGLMapBufferObject((void**)&dptr, screen->getPixelBuffer()->bufferId()));
	Q_ASSERT(dptr);
	//printf("Redraw\n");
	recolorize(dptr);
	HANDLE_ERROR(cudaGLUnmapBufferObject(screen->getPixelBuffer()->bufferId()));  // unmap buffer object
#endif
}

//Rend un jeton a  un semaphore de fin de travail.
//i Indice du semaphore de fin de travail auquel le jeton est rendu.
void FractalView::relNormalSemaEnd(int idX)
{
	semaphore->relNormalSemaEnd(idX);
}

//Demande un jeton a  un semaphore de debut de travail.
//i Indice du semaphore de debut de travail auquel le jeton est demande.
void FractalView::acqNormalSemaStart(int idX)
{
	semaphore->acqNormalSemaStart(idX);
}

//Rend un jeton a  un semaphore de fin de travail.
//i Indice du semaphore de fin de travail auquel le jeton est rendu.
void FractalView::relHighSemaEnd(int idX)
{
	semaphore->relHighSemaEnd(idX);
}

//Demande un jeton a  un semaphore de debut de travail.
//i Indice du semaphore de debut de travail auquel le jeton est demande.
void FractalView::acqHighSemaStart(int idX)
{
	semaphore->acqHighSemaStart(idX);
}

void FractalView::compute()
{
	QTime t;

	t.restart();

	work();

	int q;
	q = t.elapsed();

	perfMon->setLastTime(q);
}

void FractalView::setRecolorizeMode(bool GPUmode)
{
#ifdef USE_CUDA
	if (GPUmode)
	{
		RedrawModePtr = &FractalView::recolorizeGPU;
	}
	else
	{
#endif
		RedrawModePtr = &FractalView::recolorizeCPU;
#ifdef USE_CUDA
	}
#endif
}

void FractalView::setDrawQuality(bool low)
{
	if (low)
	{
		drawQualityPtr = &FractalView::lowQuality;
		HDmode = false;
	}
	else
	{
		HDmode = true;
		drawQualityPtr = &FractalView::highQuality;
	}
}

void FractalView::highQualityWork(int i)
{
	reflectWork(i, tab1, 0, 0);
	reflectWork(i, tab2, 1, 0);
	reflectWork(i, tab3, -1, 0);
	reflectWork(i, tab4, 0, 1);
	reflectWork(i, tab5, 0, -1);
}

void FractalView::highQuality()
{
	int x, y;

	int cond = pov->getScreensize();

	Coord pos = pov->getPosition();
	Coord H = pov->getHorizontalRef();
	Coord V = pov->getVerticalRef();
	Coord N;
	Coord D;

	double nearDist = pov->getNearDist();

	int k;

	for (x = 0; x < cond; x++)
	{
		for (y = 0; y < cond; y++)
		{
			k = x + y*cond;
			pov->pseudoOrth(normales[k], doubleTab[k], x, y);
			positions[k] = pov->getCoord(x, y);
			directions[k] = positions[k] - pos;
			directions[k].normalized();


			N = normales[k];
			D = directions[k];

			horizontales[k] = H + N*(H.scal(N)*(-2));
			verticales[k] = V + N*(V.scal(N)*(-2));
			directions[k] = D + N*(D.scal(N)*(-2));
		}
	}

	semaphore->relHighSemaStart();//lance le calcul dans les threads
	semaphore->acqHighSemaEnd();

	semaphore->swap2();

	/*
	for(x = 0;x < cond; x++)
	{
	for(y = 0;y < cond; y++)
	{
	screen->setPixel(x,y,(int)(positions[x+y*cond].get_x()*255),
	(int)(positions[x+y*cond].get_y()*255),
	(int)(positions[x+y*cond].get_z()*255));
	}
	}*/
	for (x = 0; x < cond; x++)
	{
		for (y = 0; y < cond; y++)
		{
			k = x + y*cond;
			pov->pseudoOrth(normales2[x + y*cond], doubleTab2[k], x, y, tab1, tab2, tab3, tab4, tab5);//construit les pseudo-normales a partir de tab1-tab5 
			//et les place dans normales2 et les coefficients c dans doubleTab2

			setColorNorm(normales2[k], doubleTab2[k], ColorTab[k]);//place la couleur resultante dans ColorTab
		}
	}

	for (x = 0; x < cond; x++)
	{
		k = x + (cond - 1)*cond;

		setColorNorm(x, 0, normales[x], doubleTab[x], ColorTab[x]);//fait la combinaison du reflet et de l'image simple.
		setColorNorm(x, cond - 1, normales[k], doubleTab[k], ColorTab[k]);//fait la combinaison du reflet et de l'image simple.
	}

	for (y = 0; y < cond; y++)
	{
		k = cond - 1 + y*cond;
		setColorNorm(0, y, normales[y*cond], doubleTab[y*cond], ColorTab[y*cond]);//fait la combinaison du reflet et de l'image simple.
		setColorNorm(cond - 1, y, normales[k], doubleTab[k], ColorTab[k]);//fait la combinaison du reflet et de l'image simple.
	}



	for (x = 1; x < cond - 1; x++)
	{
		for (y = 1; y < cond - 1; y++)
		{
			k = x + y*cond;
			setColorNorm(x, y, normales[k], doubleTab[k], ColorTab[k], ColorTab[k + 1], ColorTab[k - 1], ColorTab[x + (y + 1)*cond], ColorTab[x + (y - 1)*cond]);//fait la combinaison du reflet et de l'image simple.
		}
	}
}

void FractalView::work(int i)
{
	int x, y, p, q;
	Coord P2(0.0, 0.0, 0.0);

	double d;

	Coord pos(pov->getPosition());

	Coord H(pov->getHorizontalRef());
	Coord V(pov->getVerticalRef());
	Coord D(pov->getDepthRef());

	double alpha = pov->getAlpha();

	H *= alpha;
	V *= alpha;

	Coord P(pos);

	int thread_numbers = threadPanel->getThread_numbers();


	/*int inf = -pov.halfSize()/thread_numbers;
	int sup = pov.halfSize()/thread_numbers-1;
	*/

	int max = pov->getScreensize() / thread_numbers - 1;

	int dec = 0;

	if (i < pov->getScreensize() - (max + 1)*thread_numbers)
	{
		dec++;
	}

	double lim = pov->getLimitDist();
	double half = pov->getHalfSize();

	int th = thread_numbers*(max / 2);//attention aux parentheses!!

	Coord H_a;
	Coord V_a;
	Coord D_a;

	FILE *file;

	for (x = -th + i; x <= thread_numbers *(max - max / 2 + dec) + i; x += thread_numbers)// max - max/2 != max/2 : c'est des entiers
	{
		p = x + th;

		for (y = -half; y < half; y++)
		{
			q = y + half;

			H_a = H;
			H_a *= ((double)x);
			V_a = V;
			V_a *= ((double)y);
			D_a = D;

			D_a += H_a;
			D_a += V_a;

			d = 0;
			P = pos;


			while (getIncrement(P, P2, i) != incrLimit && d < lim)//distance[i] est modifie
			{
				d += correction*distance[i];

				//P = pos + (D + x_a*H + y_a*V) * d
				P = pos;
				P.MultiAddToThis(D_a, d);
			}

			//Enregistrement des coordonnees du point courant.
			if (d < lim)
				pov->saveToTable(P, p, q);
			else
				pov->saveToTable(Coord(Infini_x, Infini_y, Infini_z), p, q);
		}
	}
}


ThreadPanel* FractalView::getThreadPanel()
{
	return threadPanel;
}

void FractalView::reflectWork(int i, Coord* tab, int a, int b)
{
	//enlever les trucs qui servent a rien dans reflectWork (distance1 et les mem par exemple)

	int x, y, p, q;
	Coord P2(0.0, 0.0, 0.0);

	double d;

	double alpha = pov->getAlpha();

	Coord P;

	double lim = pov->getLimitDist();
	int half = pov->getHalfSize();
	int size = pov->getScreensize();

	Coord H_a;
	Coord V_a;
	Coord D_a;

	int thread_numbers = threadPanel->getThread_numbers();

	int max = pov->getScreensize() / thread_numbers - 1;

	int dec = 0;

	if (i < pov->getScreensize() - (max + 1)*thread_numbers)
	{
		dec++;
	}

	//FILE *file;

	int th = thread_numbers*(max / 2);//attention aux parentheses!!

	Coord pos2;

	int k;

	for (x = -th + i; x <= thread_numbers *(max - max / 2 + dec) + i; x += thread_numbers)// max - max/2 != max/2 : c'est des entiers
	{
		p = x + th;

		for (y = -half; y < half; y++)
		{
			q = y + half;
			k = p + q*size;

			pos2 = positions[k] + directions[k] * (2.00*pov->getNearDist());

			P = positions[k];

			H_a = horizontales[k] * alpha;
			V_a = verticales[k] * alpha;
			D_a = directions[k];

			D_a += H_a*a;
			D_a += V_a*b;

			d = 0;

			P = pos2;

			//dans les 2 cas, on fait l'algo normal...
			while (getIncrement(P, P2, i) != incrLimit && d < lim)
			{
				d += correction*distance[i];

				P = pos2;
				P.MultiAddToThis(D_a, d);
			}

			if (d < lim)
				tab[k] = P;
			else
				tab[k] = Coord(Infini_x, Infini_y, Infini_z);
		}
	}
}

void FractalView::lowQuality()
{
	int y;

	Coord C;
	double c;

	int cond = pov->getScreensize();

	for (int x = 0; x < cond; x++)
	{
		for (y = 0; y < cond; y++)
		{
			pov->pseudoOrth(C, c, x, y);
			setColorNorm(x, y, C, c);
		}
	}
}

void FractalView::work()
{
	(this->*workModePtr)();
}

void FractalView::setWorkMode(bool gpu_mode)
{
#ifdef USE_CUDA
	if (gpu_mode)
	{
		workModePtr = &FractalView::gpu_work;
	}
	else
	{
#endif
		workModePtr = &FractalView::cpu_work;
#ifdef USE_CUDA
	}
#endif
}

void FractalView::resizeGL()
{
	screen->resizeGLScreen(pov->getScreensize(), pov->getScreensize());
}

void FractalView::setMode(bool gpu_mode)
{
#ifdef USE_CUDA
	if (gpu_mode)
	{
		funPtr = &FractalView::getGPUCoord;
	}
	else
	{
#endif
		funPtr = &FractalView::getCPUCoord;
#ifdef USE_CUDA
	}
#endif
}

Coord FractalView::getCoord(int i)
{
	return (this->*funPtr)(i);
}

Coord FractalView::getCPUCoord(int i)
{
	return tab_h[i];
}

Coord FractalView::getGPUCoord(int i)
{
	Coord temp;
#ifdef	USE_CUDA
	Coord *val;
	cudaMalloc((void**)&val, sizeof(Coord));
	accessGPU(val, i, tab_d);

	cudaMemcpy(&temp, val, sizeof(Coord), cudaMemcpyDeviceToHost);
	cudaFree(val);
#endif
	return temp;
}


void FractalView::setGPU_CPUmode(bool GPUmode)
{
	setRecolorizeMode(GPUmode);
	setWorkMode(GPUmode);
	screen->setPaintMode(GPUmode);
	setMode(GPUmode);
	screen->setResizeMode(GPUmode);
}

void FractalView::updateGL()
{
	screen->updateGL();
}

void FractalView::transfertGPU_CPU(bool GPUmode)
{
#ifdef	USE_CUDA
	if (GPUmode)
	{
		cudaMemcpy(tab_h, tab_d, sizeof(Coord)*pov->getScreensize()*pov->getScreensize(), cudaMemcpyDeviceToHost);
	}
	else
	{
		cudaMemcpy(tab_d, tab_h, sizeof(Coord)*pov->getScreensize()*pov->getScreensize(), cudaMemcpyHostToDevice);
	}
#endif
}

void FractalView::setSemaphore(Semaphore* semaphore)
{
	this->semaphore = semaphore;
}

void FractalView::cpu_work()
{
	semaphore->relNormalSemaStart();
	semaphore->acqNormalSemaEnd();

	semaphore->swap1();

	(this->*drawQualityPtr)();
}

#ifdef USE_CUDA
void FractalView::gpu_work()
{
	uchar3 *dptr = NULL;

	Q_ASSERT(screen->getPixelBuffer());

	// map OpenGL buffer object for writing from CUDA		on a single GPU no data is moved (Win & Linux).		OpenGL should not use this buffer
	HANDLE_ERROR(cudaGLMapBufferObject((void**)&dptr, screen->getPixelBuffer()->bufferId()));
	Q_ASSERT(dptr);
	//printf("Compute\n");

	GPUCompute(dptr);

	HANDLE_ERROR(cudaGLUnmapBufferObject(screen->getPixelBuffer()->bufferId()));  // unmap buffer object
}
#endif

double FractalView::getS()
{
	return s;
}

double FractalView::getMinRadius()
{
	return minRadius;
}

double FractalView::getFixedRadius()
{
	return fixedRadius;
}

double FractalView::getL()
{
	return L;
}
