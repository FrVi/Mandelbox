#include "ThreadPanel.h"
#include "WindowConst.h"
#include "WindowConst2.h"

#include "FractalView.h"

#include "KernelConst.h"

#include "WindowConst.h"
#include "WindowConst2.h"
#include <sstream>

int ThreadPanel::getThread_numbers()
{
	return thread_numbers;
}

void ThreadPanel::incrThread_numbers()
{
	if (thread_numbers < thread_number_max)
	{
		thread_numbers++;
	}

	setStringThread();
}

void ThreadPanel::decrThread_numbers()
{
	if (thread_numbers > 1)
	{
		thread_numbers--;
	}

	setStringThread();
}

void ThreadPanel::setStringThread()
{
	std::string Str = "            Thread number : ";
	std::stringstream out;
	out << thread_numbers;
	Str.append(out.str());
	threadLabel.setText(QString(Str.c_str()));
}

void ThreadPanel::modeCPU(bool checked)
{
	if (checked)
	{
		fview->setGPU_CPUmode(false);
		fview->resizeGL();
		fview->transfertGPU_CPU(false);
		fview->update();
		fview->updateGL();
	}
}

void ThreadPanel::modeGPU(bool checked)
{
	if (checked)
	{
		fview->setGPU_CPUmode(true);
		fview->resizeGL();
		fview->transfertGPU_CPU(true);
		fview->update();
		fview->updateGL();
	}
}


void ThreadPanel::modeHD(bool checked)
{
	if (checked)
	{
		fview->setDrawQuality(false);
		fview->update();
		fview->updateGL();
	}
}

void ThreadPanel::modeLD(bool checked)
{
	if (checked)
	{
		fview->setDrawQuality(true);
		fview->update();
		fview->updateGL();
	}
}

void ThreadPanel::set(FractalView* fview)
{
	this->fview = fview;
	fview->setThreadPanel(this);

	boutonRadio1.setChecked(iniLowQualityMode);
	boutonRadio2.setChecked(!iniLowQualityMode);
	boutonRadio4.setChecked(iniGPUMode);
	boutonRadio3.setChecked(!iniGPUMode);
}

ThreadPanel::ThreadPanel(FractalView* fview) :threadLabel("            Thread number : 2"), plus("+"), moins("-"), boutonRadio1("Fast"), boutonRadio2("Slow"), boutonRadio3("CPU"), boutonRadio4("GPU")
{
	box.addWidget(&boutonRadio1, 0, 0);
	box.addWidget(&boutonRadio2, 1, 0);


	boutonRadio1.setChecked(iniLowQualityMode);
	boutonRadio2.setChecked(!iniLowQualityMode);

	groupe.setLayout(&box);

	groupe.setMaximumSize(94, 80);
	groupe.setMinimumSize(94, 80);

	box2.addWidget(&boutonRadio3, 0, 0);
	box2.addWidget(&boutonRadio4, 1, 0);

	boutonRadio4.setChecked(iniGPUMode);
	boutonRadio3.setChecked(!iniGPUMode);

	groupe2.setLayout(&box2);

	groupe2.setMaximumSize(94, 80);
	groupe2.setMinimumSize(94, 80);

	set(fview);

	setMinimumSize(500, 100);
	setMaximumSize(500, 100);

	setAutoFillBackground(true);
	setPalette(QPalette(CenterPanelColor));

	layout.setContentsMargins(10, 10, 10, 10);
	layout.setSpacing(0);

	plus.setMinimumSize(42, 26);
	plus.setMaximumSize(42, 26);
	moins.setMinimumSize(42, 26);
	moins.setMaximumSize(42, 26);

	setAutoFillBackground(true);
	setPalette(QPalette(background_color));

	layout.addWidget(&groupe, 0, 1, 3, 1);
	layout.addWidget(&groupe2, 0, 0, 3, 1);

	layout.addWidget(&threadLabel, 0, 2);
	layout.addWidget(&plus, 1, 2);
	layout.addWidget(&moins, 2, 2);

	setLayout(&layout);
	thread_numbers = thread_numbers_ini;
	connect(&plus, SIGNAL(clicked()), this, SLOT(incrThread_numbers()));
	connect(&moins, SIGNAL(clicked()), this, SLOT(decrThread_numbers()));
	connect(&boutonRadio1, SIGNAL(toggled(bool)), this, SLOT(modeLD(bool)));
	connect(&boutonRadio2, SIGNAL(toggled(bool)), this, SLOT(modeHD(bool)));
	connect(&boutonRadio3, SIGNAL(toggled(bool)), this, SLOT(modeCPU(bool)));
	connect(&boutonRadio4, SIGNAL(toggled(bool)), this, SLOT(modeGPU(bool)));
}
