#ifndef COORDINFOSET_H
#define COORDINFOSET_H

#include <QHBoxLayout>
#include <QLabel>
#include <QDoubleSpinBox>

#include "Coord.h"
class CoordInfoSet : public QWidget
{
public:
	//CoordInfoSet(std::string name);
	CoordInfoSet();
	void set_text(std::string name);
	Coord get_coord();
	void set(Coord C);
private:
	QLabel nameLabel;
	QDoubleSpinBox infoText1;
	QDoubleSpinBox infoText2;
	QDoubleSpinBox infoText3;
	QHBoxLayout layout;
};

#endif//COORDINFOSET_H
