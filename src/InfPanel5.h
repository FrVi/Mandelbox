#ifndef INFPANEL5_H
#define INFPANEL5_H

#include <QHBoxLayout>

#include "ThreadPanel.h"

class InfPanel5 : public QWidget
{
public:
	InfPanel5(FractalView* fview);

private:
	ThreadPanel threadPanel;
	QHBoxLayout layout;

};

#endif//INFPANEL5_H
