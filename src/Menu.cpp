#include "Menu.h"
#include "Window.h"
#include <iostream>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include "WindowConst.h"
#include "WindowConst2.h"

Menu::Menu(Window *window) :FileMenu("File"), Save_Menu("Save parameters", this),
Load_Menu("Load parameters", this), ImageMenu("Image"), GenerateImageMenu("Generate Image", this),
LoadVideoPointsMenu("Load Video Points", this), SaveVideoPointsMenu("Save Video Points", this),
ResizeScrenMenu("Resize screen", this), FractalTypeMenu("Fractal type", this), AboutUsMenu("About us", this),
AboutThisMenu("About this", this), OptionsMenu("Options"), InformationMenu("Information")

{
	this->window = window;

	setMinimumSize(windows_x_ini, 24);
	setMaximumSize(windows_x_ini, 24);

	addMenu(&FileMenu);
	FileMenu.addAction(&Save_Menu);
	FileMenu.addAction(&Load_Menu);
	addMenu(&ImageMenu);
	ImageMenu.addAction(&GenerateImageMenu);
	ImageMenu.addAction(&LoadVideoPointsMenu);
	ImageMenu.addAction(&SaveVideoPointsMenu);
	addMenu(&OptionsMenu);
	OptionsMenu.addAction(&ResizeScrenMenu);
	OptionsMenu.addAction(&FractalTypeMenu);
	addMenu(&InformationMenu);
	InformationMenu.addAction(&AboutUsMenu);
	InformationMenu.addAction(&AboutThisMenu);

	connect(&Save_Menu, SIGNAL(triggered()), this, SLOT(Save()));
	connect(&Load_Menu, SIGNAL(triggered()), this, SLOT(Load()));
	connect(&GenerateImageMenu, SIGNAL(triggered()), this, SLOT(GenerateImage()));
	connect(&LoadVideoPointsMenu, SIGNAL(triggered()), this, SLOT(LoadVideo()));
	connect(&SaveVideoPointsMenu, SIGNAL(triggered()), this, SLOT(SaveVideo()));
	connect(&ResizeScrenMenu, SIGNAL(triggered()), this, SLOT(ResizeScreen()));
	connect(&FractalTypeMenu, SIGNAL(triggered()), this, SLOT(FractalType()));
	connect(&AboutUsMenu, SIGNAL(triggered()), this, SLOT(AboutUs()));
	connect(&AboutThisMenu, SIGNAL(triggered()), this, SLOT(AboutThis()));

}

Menu::~Menu()
{

}

void Menu::Save()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save a config file", QString(), "*.cfg");

	if (fileName.size() != 0)
	{
		QByteArray ba = fileName.toLocal8Bit();
		window->afficher_config(ba);
	}
}

void Menu::Load()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Save a config file", QString(), "*.cfg");

	if (fileName.size() != 0)
	{
		QByteArray ba = fileName.toLocal8Bit();
		window->charger_config(ba);
	}
}

void Menu::GenerateImage()
{
	//throw 0;//on doit tester le fichier en cas de refus de l'utilisateur...
	QString fichier = QFileDialog::getSaveFileName(this, "Save an image", QString(), "Images (*.png *.gif *.jpg *.jpeg *.bmp)");

	if (fichier.size() != 0)
	{
		std::string name = toUtf8(fichier);
		std::string ext("bmp");
		window->ScreenWriteImage(name, ext);
	}
}

void Menu::LoadVideo()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Open a file", QString(), "*.vid");

	if (fileName.size() != 0)
	{
		FILE* fichier;
		QByteArray ba = fileName.toLocal8Bit();
		const char *c_str = ba.data();

		fichier = fopen(c_str, "r");

		window->load_pile(fichier);

		fclose(fichier);
	}
}

void Menu::SaveVideo()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save a file", QString(), "*.vid");

	if (fileName.size() != 0)
	{
		FILE* fichier;
		QByteArray ba = fileName.toLocal8Bit();
		const char *c_str = ba.data();

		fichier = fopen(c_str, "w");

		window->afficher_pile(fichier);

		fclose(fichier);
	}
}

void Menu::ResizeScreen()
{

	int newSize = QInputDialog::getInt(this, "Resize", "new Size of the screen");

	if (newSize >= 256 && newSize + menu_y + PerfHeight + 100 <= window->size().width())
	{
		window->setScreensize(newSize);
	}
	else
	{
		QMessageBox::critical(this, "Error", "Negative, too big or too small number");
	}
}

void Menu::FractalType()
{
	int reponse = QMessageBox::question(this, "Fractale type selection", "Do you want to display the Mandelbox?", QMessageBox::Yes | QMessageBox::No);

	if (reponse == QMessageBox::Yes)
	{
		window->setMandelbox();
	}
	else if (reponse == QMessageBox::No)
	{
		window->setMandelbulb();
	}

}

void Menu::AboutThis()
{
	QMessageBox::information(this, "AboutThis", about.c_str());
}

void Menu::AboutUs()
{
	QMessageBox::information(this, "AboutUs", about_us.c_str());
}
