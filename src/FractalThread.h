#ifndef FRACTALTHREAD_H
#define FRACTALTHREAD_H

#include <QSemaphore>
#include <QThread>

class FractalView;

class FractalThread;
class NormalThread;
class HighThread;

class Semaphore
{

private:
	NormalThread *normalThreads;
	HighThread *highThreads;
	QSemaphore sNormalStart[2]; 	//Tableau des semaphores de depart.
	QSemaphore sNormalEnd[2];		//Tableau des semaphores de fin.
	QSemaphore sHighStart[2]; 	//Tableau des semaphores de depart.
	QSemaphore sHighEnd[2];		//Tableau des semaphores de fin.

	int idX;	//Indice du groupe de semaphores actif.
	int idX2;	//Indice du groupe de semaphores HighQuality actif.

	FractalView *fview;

public:

	void swap1();
	void swap2();
	~Semaphore();
	Semaphore();
	void set(FractalView *fview);
	void closeThreads();
	void relNormalSemaEnd(int idX);
	void acqNormalSemaStart(int idX);
	void acqNormalSemaEnd();
	void relNormalSemaStart();
	void relHighSemaEnd(int idX);
	void acqHighSemaStart(int idX);
	void acqHighSemaEnd();
	void relHighSemaStart();
	void destruct();
};

class FractalThread : public QThread
{
	//Vue de figure fractale sur laquelle le thread courant travaille.

protected:
	FractalView *fView;

	//Indice de la partie de figure sur laquelle le thread courant travaillera.	
	int i;

	bool continueTh;

public:
	FractalThread();
	void set(FractalView *fView, int i);
	void end();
	//Instructions de travail du thread.
protected:
	virtual void run() = 0;
};

class NormalThread : public FractalThread
{
protected:
	void run();
};

class HighThread : public FractalThread
{
protected:
	void run();
};

#endif//FRACTALTHREAD_H
