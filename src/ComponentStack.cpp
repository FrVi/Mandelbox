#include "ComponentStack.h"
#include "WindowConst.h"
#include "FractalView.h"

#define max(x,y) ((x>y) ? (x) : (y))

using namespace std;

ComponentStack::~ComponentStack()
{
	reset();
}

void ComponentStack::afficher(FILE* fichier)
{
	fprintf(fichier, "%ld\n", resultat.size());

	for (int i = 0; i < resultat.size(); i++)
	{
		resultat.at(i)->afficher(fichier);
	}
}

void ComponentStack::set_time(int duree)
{
	this->duree = max(0, duree);
}

void ComponentStack::setFractalView(FractalView *fview)
{
	this->fview = fview;
}

ComponentStack::ComponentStack(PointOfView *pov/*,Window w*/) :A(3)
{
	this->pov = pov;

	int i;

	size = 0;

	for (i = 0; i < 4; i++)
	{

		coords.push_back(vector<Coord*>());
		coords_a.push_back(vector<Coord*>());
		coords_b.push_back(vector<Coord*>());
	}

	for (i = 0; i < 8; i++)
	{
		doubles.push_back(vector<double>());
		doubles_a.push_back(vector<double>());
		doubles_b.push_back(vector<double>());
	}

	for (i = 0; i < 10; i++)
	{
		ints.push_back(vector<int>());
		ints_a.push_back(vector<double>());
		ints_b.push_back(vector<double>());
	}
}

void ComponentStack::add()
{
	size++;

	Coord* temp;

	temp = new Coord(pov->getPosition());
	coords.at(0).push_back(temp);
	temp = new Coord(pov->getDepthRef());
	coords.at(1).push_back(temp);
	temp = new Coord(pov->getHorizontalRef());
	coords.at(2).push_back(temp);
	temp = new Coord(pov->getVerticalRef());
	coords.at(3).push_back(temp);

	doubles.at(0).push_back(pov->getZoomFactor());
	doubles.at(1).push_back(pov->getLimitDist());
	doubles.at(2).push_back(pov->getNearDist());
	doubles.at(3).push_back(fview->getS());
	doubles.at(4).push_back(fview->getMinRadius());
	doubles.at(5).push_back(fview->getFixedRadius());
	doubles.at(6).push_back(fview->getL());
	doubles.at(7).push_back(fview->getCorrection());

	ints.at(0).push_back(fview->getIncrLimit());

	for (int i = 0; i < 9; i++)
	{
		ints.at(i + 1).push_back(fview->getColors(i));
	}
}

void ComponentStack::deleteElement()
{
	if (size == 0)
		return;
	size--;

	int i;

	for (i = 0; i < 4; i++)
	{
		delete coords.at(i).at(size);
		coords.at(i).pop_back();
	}

	for (i = 0; i < 8; i++)
	{
		doubles.at(i).pop_back();
	}

	for (i = 0; i < 10; i++)
	{
		ints.at(i).pop_back();
	}
}

void ComponentStack::reset_partiel()
{
	int i;
	int j;
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < size; j++)
		{
			delete coords.at(i).at(j);
		}
		coords.at(i).clear();
		coords_a.at(i).clear();
		coords_b.at(i).clear();

	}

	for (i = 0; i < 8; i++)
	{
		doubles_a.at(i).clear();
		doubles_b.at(i).clear();
		doubles.at(i).clear();
	}

	for (i = 0; i < 10; i++)
	{
		ints_a.at(i).clear();
		ints_b.at(i).clear();
		ints.at(i).clear();
	}

	size = 0;
}

void ComponentStack::reset()
{

	reset_partiel();

	int i;

	for (i = 0; i < resultat.size(); i++)
	{
		delete resultat[i];
	}

	resultat.clear();
}

void ComponentStack::initCoord(int j)
{
	int i;

	Matrix dy(size, 1);


	Coord *Dy = new Coord[size];

	Dy[0] = ((*coords.at(j).at(1)) - *coords.at(j).at(0)) * 3;
	Dy[size - 1] = ((*coords.at(j).at(size - 1)) - *coords.at(j).at(size - 2)) * 3;

	for (i = 1; i < size - 1; i++)
	{
		Dy[i] = (*coords.at(j).at(i + 1) - *coords.at(j).at(i - 1)) * 3;
	}

	for (i = 0; i < size; i++)
	{
		dy.set(i, 0, Dy[i].get_x());
	}

	Matrix kx(A*dy);

	for (i = 0; i < size; i++)
	{
		dy.set(i, 0, Dy[i].get_y());
	}

	Matrix ky(A*dy);

	for (i = 0; i < size; i++)
	{
		dy.set(i, 0, Dy[i].get_z());
	}

	Matrix kz(A*dy);

	Coord *D = new Coord[size - 1];

	for (i = 0; i < size - 1; i++)
	{
		D[i] = *coords.at(j).at(i + 1) - *coords.at(j).at(i);
	}

	for (i = 0; i < size - 1; i++)
	{
		Coord *tmp = new Coord();
		tmp->set(kx.at(i, 0) - D[i].get_x(), ky.at(i, 0) - D[i].get_y(), kz.at(i, 0) - D[i].get_z());
		coords_a.at(j).push_back(tmp);
		tmp = new Coord();
		tmp->set(-kx.at(i + 1, 0) + D[i].get_x(), -ky.at(i + 1, 0) + D[i].get_y(), -kz.at(i + 1, 0) + D[i].get_z());
		coords_b.at(j).push_back(tmp);
	}

	delete[] D;
	delete[] Dy;
}

void ComponentStack::initdouble(int j)
{
	int i;

	Matrix dy(size, 1);


	double* Dy = new double[size];

	Dy[0] = (doubles.at(j).at(1) - doubles.at(j).at(0)) * 3;
	Dy[size - 1] = (doubles.at(j).at(size - 1) - doubles.at(j).at(size - 2)) * 3;

	for (i = 1; i < size - 1; i++)
	{
		Dy[i] = (doubles.at(j).at(i + 1) - doubles.at(j).at(i - 1)) * 3;
	}

	for (i = 0; i < size; i++)
	{
		dy.set(i, 0, Dy[i]);
	}

	Matrix k(A*dy);

	double *D = new double[size - 1];

	for (i = 0; i < size - 1; i++)
	{
		D[i] = doubles.at(j).at(i + 1) - doubles.at(j).at(i);
	}

	for (i = 0; i < size - 1; i++)
	{
		doubles_a.at(j).push_back(k.at(i, 0) - D[i]);
		doubles_b.at(j).push_back(-k.at(i + 1, 0) + D[i]);
	}

	delete[] D;
	delete[] Dy;
}

void ComponentStack::initInt(int j)
{
	int i;

	Matrix dy(size, 1);

	double *Dy = new double[size];

	Dy[0] = (double)(ints.at(j).at(1) - ints.at(j).at(0)) * 3;
	Dy[size - 1] = (double)(ints.at(j).at(size - 1) - ints.at(j).at(size - 2)) * 3;

	for (i = 1; i < size - 1; i++)
	{
		Dy[i] = (double)(ints.at(j).at(i + 1) - ints.at(j).at(i - 1)) * 3;
	}

	for (i = 0; i < size; i++)
	{
		dy.set(i, 0, Dy[i]);
	}

	Matrix k(A*dy);

	double* D = new double[size - 1];

	for (i = 0; i < size - 1; i++)
	{
		D[i] = (double)(ints.at(j).at(i + 1) - ints.at(j).at(i));
	}

	for (i = 0; i < size - 1; i++)
	{
		ints_a.at(j).push_back(k.at(i, 0) - D[i]);
		ints_b.at(j).push_back(-k.at(i + 1, 0) + D[i]);
	}

	delete[] D;
	delete[] Dy;
}

//refuser le calcul si on a pas assez de points pour faire une interpolation
void ComponentStack::calcul()
{
	if (size <= 2)
	{
		return;
	}

	int nbre_images = duree * framerate;

	double t;

	int i;

	Matrix B(size, size);

	for (i = 1; i < size - 1; i++)
	{
		B.set(i, i, 4);
		B.set(i + 1, i, 1);
		B.set(i, i + 1, 1);
	}

	B.set(0, 1, 1);
	B.set(1, 0, 1);
	B.set(0, 0, 2);
	B.set(size - 1, size - 1, 2);

	A = Matrix(size);

	initCoord(0);
	initCoord(1);

	for (i = 0; i < 8; i++)
	{
		initdouble(i);
	}

	for (i = 0; i < 10; i++)
	{
		initInt(i);
	}

	for (i = 0; i <= nbre_images; i++)
	{
		t = (double)i / (double)nbre_images;
		Component *temp = new Component(compute(t));

		resultat.push_back(temp);
	}

	reset_partiel();

	int j;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < size; j++)
		{
			delete coords.at(i).at(j);
			delete coords_a.at(i).at(j);
			delete coords_b.at(i).at(j);
		}
		coords.at(i).clear();
		coords_a.at(i).clear();
		coords_b.at(i).clear();
	}
}

int ComponentStack::getSize()
{
	return resultat.size();
}

void ComponentStack::setConfig(int i)
{
	Component C = *(resultat.at(i));

	pov->setPosition(C.getCoord(0));
	pov->setDepthRef(C.getCoord(1));
	pov->setHorizontalRef(C.getCoord(2));
	pov->setVerticalRef(C.getCoord(3));

	pov->setZoomFactor(C.getdouble(0));
	pov->setLimitDist(C.getdouble(1));
	pov->setNearDist(C.getdouble(2));
	fview->setS(C.getdouble(3));
	fview->setMinRadius(C.getdouble(4));
	fview->setFixedRadius(C.getdouble(5));
	fview->setL(C.getdouble(6));
	fview->setCorrection(C.getdouble(7));

	fview->setIncrLimit(C.getInt(0));
	fview->setColors(C.getInt(1), C.getInt(2), C.getInt(3), C.getInt(4), C.getInt(5), C.getInt(6), C.getInt(7), C.getInt(8), C.getInt(9));

	pov->update();
}

Component ComponentStack::compute(double t)
{
	Coord C[4];
	double D[8];
	int I[10];

	int i;

	//positon et regard
	for (i = 0; i < 2; i++)
	{
		C[i] = compute(t, i);
	}

	C[1].normalized();

	//on fait le calcul pour les 2 autres vecteurs pour conserver une base.

	int t2 = (int)(t*(size - 1));

	//cf 	public void ChangePointView(int x, int y, double echelle) dans point of view

	//d3 = d2 - (h.d2)h
	//d3 projete dans le plan (v,d)
	Coord d3 = Coord::normalize(C[1] + (*coords.at(2).at(t2))*(-coords.at(2).at(t2)->scal(C[1])));
	double cosTheta = d3.scal(*coords.at(1).at(t2));
	double sinTheta = d3.scal(*coords.at(3).at(t2));

	C[3] = Coord::normalize((*coords.at(1).at(t2))*(-sinTheta) + ((*coords.at(3).at(t2))*(cosTheta)));
	C[2] = C[3] ^ C[1];


	for (i = 0; i < 8; i++)
	{
		D[i] = getdouble(t, i);
	}

	for (i = 0; i < 10; i++)
	{
		I[i] = getInt(t, i);
	}

	return Component(C, D, I);
}

double ComponentStack::getdouble(double t, int i)
{
	if (t == 1)
	{
		return doubles.at(i).at(size - 1);
	}
	t *= size - 1;

	int intervalle = (int)floor(t);
	double pos = t - (double)intervalle;

	double a = doubles_a.at(i).at(intervalle);
	double b = doubles_b.at(i).at(intervalle);

	return (1 - pos) * doubles.at(i).at(intervalle)
		+ (pos)* doubles.at(i).at(intervalle + 1)
		+ (1 - pos) * (pos)* (a * (1 - pos) + b * (pos));
}

int ComponentStack::getInt(double t, int i)
{
	if (t == 1)
	{
		return ints.at(i).at(size - 1);
	}
	t *= size - 1;

	int intervalle = (int)floor(t);
	double pos = t - (double)intervalle;

	double a = ints_a.at(i).at(intervalle);
	double b = ints_b.at(i).at(intervalle);

	return (int)((1 - pos) * (double)ints.at(i).at(intervalle)
		+ (pos)* (double)ints.at(i).at(intervalle + 1)
		+ (1 - pos) * (pos)* (a * (1 - pos) + b * (pos)));
}

//t dans [0;1]
Coord ComponentStack::compute(double t, int i)
{
	if (t == 1)
	{
		return Coord(*coords.at(i).at(size - 1));
	}

	t *= size - 1;

	int intervalle = (int)floor(t);
	double pos = t - (double)intervalle;

	Coord a = *coords_a.at(i).at(intervalle);
	Coord b = *coords_b.at(i).at(intervalle);

	// (1-t) y_i + t y_i-1 + t(1-t) (a (1-t) + b t)
	return 	(*coords.at(i).at(intervalle))*(1 - pos) + (*coords.at(i).at(intervalle + 1))*(pos)+(a* (1 - pos) + (b*pos))* ((1 - pos) * pos);
}

void ComponentStack::add(const Component &C)
{
	Component* temp = new Component(C);
	resultat.push_back(temp);
}
