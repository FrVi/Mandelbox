#ifndef WINDOWCONST_H
#define WINDOWCONST_H

#include <string>
#include "Coord.h"
#define NOMINMAX
#include <windows.h>
#include <GL/gl.h>

#define screensize_ini   512// Largeur et hauteur de l'ecran d'affichage de la figure.
#define Taille screensize_ini
#define menu_x   220	// Largeur des menus.
#define menu_y   300	//Hauteur des menus.
#define windows_x_ini  1024	//Largeur de la fenetre principale.
#define PerfHeight 50	//Hauteur du panneau de performance
#define windows_y_ini   (screensize_ini+menu_y+PerfHeight+100)	//Hauteur de la fenetre principale.
#define windows_xmin   (3*menu_x+300)	//Largeur minimale de la fenetre principale.
#define windows_ymin  (screensize+menu_y+PerfHeight+100)	//Hauteur minimale de la fenetre principale.
#define nameLabelWidth   120	//Largeur des intitules d'afficheurs d'information.
#define nameLabelHeight   20	//Hauteur des intitules d'afficheurs d'information.
#define infoTextWidth   100	//Largeur des afficheurs d'information.
#define infoTextHeight   20	//Hauteur des afficheurs d'information.
#define digit   8			//Nombre de chiffres affiches apres la virgule
#define time_ini   10
#define framerate   20
#define infopanel_x   windows_xmin-50		//largeur de l'infopanel
#define infopanel_y   300	//hauteur de l'infopanel	

const std::string title = "MandelBox";	//Titre de la fenetre principale.

const bool iniGPUMode = false;
const bool iniLowQualityMode = true;


const std::string fileExtension = "cfg";	//Extension des fichiers de configuration.	

//Information sur les developpeurs
const std::string about_us = "Hamza Chouh and Frederic Vitzikam\n\nStudents of Supelec\n\n2013 Promotion";

//Information sur le programme
const std::string about = "C++ Program made Frederic Vitzikam\n\n\n\nBased on a Software Development Project (by Hamza Chouh and Frederic Vitzikam)\n\nSupervised by Mr Stephane Vialle\n\nMade in Java and Swing\n\nSUPELEC 2011-2012";

#define colorX_x 255
#define colorX_y 255
#define colorX_z 255

#define colorY_x 0
#define colorY_y 0
#define colorY_z 204

#define colorZ_x 0
#define colorZ_y 85
#define colorZ_z 255

#endif//WINDOWCONST_H
