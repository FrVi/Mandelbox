#ifndef POINTOFVIEW_H
#define POINTOFVIEW_H

#include "Matrix3.h"

class Window;

class PointOfView
{

private:

	Window *window;										//Fenetre associee.
	Coord position;										//Position du point de vue.
	Coord verticalRef;									//Coordonnees du vecteur unitaire vertical du plan de vue.
	Coord horizontalRef;								//Coordonnees du vecteur unitaire horizontal du plan de vue. 
	Coord depthRef;										//Coordonnees du vecteur unitaire de la direction de vue. 

	double zoomFactor;		//Facteur de zoom.
	double mvt_fact;
	double rot_fact;
	double limitDist;
	double nearDist;
	bool switch10;			//Booleen indiquant si le systeme est en pas de mouvement multplie.
	bool switch10rot;		//Booleen indiquant si le systeme est en pas de rotation multplie.
	int screensize;
	int halfSize;
	double alpha;

	//void freeTab();
	void initScreenSize(int screensize);


public:

	Coord getCoord(int x, int y);
	void saveToTable(Coord P, int x, int y);
	PointOfView(Window *w);
	~PointOfView();
	int getScreensize();
	double getAlpha();
	void setScreensize(int screensize);
	void set();
	void switchFactor(int a);
	void switchFactorRotation(int a);
	Coord getPosition();
	Coord getVerticalRef();
	double getZoomFactor();
	void setDepthRef(Coord coord);
	void setVerticalRef(Coord coord);
	void setHorizontalRef(Coord coord);
	void setPosition(Coord coord);
	Coord getDepthRef();
	Coord getHorizontalRef();
	double getLimitDist();
	void update();
	void zoom(double factor);
	void back();
	void front();
	void rot(const Matrix3 *M);
	void left();
	void right();
	void down();
	void up();
	void setZoomFactor(double z);
	void pseudoOrth(Coord &C, double &c, int x, int y, Coord *tab);
	void pseudoOrth(Coord &C, double &c, int x, int y, Coord *tab1, Coord *tab2, Coord *tab3, Coord *tab4, Coord *tab5);
	void pseudoOrth(Coord &C, double &c, int x, int y);
	void ChangePointView(int x, int y, double echelle);
	int getHalfSize();
	double getNearDist();
	void setNearDist(double d);
	void setLimitDist(double d);
	//Coord* getCoordTab();//pas propre
};

#endif//POINTOFVIEW_H
