#ifndef INFOPANEL_H
#define INFOPANEL_H

#include <QGridLayout>
#include <QKeyEvent>

#include "CoordInfoSet.h"
#include "DoubleInfoSet.h"

class PointOfView;
class FractalView;
class InfoPanel : public QWidget
{
public:
	InfoPanel(PointOfView* pov, FractalView *fview);
	void setFractalView(FractalView* fview);
	//void Update();
	void getInformations();
	void setInformations();

protected:
	void keyPressEvent(QKeyEvent *e);

private:
	FractalView* fview;
	PointOfView* pov;
	CoordInfoSet coords[4];
	DoubleInfoSet doubles[9];
	QGridLayout layout;
};

#endif//INFOPANEL_H

