
#include <iostream>
#include <QMessageBox>
#include "PointOfView.h"
#include "Screen.h"
#include "WindowConst.h"
using namespace std;

#include <GL/glu.h>
#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <cuda_gl_interop.h>
#endif

Screen::Screen(PointOfView* pov, QWidget *parent) : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
	glError = GL_NO_ERROR;
	this->pov = pov;
	int size = pov->getScreensize();
	setFixedSize(size, size);
	setMouseTracking(true);
	Texture = new GLubyte[4 * size*size];
	//initialisation();
}

void Screen::setFview(FractalView* fview)
{
#ifdef USE_CUDA
	spbo.setFview(fview);
#endif
}

void Screen::resize22(int w, int h)
{
	setFixedSize(w, h);
	resizeGL(w, h);
}

void Screen::initializeGL()
{
	int vmaj = format().majorVersion();
	int vmin = format().minorVersion();
	if (vmaj < 2){
		QMessageBox::warning(this,
			tr("Wrong OpenGL version"),
			tr("OpenGL version 2.0 or higher needed. You have %1.%2, so some functions may not work properly.").arg(vmaj).arg(vmin));
	}
	qDebug() << tr("OpenGL Version: %1.%2").arg(vmaj).arg(vmin);
#ifdef USE_CUDA
	spbo.initCuda();
#endif
	//InitCudaScreenTab(&tab_d,pov->getScreensize());

	glClearColor(0, 0, 0, 1);
	glDisable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
	glDisable(GL_LIGHTING);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glViewport(0, 0, Taille, Taille);
	glEnable(GL_TEXTURE_2D);
#ifdef USE_CUDA
	spbo.resize(Taille, Taille);
#endif
	glMatrixMode(GL_PROJECTION); //Select The Projection Matrix
	glLoadIdentity(); //Reset The Projection Matrix

	glMatrixMode(GL_MODELVIEW); //Select The Modelview Matrix
	glLoadIdentity(); //Reset The Modelview Matrix

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &Nom);
	glDisable(GL_TEXTURE_2D);
}

void Screen::setResizeMode(bool gpu_mode)
{
#ifdef USE_CUDA
	if (gpu_mode)
	{
		resizeModePtr = &Screen::resizeGPU_GL;
	}
	else
	{
#endif
		resizeModePtr = &Screen::resizeCPU_GL;
#ifdef USE_CUDA
	}
#endif
}


void Screen::resizeGLScreen(int w, int h)
{
	(this->*resizeModePtr)(w, h);
}

void Screen::resizeCPU_GL(int w, int h)
{
	glViewport(0, 0, w, h);
	glEnable(GL_TEXTURE_2D);
	if (Texture != 0)
	{
		delete[] Texture;
	}
	Texture = new GLubyte[4 * h*w];
	glMatrixMode(GL_PROJECTION); //Select The Projection Matrix
	glLoadIdentity(); //Reset The Projection Matrix

	glMatrixMode(GL_MODELVIEW); //Select The Modelview Matrix
	glLoadIdentity(); //Reset The Modelview Matrix
}

void Screen::resizeGPU_GL(int w, int h)
{
	glViewport(0, 0, w, h);
	glEnable(GL_TEXTURE_2D);
#ifdef USE_CUDA
	spbo.resize(w, h);
#endif
	glMatrixMode(GL_PROJECTION); //Select The Projection Matrix
	glLoadIdentity(); //Reset The Projection Matrix

	glMatrixMode(GL_MODELVIEW); //Select The Modelview Matrix
	glLoadIdentity(); //Reset The Modelview Matrix
}


void Screen::runCuda()
{
#ifdef USE_CUDA
	spbo.runCuda();
#endif
}

#ifdef USE_CUDA
QGLBuffer* Screen::getPixelBuffer()
{
	return spbo.getPixelBuffer();
}
#endif

void Screen::Paint()
{
	(this->*paintModePtr)();
}

void Screen::setPaintMode(bool gpu_mode)
{
#ifdef USE_CUDA
	if (gpu_mode)
	{
		paintModePtr = &Screen::GPUpaint;
	}
	else
	{
#endif
		paintModePtr = &Screen::CPUpaint;
#ifdef USE_CUDA
	}
#endif
}

void Screen::CPUpaint()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //Select The Modelview Matrix
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);


	int x, y, i;

	int size = pov->getScreensize();

	glBindTexture(GL_TEXTURE_2D, Nom);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, Texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Draw a single Quad with texture coordinates for each vertex.
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);  glVertex3f(-1.0f, -1.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);  glVertex3f(-1.0f, 1.0f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);  glVertex3f(1.0f, 1.0f, -1.0f);
	glTexCoord2f(1.0f, 1.0f);  glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();

}
/*
void Screen::PixelDraw(unsigned int x, unsigned int y, Color &color)
{
glColor3ub(color.r,color.g,color.b);
glVertex2i(x,y);
}
*/


void Screen::GPUpaint()
{
#ifdef USE_CUDA
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //Select The Modelview Matrix
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	//spbo.runCuda(); //run CUDA kernel
	spbo.bind();  // now bind buffer after cuda is done

	// Draw a single Quad with texture coordinates for each vertex.
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);  glVertex3f(-1.0f, -1.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);  glVertex3f(-1.0f, 1.0f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);  glVertex3f(1.0f, 1.0f, -1.0f);
	glTexCoord2f(1.0f, 1.0f);  glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();

	spbo.release();

	glError = glGetError();
#endif
}

void Screen::paintGL()
{
	Paint();
}

void Screen::mousePressEvent(QMouseEvent *event)
{
	printf("%d, %d\n", event->x(), event->y());

	float echelle = 0.5;//zoom
	if (event->buttons() == Qt::RightButton)//clic droit
	{
		echelle = 2;
	}
	else if (event->buttons() == Qt::MidButton)//clic centrale
	{
		echelle = 1;
	}

	pov->ChangePointView(event->x(), event->y(), echelle);
	pov->update();
}
void Screen::mouseMoveEvent(QMouseEvent *event)
{
}

void Screen::keyPressEvent(QKeyEvent* event) {
	switch (event->key()) {
	case Qt::Key_Escape:
		close();
		break;
	default:
		event->ignore();
		break;
	}
}

Screen::~Screen()
{
	glDeleteTextures(1, &Nom);

	if (Texture != 0)
	{
		delete[] Texture;
	}
}

void Screen::setPixel(int x, int y, int r, int g, int b)
{
	int s = pov->getScreensize();
	int i = 4 * (x + y*s);

	Texture[i] = min(r, 255);
	Texture[i + 1] = min(g, 255);
	Texture[i + 2] = min(b, 255);
	Texture[i + 3] = 255;
}

void Screen::display()
{
	int size = pov->getScreensize();
	glDrawPixels(size, size, GL_RGBA, GL_UNSIGNED_BYTE, 0);
}

void Screen::writeImage(std::string path, std::string type)
{
	glReadBuffer(GL_FRONT);
	QImage screenshot = grabFrameBuffer(true);
	screenshot.save(path.c_str(), type.c_str(), -1);
}
