#include <algorithm>
#include "MandelboxView.h"
#include "MandelbulbView.h"
#include "KernelPBO.h"
#include "KernelConst.h"

#ifndef USE_CUDA
#define __device__ 
#define __host__ 
#endif

#include <stdio.h>

#define NOMINMAX
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda_gl_interop.h>
#endif

#ifdef	USE_CUDA
__device__ int INCRLIMIT;
__device__ int SCREENSIZE;
__device__ int HALF;
__device__ int COLORA;
__device__ int COLORB;
__device__ int COLORC;
__device__ int COLORD;
__device__ int COLORE;
__device__ int COLORF;
__device__ int COLORG;
__device__ int COLORH;
__device__ int COLORI;

__device__ float INFINI_X;
__device__ float INFINI_Y;
__device__ float INFINI_Z;
__device__ float S;  
__device__ float ALPHA;
__device__ float LIM;
__device__ float MR2D;
__device__ float FR2D;
__device__ float L2D;
__device__ float CORRECTION;
__device__ float FR2MR2;

const int thPerBlock = 256;

__global__ void GPUMandelbulbKernelGenerique(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbGenerique(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelHuit(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbHuit(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelSept(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbSept(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelCinq(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbCinq(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelQuatre(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbQuatre(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelDeux(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbulbDeux(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUMandelbulbKernelGeneriqueHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbGenerique(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelbulbKernelHuitHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbHuit(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelbulbKernelSeptHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbSept(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelbulbKernelCinqHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbCinq(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelbulbKernelQuatreHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbQuatre(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelbulbKernelDeuxHD(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbulbDeux(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}
#endif

void PointOfView::initScreenSize(int screensize)
{
	this->screensize = screensize;

	halfSize = screensize / 2;
	alpha = tan35 / screensize;

	float alphad = (float)alpha;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(ALPHA, &alphad, sizeof(float));
	cudaMemcpyToSymbol(HALF, &halfSize, sizeof(int));
	cudaMemcpyToSymbol(SCREENSIZE, &screensize, sizeof(int));
#endif
}

void PointOfView::setLimitDist(double d)
{
	limitDist = d;
	float lim = (float)limitDist;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(LIM, &lim , sizeof(float));
#endif
}


void FractalView::setColors(int a, int b, int c, int d, int e, int f, int g, int h, int i)
{
	colorA = std::min(255, std::max(a, 0));
	colorB = std::min(255, std::max(b, 0));
	colorC = std::min(255, std::max(c, 0));
	colorD = std::min(255, std::max(d, 0));
	colorE = std::min(255, std::max(e, 0));
	colorF = std::min(255, std::max(f, 0));
	colorG = std::min(255, std::max(g, 0));
	colorH = std::min(255, std::max(h, 0));
	colorI = std::min(255, std::max(i, 0));

	colorize_function();
}


void FractalView::setIncrLimit(int incrLimit)
{
	this->incrLimit = incrLimit;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(INCRLIMIT, &incrLimit, sizeof(int));
#endif
}

void FractalView::setS(double s)
{
	this->s = s;
	float s2 = (float)s;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(S, &s2, sizeof(float));
#endif
	define();
}

void FractalView::setMinRadius(double  minRadius)
{
	this->minRadius = minRadius;
	mR2 = minRadius *minRadius;
	fR2mR2 = fR2 / mR2;

	float mR2d = (float)mR2;
	float fR2mR2d = (float)fR2mR2;

#ifdef	USE_CUDA
	cudaMemcpyToSymbol(MR2D, &mR2d, sizeof(float));
	cudaMemcpyToSymbol(FR2MR2, &fR2mR2d, sizeof(float));
#endif
}

void FractalView::setFixedRadius(double fixedRadius)
{
	this->fixedRadius = fixedRadius;
	fR2 = fixedRadius * fixedRadius;
	fR2mR2 = fR2 / mR2;

	float fR2d = (float)fR2;
	float fR2mR2d = (float)fR2mR2;

#ifdef	USE_CUDA
	cudaMemcpyToSymbol(FR2D, &fR2d, sizeof(float));
	cudaMemcpyToSymbol(FR2MR2, &fR2mR2d, sizeof(float));
#endif
}

//Fixe la correction de l'estimateur de distance apartir d'une valeur passee en argument
void FractalView::setCorrection(double correction)
{
	this->correction = correction;

	float correctiond = (float)correction;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(CORRECTION, &correctiond, sizeof(float));
#endif
}

void FractalView::setL(double L)
{
	this->L = L;
	float L2 = (float)(L*L);
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(L2D, &L2, sizeof(float));
#endif
}

__host__ __device__ int fun2(int colorA, double x)
{
	x = abs(x);

	if (1 > x)
	{
		return (int)(x * colorA);
	}
	else
	{
		return colorA;
	}
}

void FractalView::colorize_function()
{
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(COLORA, &colorA, sizeof(int));
	cudaMemcpyToSymbol(COLORB, &colorB, sizeof(int));
	cudaMemcpyToSymbol(COLORC, &colorC, sizeof(int));
	cudaMemcpyToSymbol(COLORD, &colorD, sizeof(int));
	cudaMemcpyToSymbol(COLORE, &colorE, sizeof(int));
	cudaMemcpyToSymbol(COLORF, &colorF, sizeof(int));
	cudaMemcpyToSymbol(COLORG, &colorG, sizeof(int));
	cudaMemcpyToSymbol(COLORH, &colorH, sizeof(int));
	cudaMemcpyToSymbol(COLORI, &colorI, sizeof(int));
#endif
}

void FractalView::GPU_const_init()
{
	float lim = (float)pov->getLimitDist();
	float i_x = (float)Infini_x;
	float i_y = (float)Infini_y;
	float i_z = (float)Infini_z;
	int half = pov->getHalfSize();
	int screensize = pov->getScreensize();
	float alpha = (float)pov->getAlpha();
	float sd = (float)s;
	float mR2d = (float)mR2;
	float fR2mR2d = (float)fR2mR2;
	float fR2d = (float)fR2;
	float correctiond = (float)correction;
	float L2d = (float)L*L;
#ifdef	USE_CUDA
	cudaMemcpyToSymbol(ALPHA, &alpha, sizeof(float));
	cudaMemcpyToSymbol(FR2MR2, &fR2mR2d, sizeof(float));
	cudaMemcpyToSymbol(LIM, &lim, sizeof(float));
	cudaMemcpyToSymbol(MR2D, &mR2d, sizeof(float));
	cudaMemcpyToSymbol(FR2D, &fR2d, sizeof(float));
	cudaMemcpyToSymbol(L2D, &L2d, sizeof(float));
	cudaMemcpyToSymbol(CORRECTION, &correctiond, sizeof(float));
	cudaMemcpyToSymbol(INFINI_X, &i_x, sizeof(float));
	cudaMemcpyToSymbol(INFINI_Y, &i_y, sizeof(float));
	cudaMemcpyToSymbol(INFINI_Z, &i_z, sizeof(float));
	cudaMemcpyToSymbol(S, &sd, sizeof(float));
	cudaMemcpyToSymbol(INCRLIMIT, &incrLimit, sizeof(float));
	cudaMemcpyToSymbol(HALF, &half, sizeof(int));
	cudaMemcpyToSymbol(SCREENSIZE, &screensize, sizeof(int));
	cudaMemcpyToSymbol(COLORA, &colorA, sizeof(int));
	cudaMemcpyToSymbol(COLORB, &colorB, sizeof(int));
	cudaMemcpyToSymbol(COLORC, &colorC, sizeof(int));
	cudaMemcpyToSymbol(COLORD, &colorD, sizeof(int));
	cudaMemcpyToSymbol(COLORE, &colorE, sizeof(int));
	cudaMemcpyToSymbol(COLORF, &colorF, sizeof(int));
	cudaMemcpyToSymbol(COLORG, &colorG, sizeof(int));
	cudaMemcpyToSymbol(COLORH, &colorH, sizeof(int));
	cudaMemcpyToSymbol(COLORI, &colorI, sizeof(int));
#endif
}

#ifdef USE_CUDA
__device__ void set(uchar3 *devPtr,int i,int c1,int c2,int c3)
{
	devPtr[i].x = (int)fminf(255,c1);
	devPtr[i].y = (int)fminf(255,c2);
	devPtr[i].z = (int)fminf(255,c3);
}
#endif

#define PI 3.1415926535897932f
#ifdef USE_CUDA
void checkCUDAError(const char *msg) {
	cudaError_t err = cudaGetLastError();
	if( cudaSuccess != err) {
		fprintf(stderr, "Cuda error: %s: %s.\n", msg, cudaGetErrorString( err) ); 
		exit(EXIT_FAILURE); 
	}
} 
#endif

__host__ __device__ Color::Color(int r, int g, int b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

//  Renvoie le produit scalaire du vecteur courant avec le vecteur de coordonnees passe en argument.
__host__ __device__ float Coord::scal(const Coord &A) const
{
	return (A.x*x + A.y*y + A.z*z);
}

__host__ __device__ int Coord::Round(int colorA, int colorD, int colorG) const
{
	return (fun2(colorA, x) + fun2(colorD, y) + fun2(colorG, z)) / 3;
}


//Normalise le vecteur
__host__ __device__ void Coord::normalized()
{
#if !defined(__CUDA_ARCH__) // Host code path
	float norm = sqrt(x*x + y*y + z*z);
#else// Device code path
	float norm = sqrtf(x*x+y*y+z*z);
#endif

	if (norm != 0)
	{
		x /= norm;
		y /= norm;
		z /= norm;
	}
}

// Fixe la coordonnee courante a� partir d'un triplet de flottants.
__host__ __device__ void Coord::set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

//Constructeur generique.
__host__ __device__ Coord::Coord()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

// Renvoie le produit scalaire du vecteur passe en argument avec le vecteur courant. Si celui-ci est inferieur a� une valeur minimale fixee, cette valeur minimale est renvoyee.
__host__ __device__ float Coord::modifiedScal(const Coord &A) const
{
	float u = (A.x*x + A.y*y + A.z*z);
	if (u > scalLimit)
		return u;
	else return scalLimit; //Cette condition est la� pour palier au probleme des traits noirs que l'on observe sans elle.
}

//  Renvoye la pseudo normale au point X, en considerant les quatres trianlges au'il forme avec ses voisins immediats
__host__ __device__ void Coord::pseudoOrth(const Coord &X1, const Coord &X2, const  Coord &X3, const  Coord &X4)
{
	float x1 = X1.x;
	float y1 = X1.y;
	float z1 = X1.z;

	float x2 = X2.x;
	float y2 = X2.y;
	float z2 = X2.z;

	float x3 = X3.x;
	float y3 = X3.y;
	float z3 = X3.z;

	float x4 = X4.x;
	float y4 = X4.y;
	float z4 = X4.z;

	float x_s = (y2 - y4) * (z1 - z3) + (y3 - y1) * (z2 - z4);
	float y_s = (z2 - z4) * (x1 - x3) + (z3 - z1) * (x2 - x4);
	float z_s = (x2 - x4) * (y1 - y3) + (x3 - x1) * (y2 - y4);

#if !defined(__CUDA_ARCH__) // Host code path
	float inv_norme = 1 / sqrt(x_s* x_s + y_s* y_s + z_s* z_s);
#else// Device code path
	float inv_norme = 1/sqrtf(x_s* x_s+ y_s* y_s+ z_s* z_s);
#endif

	set(x_s*inv_norme, y_s*inv_norme, z_s*inv_norme);
}

// Construit un nouveau set de coordonnees a� partir des coordonnees a� enregistrer.
__host__ __device__ Coord::Coord(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

// Construit un nouveau set de coordonnees en copiant une coordonnee C passee en argument.
__host__ __device__ Coord::Coord(const Coord &C)
{
	*this = C;
}

// Methode permettant d'ajouter une coordonnee a� la coordonnee courante.
__host__ __device__ void Coord::operator+=(const Coord &A)
{
	x += A.x;
	y += A.y;
	z += A.z;
}

// Multiplie la coordonnee courante par un scalaire.
__host__ __device__ void Coord::operator*=(float a)
{
	x *= a;
	y *= a;
	z *= a;
}

// Fixe la coordonnee courante a� partir d'une coordonnee passee en argument.
__host__ __device__ void Coord::operator=(const Coord &C)
{
	x = C.x;
	y = C.y;
	z = C.z;
}

// Renvoie la norme du vecteur de coordonnees courant.
__host__ __device__ float Coord::norm() const
{
#if !defined(__CUDA_ARCH__) // Host code path
	return sqrt(x*x + y*y + z*z);
#else// Device code path
	return sqrtf(x*x+y*y+z*z);
#endif
}

// Renvoie la norme au carre du vecteur de coordonnees courant.
__host__ __device__ float Coord::norm2() const
{
	return x*x + y*y + z*z;
}

// Effectue un pliage de la coordonnee.
__host__ __device__ void Coord::bendThis()
{
	if (x > 1)
		x = 2 - x;
	else if (x < -1)
		x = -2 - x;

	if (y > 1)
		y = 2 - y;
	else if (y < -1)
		y = -2 - y;

	if (z > 1)
		z = 2 - z;
	else if (z < -1)
		z = -2 - z;
}


// Ajoute les deux coordonnees passees en argument.
__host__ __device__  Coord operator+(Coord const& A, Coord const& B)
{
	Coord tmp;

	tmp.set(A.x + B.x, A.y + B.y, A.z + B.z);
	return tmp;
}

__host__ __device__  Coord operator-(Coord const& A, Coord const& B)
{
	Coord tmp;

	tmp.set(A.x - B.x, A.y - B.y, A.z - B.z);
	return tmp;
}

//  Multiplie la coordonnee D par le scalaire passe en argument.
__host__ __device__ Coord operator*(Coord const& A, float a)
{
	Coord tmp;
	tmp.set(a*A.x, a*A.y, a*A.z);
	return tmp;
}

// Ajoute v*H  a la coordonnee
__host__ __device__ void Coord::MultiAddToThis(const Coord &H, float v)
{
	x += H.x*v;
	y += H.y*v;
	z += H.z*v;
}

Color Coord::toColor() const
{
	return Color((int)x, (int)y, (int)z);
}

// Renvoie la valeur de la coordonnee x.
__host__ __device__ float Coord::get_x() const
{
	return x;
}

// Renvoie la valeur de la coordonnee y.
__host__ __device__ float Coord::get_y() const
{
	return y;
}

// Renvoie la valeur de la coordonnee z.
__host__ __device__ float Coord::get_z() const
{
	return z;
}

// Fixe la valeur de la coordonnee x.
__host__ __device__ void Coord::set_x(float x)
{
	this->x = x;
}

// Fixe la valeur de la coordonnee y.
__host__ __device__ void Coord::set_y(float y)
{
	this->y = y;
}

// Fixe la valeur de la coordonnee z.
__host__ __device__ void Coord::set_z(float z)
{
	this->z = z;
}

#ifdef	USE_CUDA
__global__ void	initia(Coord pos,Coord *positions, Coord *normales,Coord *horizontales,Coord *verticales,Coord *directions,Coord H, Coord V)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;

	Coord N,D;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{	
			if(q < SCREENSIZE)
			{
				i = p+q*SCREENSIZE;

				directions[i] = positions[i] - pos;
				directions[i].normalized();

				N = normales[i];
				D = directions[i];

				horizontales[i] = H + N*(H.scal(N)*(-2));
				verticales[i] = V + N*(V.scal(N)*(-2));
				directions[i] = D + N*(D.scal(N)*(-2));
			}
		}
	}
}

__device__ void pseudoOrth(Coord &C, float &c, int x, int y, Coord *tab1,Coord *tab2,Coord *tab3,Coord *tab4,Coord *tab5,Coord depthRef)
{	
	if(tab1[x+y*SCREENSIZE].get_x()== Infini_x && tab1[x+y*SCREENSIZE].get_y()== Infini_y && tab1[x+y*SCREENSIZE].get_z()== Infini_z)
	{
		c = -1;
		return;
	}

	int x1=x-1;
	int x2=x;
	int x3=x+1;

	int y1=y-1;
	int y2=y;
	int y3=y+1;

	//Bords de l'ecran
	if(x==0 || y==0 || y==SCREENSIZE-1 || x==SCREENSIZE-1)
	{
		if(x==0)
			x1 = x;
		else if(x==SCREENSIZE-1)
			x3 = x;
		if(y==0)
			y1 = y;
		else if(y==SCREENSIZE-1)
			y3 = y;
	}
	//Cas Generique
	C.pseudoOrth(tab3[x1+y2*SCREENSIZE],tab4[x2+y3*SCREENSIZE],tab2[x3+y2*SCREENSIZE],tab5[x2+y1*SCREENSIZE]);

	c = depthRef.modifiedScal(C); //fait une copie mais ne change rien en terme de vitesse en vrai
	c = max(0.0,c);

	c*= 1.7;
}

__device__ void setColorNorm(const Coord &X, float c, Color &Y)
{
	if(c == -1)
	{
		Y = Color(0,0,0);
		return;
	}

	int c1 = (int)(c*(X.Round(COLORA,COLORD,COLORG)));
	int c2 = (int)(c*(X.Round(COLORB,COLORE,COLORH)));
	int c3 = (int)(c*(X.Round(COLORC,COLORF,COLORI)));
	Y = Color(c1,c2,c3);
}

__device__ void setColorNorm(int i,const Coord &X, float c,const Color &Y,uchar3 *devPtr)
{
	if(c == -1)
	{
		devPtr[i].x = 255;
		devPtr[i].y = 255;
		devPtr[i].z = 255;
		return;
	}

	int c1 = (int)((c*X.Round(COLORA,COLORD,COLORG)));
	int c2 = (int)((c*X.Round(COLORB,COLORE,COLORH)));
	int c3 = (int)((c*X.Round(COLORC,COLORF,COLORI)));

	float alp = 0.7f;
	float bet= 1.0f - alp;

	if(Y.r == 255 && Y.g == 255 && Y.b== 255)
	{
		set(devPtr,i,c1,c2,c3);
	}
	else
	{
		c1 = c1*alp + Y.r*bet;
		c2 = c2*alp + Y.g*bet;
		c3 = c3*alp + Y.b*bet;

		set(devPtr,i,c1,c2,c3);
	}	
}

__device__ void setColorNorm(int i,const Coord &X, float c,const Color &Y0,const Color &Y1,const Color &Y2,const Color &Y3,const Color &Y4,uchar3 *devPtr)
{
	if(c == -1)
	{
		devPtr[i].x = 255;
		devPtr[i].y = 255;
		devPtr[i].z = 255;
		return;
	}

	int c1 = (int)((c*X.Round(COLORA,COLORD,COLORG)));
	int c2 = (int)((c*X.Round(COLORB,COLORE,COLORH)));
	int c3 = (int)((c*X.Round(COLORC,COLORF,COLORI)));

	float alp = 0.7f;
	float bet= 1.0f - alp;

	Color Y( Y0.r + Y1.r + Y2.r + Y3.r + Y4.r,
		Y0.g + Y1.g + Y2.g + Y3.g + Y4.g,
		Y0.b + Y1.b + Y2.b + Y3.b + Y4.b);

	int norme[5];

	norme[0] = Y0.r+Y0.g+Y0.b;
	norme[1] = Y1.r+Y1.g+Y1.b;
	norme[2] = Y2.r+Y2.g+Y2.b;
	norme[3] = Y3.r+Y3.g+Y3.b;
	norme[4] = Y4.r+Y4.g+Y4.b;

	float nbre = 0;

	for(int k = 0; k < 5; k++)
	{
		if(norme[k] !=0)
		{
			nbre++;
		}
	}

	if(Y.r == 0 && Y.g == 0 && Y.b== 0)
	{
		set(devPtr,i,c1,c2,c3);
	}
	else
	{
		c1 = c1*alp + (Y.r/nbre)*bet;
		c2 = c2*alp + (Y.g/nbre)*bet;
		c3 = c3*alp + (Y.b/nbre)*bet;

		set(devPtr,i,c1,c2,c3);
	}	
}

void GPUColorSettingKernelLauncher(dim3 numBlocks2,dim3 threadsPerBlock2,Coord *normales_d,Coord *normales2_d,float *doubleTab_d,float *doubleTab2_d,Color *ColorTab_d,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr)
{
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 0);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 0);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 0);

	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 1);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 1);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 1);

	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 2);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 2);
	GPUColorSettingKernel<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 2);
}

void GPUColorSettingKernelLauncher2(dim3 numBlocks2,dim3 threadsPerBlock2,Coord *normales_d,Coord *normales2_d,float *doubleTab_d,float *doubleTab2_d,Color *ColorTab_d,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr)
{
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 0);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 0);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 0);

	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 1);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 1);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 1);

	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,0, 2);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,1, 2);
	GPUColorSettingKernel2<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr,2, 2);
}

__global__ void	GPUColorSettingKernel(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr,int dec_x, int dec_y)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x*3 + dec_x; 
	int q = blockIdx.y*3 * blockDim.y + threadIdx.y + dec_y; 
	int i;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pseudoOrth(normales2[i],doubleTab2[i],p,q,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D);//construit les pseudo-normales a partir de tab1-tab5 
			//et les place dans normales2 et les coefficients c dans doubleTab2

			//test(i,normales2[i],devPtr);
			setColorNorm(normales2[i],doubleTab2[i],ColorTab[i]);//place la couleur resultante dans ColorTab

			if(q == 0 || q == SCREENSIZE-1 ||p == 0 || p == SCREENSIZE-1)//tres mal, on ralentit bcp de groupe de thread juste pour les bords...
			{
				setColorNorm(i,normales[i],doubleTab[i],ColorTab[i],devPtr);//fait la combinaison du reflet et de l'image simple.	
			}
		}
	}
}


__global__ void	GPUColorSettingKernel2(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,Coord *tab_2d1,Coord *tab_2d2,Coord *tab_2d3,Coord *tab_2d4,Coord *tab_2d5,Coord D,uchar3 *devPtr,int dec_x, int dec_y)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x*3 + dec_x; 
	int q = blockIdx.y*3 * blockDim.y + threadIdx.y + dec_y; 
	int i;

	if(p < SCREENSIZE-1 && p > 0)//p > 0: solution vite faite, il faudrait mieux adapter les dimensions de la grid pour que les bords soit pas dedans...
	{
		if (q < SCREENSIZE-1 && q > 0)
		{
			i= p+q*SCREENSIZE;

			setColorNorm(i,normales[i],doubleTab[i],ColorTab[i],ColorTab[i+1],ColorTab[i-1],ColorTab[i+SCREENSIZE],ColorTab[i-SCREENSIZE],devPtr);//fait la combinaison du reflet et de l'image simple.	
		}
	}
}


__global__ void GPUMandelboxKernelHD2(float NearDist,Coord *horizontales, Coord *verticales, Coord *directions, Coord *positions, Coord *tab,int a, int b)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	Coord P;

	Coord H_a,D_a,V_a,pos2;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			pos2 = positions[i]+directions[i]*(2.00*NearDist);

			H_a=horizontales[i]*ALPHA;
			V_a=verticales[i]*ALPHA;
			D_a=directions[i];

			D_a+=H_a*a;
			D_a+=V_a*b;

			d=0;
			P=pos2;//pour eviter que l'algo soit terminer directement: on serait deja dans la zone

			while(getIncrementMandelbox(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
			{
				d+=CORRECTION*distance;

				P = pos2;
				P.MultiAddToThis(D_a,d);
			}
			//Enregistrement des coordonnees du point courant.
			if(d<LIM)
				tab[i] = P;
			else
				tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
		}
	}
}

__global__ void GPUMandelboxKernelHD(float NearDist,Coord H, Coord V, Coord D, Coord pos, Coord *tab)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x; 
	int q = blockIdx.y * blockDim.y*pointsPerThread+ threadIdx.y;
	int i;
	float distance;

	float d;

	H*=(p - HALF)*ALPHA;
	Coord Va;
	Coord P;

	Va = V;
	Va*=(q-HALF)*ALPHA;

	D+=H;
	D+=Va;

	int k = 0;

	if(p < SCREENSIZE)
	{
		for(int j= 0; j<pointsPerThread; j++)
		{		
			if (q < SCREENSIZE)
			{
				i= p+q*SCREENSIZE;

				P = pos;

				d=0;

				while(getIncrementMandelbox(NearDist,P, distance) != INCRLIMIT && d<LIM)//distance[i] est modifie
				{
					k++;
					d+=CORRECTION*distance;

					//P = pos + (D + x_a*H + y_a*V) * d
					P = pos;
					P.MultiAddToThis(D,d);
				}
				//Enregistrement des coordonnees du point courant.
				if(d<LIM)//distance si marche plus comme ca....
				{
					tab[i] = P;
				}
				else
				{
					tab[i].set(INFINI_X,INFINI_Y,INFINI_Z);	
				}
			}
			q++;
			D+=V;
		}
	}
}

__global__ void GPUColorKernel(Coord D2, Coord* tab_d, uchar3* devPtr,int dec_x, int dec_y)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x*3 + dec_x; 
	int y = blockIdx.y*3 * blockDim.y + threadIdx.y + dec_y; 

	float c;
	Coord C; 

	if(x < SCREENSIZE && y < SCREENSIZE)
	{
		int i = x+y*SCREENSIZE;

		if(tab_d[i].get_x()== Infini_x && tab_d[i].get_y()== Infini_y && tab_d[i].get_z()== Infini_z)
		{
			devPtr[i].x = 255;
			devPtr[i].y = 255;
			devPtr[i].z = 255;
		}
		else
		{
			int x1=x-1;
			int x2=x;
			int x3=x+1;

			int y1=y-1;
			int y2=y;
			int y3=y+1;

			//Bords de l'ecran
			if(x==0 || y==0 || y==SCREENSIZE-1 || x==SCREENSIZE-1)
			{
				if(x==0)
					x1 = x;
				else if(x==SCREENSIZE-1)
					x3 = x;
				if(y==0)
					y1 = y;
				else if(y==SCREENSIZE-1)
					y3 = y;
			}
			//Cas Generique
			C.pseudoOrth(tab_d[x1+y2*SCREENSIZE],tab_d[x2+y3*SCREENSIZE],tab_d[x3+y2*SCREENSIZE],tab_d[x2+y1*SCREENSIZE]);

			c=1.7*fmaxf(0.0f, D2.modifiedScal(C));

			set(devPtr,x+y*SCREENSIZE,(int)(c*(C.Round(COLORA,COLORD,COLORG))),
				(int)(c*(C.Round(COLORB,COLORE,COLORH)))
				,(int)(c*(C.Round(COLORC,COLORF,COLORI))));
		}
	}
}

__global__ void GPUColorKernelHD(Coord D2, Coord* tab_d, uchar3* devPtr,int dec_x, int dec_y,Coord* normales,float *doubleTab)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x*3 + dec_x; 
	int y = blockIdx.y*3 * blockDim.y + threadIdx.y + dec_y; 

	float c;
	Coord C; 

	if(x < SCREENSIZE && y < SCREENSIZE)
	{
		int i = x+y*SCREENSIZE;

		if(tab_d[i].get_x()== Infini_x && tab_d[i].get_y()== Infini_y && tab_d[i].get_z()== Infini_z)
		{
			devPtr[i].x = 255;
			devPtr[i].y = 255;
			devPtr[i].z = 255;
			doubleTab[i] =-1;
		}
		else
		{
			int x1=x-1;
			int x2=x;
			int x3=x+1;

			int y1=y-1;
			int y2=y;
			int y3=y+1;

			//Bords de l'ecran
			if(x==0 || y==0 || y==SCREENSIZE-1 || x==SCREENSIZE-1)
			{
				if(x==0)
					x1 = x;
				else if(x==SCREENSIZE-1)
					x3 = x;
				if(y==0)
					y1 = y;
				else if(y==SCREENSIZE-1)
					y3 = y;
			}
			//Cas Generique
			C.pseudoOrth(tab_d[x1+y2*SCREENSIZE],tab_d[x2+y3*SCREENSIZE],tab_d[x3+y2*SCREENSIZE],tab_d[x2+y1*SCREENSIZE]);

			normales[x+y*SCREENSIZE] = C;

			doubleTab[x+y*SCREENSIZE] = D2.modifiedScal(C); //fait une copie mais ne change rien en terme de vitesse en vrai
			doubleTab[x+y*SCREENSIZE] = fmaxf(0.0f,doubleTab[x+y*SCREENSIZE]);

			doubleTab[x+y*SCREENSIZE]=1.7*doubleTab[x+y*SCREENSIZE];

			c= doubleTab[x+y*SCREENSIZE];

			set(devPtr,x+y*SCREENSIZE,(int)(c*(C.Round(COLORA,COLORD,COLORG))),
				(int)(c*(C.Round(COLORB,COLORE,COLORH)))
				,(int)(c*(C.Round(COLORC,COLORF,COLORI))));
		}
	}
}

__device__ int getIncrementMandelbox(float NearDist,const Coord &P, float &distance) 
{
	float R2;
	Coord P2;
	P2 = P;
	float DEfactor = S;
	int incr = 0;

	while(incr < INCRLIMIT) // Tant que la limite d'incrementation n'est pas atteinte
	{
		P2.bendThis();
		R2=P2.norm2();
		if(R2<MR2D)
		{
			P2*=FR2MR2;
			DEfactor *=  FR2MR2;
		}
		else if(R2<FR2D)//bug si fR2 meme si fr2 vaut 1.0
		{
			P2*=FR2D/R2;
			DEfactor *= FR2D / R2;
		}
		//P2 = P2*s +P
		P2*=S;
		P2+=P;
		DEfactor *= S;
		R2=P2.norm2();
		if(R2>L2D)
		{
			distance = P.norm()/fabsf(DEfactor);
			if(distance <= NearDist)//NearDist mais il veut pas parce que trop de variable deja, il gere pas.
				incr = INCRLIMIT;
			return incr;
		}
		incr++;
	}	
	distance = P.norm()/fabsf(DEfactor);
	return incr;
}


__global__ void	GPURecolorizeKernelHD(Coord *normales,Coord *normales2,float *doubleTab,float *doubleTab2,Color *ColorTab,uchar3 *devPtr,int dec_x, int dec_y)
{
	int p = blockIdx.x * blockDim.x + threadIdx.x*3 + dec_x; 
	int q = blockIdx.y*3 * blockDim.y + threadIdx.y + dec_y; 
	int i;

	if(p < SCREENSIZE)
	{
		if (q < SCREENSIZE)
		{
			i= p+q*SCREENSIZE;

			//pseudoOrth(normales2[i],doubleTab2[i],p,q,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D);//construit les pseudo-normales a partir de tab1-tab5 
			//et les place dans normales2 et les coefficients c dans doubleTab2

			//test(i,normales2[i],devPtr);
			setColorNorm(normales2[i],doubleTab2[i],ColorTab[i]);//place la couleur resultante dans ColorTab
			setColorNorm(i,normales[i],doubleTab[i],ColorTab[i],devPtr);//fait la combinaison du reflet et de l'image simple.


		}
	}
}

void FractalView::recolorizeGPU(uchar3 *devPtr)
{
	int screensize=pov->getScreensize();
	Coord D2 = pov->getDepthRef();

	dim3 threadsPerBlock2(thPerBlock/3);
	dim3 numBlocks2(screensize / threadsPerBlock2.x, screensize/3+1);//doit etre bien divisible;

	if(HDmode)
	{
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,0, 0);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,1, 0);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,2, 0);

		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,0, 1);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,1, 1);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,2, 1);

		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,0, 2);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,1, 2);
		GPURecolorizeKernelHD<<<numBlocks2,threadsPerBlock2>>>(normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,devPtr,2, 2);
	}
	else
	{
		GPUColorKernelLauncher(numBlocks2,threadsPerBlock2,D2,getTab_d(),devPtr);
	}
}



/*
void CudaRecolorize(int screensize,Coord D2,Coord* tab_d,uchar3 *devPtr)
{
dim3 threadsPerBlock2(thPerBlock/3);
dim3 numBlocks2(screensize / threadsPerBlock2.x, screensize/3+1);//doit etre bien divisible;

//Coord *normales;
//float *doubleTab;
//size_t size2 = screensize*screensize* sizeof(Coord);//pas propre
//size_t size4 = screensize*screensize* sizeof(float);//pas propre
//cudaMalloc((void**)&normales, size2);
//cudaMalloc((void**)&doubleTab, size4);

GPUColorKernelLauncherHD(numBlocks2,threadsPerBlock2,D2,tab_d,devPtr,normales,doubleTab);

//cudaFree(doubleTab);
//cudaFree(normales);
}
*/

void GPUColorKernelLauncher(dim3 numBlocks2,dim3 threadsPerBlock2,Coord D2,Coord* tab_d,uchar3 *devPtr)
{
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,0);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,0);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,0);

	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,1);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,1);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,1);

	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,2);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,2);
	GPUColorKernel<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,2);
}

void GPUColorKernelLauncherHD(dim3 numBlocks2,dim3 threadsPerBlock2,Coord D2,Coord* tab_d,uchar3 *devPtr, Coord* normales,float* doubleTab)
{
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,0,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,0,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,0,normales,doubleTab);

	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,1,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,1,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,1,normales,doubleTab);

	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,0,2,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,1,2,normales,doubleTab);
	GPUColorKernelHD<<<numBlocks2,threadsPerBlock2>>>(D2,tab_d,devPtr,2,2,normales,doubleTab);
}

__global__ void GPUaccessKernel(Coord* test, int i, Coord* tab) 
{
	*test = tab[i];
}

void CPUaccessGPU(Coord* test, int i, Coord* tab)
{
	GPUaccessKernel<<<1,1>>>(test,i,tab);
}

//creer fonctions membres h�rit�es pour ces 2 et les equivalents en Mandelbulb...
void MandelboxView::GPUComputeKernel(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelboxKernelHD<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelboxView::GPUComputeKernelLauncherHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelboxKernelHD2<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelboxKernelHD2<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelboxKernelHD2<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelboxKernelHD2<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelboxKernelHD2<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUComputeKernel(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	(this->*GPUfunction)(numBlocks,threadsPerBlock,NearDist,H,V,D,pos,tab_d);
}

void MandelbulbView::GPUComputeKernelLauncherHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	(this->*GPUfunctionHD)(numBlocks,threadsPerBlock,NearDist,horizontales_d,verticales_d,directions_d,tab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5);
}

void MandelbulbView::GPUFunctionHuit(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelHuit<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionSept(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelSept<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionCinq(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelCinq<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionQuatre(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelQuatre<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionDeux(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelDeux<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionGenerique(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)
{
	GPUMandelbulbKernelGenerique<<<numBlocks,threadsPerBlock>>>(NearDist,H, V, D, pos, tab_d);
}

void MandelbulbView::GPUFunctionHuitHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelHuitHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelHuitHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelHuitHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelHuitHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelHuitHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUFunctionSeptHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelSeptHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelSeptHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelSeptHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelSeptHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelSeptHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUFunctionCinqHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelCinqHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelCinqHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelCinqHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelCinqHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelCinqHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUFunctionQuatreHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelQuatreHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelQuatreHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelQuatreHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelQuatreHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelQuatreHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUFunctionDeuxHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelDeuxHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelDeuxHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelDeuxHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelDeuxHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelDeuxHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void MandelbulbView::GPUFunctionGeneriqueHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)
{
	GPUMandelbulbKernelGeneriqueHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1,0,0);
	GPUMandelbulbKernelGeneriqueHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d2,1,0);
	GPUMandelbulbKernelGeneriqueHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d3,-1,0);
	GPUMandelbulbKernelGeneriqueHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d4,0,1);
	GPUMandelbulbKernelGeneriqueHD<<<numBlocks,threadsPerBlock>>>(NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d5,0,-1);
}

void FractalView::GPUCompute(uchar3 *devPtr)
{
	Coord H = pov->getHorizontalRef();
	Coord V = pov->getVerticalRef();
	Coord D = pov->getDepthRef();
	Coord pos = pov->getPosition();
	//Coord *tab_d = getTab_d();

	int screensize = pov->getScreensize();
	float NearDist = (float)pov->getNearDist();

	dim3 threadsPerBlock(thPerBlock);//pas plus de 512 thread par bloc sur la 8800
	dim3 numBlocks(screensize/thPerBlock, screensize/pointsPerThread);//doit etre bien divisible;

	dim3 threadsPerBlock2(thPerBlock/3);
	dim3 numBlocks2(screensize / threadsPerBlock2.x, screensize/3+1);//doit etre bien divisible;

	GPUComputeKernel(numBlocks,threadsPerBlock,NearDist,H, V, D, pos, tab_d);

	cudaError_t err = cudaGetLastError();
	if( cudaSuccess != err) 
	{
		exit(EXIT_FAILURE); 
	}

	if(HDmode)
	{
		//on doit stocker les normales en plus 
		GPUColorKernelLauncherHD(numBlocks2,threadsPerBlock2,D,tab_d,devPtr,normales_d,doubleTab_d);
		//On va maintenant generer pour chaque point de notre image (sauf les infinis)
		//une carte des 3vecteurs reflechis par la surface.

		initia<<<numBlocks,threadsPerBlock>>>(pos,tab_d,normales_d,horizontales_d,verticales_d,directions_d,H,V);

		//on relance avec comme position tab_d[i]
		//comme direction test1[i], horizontale test2[i] et comme verticale test3[i]

		GPUComputeKernelLauncherHD(numBlocks,threadsPerBlock,NearDist,horizontales_d, verticales_d, directions_d, tab_d, tab_2d1, tab_2d2, tab_2d3, tab_2d4, tab_2d5);

		GPUColorSettingKernelLauncher(numBlocks2,threadsPerBlock2,normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr);
		GPUColorSettingKernelLauncher2(numBlocks2,threadsPerBlock2,normales_d,normales2_d,doubleTab_d,doubleTab2_d,ColorTab_d,tab_2d1,tab_2d2,tab_2d3,tab_2d4,tab_2d5,D,devPtr);
	}
	else
	{
		GPUColorKernelLauncher(numBlocks2,threadsPerBlock2,D,tab_d,devPtr);
	}

	cudaThreadSynchronize();
}

__device__ int getIncrementMandelbulbGenerique(float NearDist, const Coord &P, float &distance) 
{
	double x0 = P.get_x()/10.0;
	double y0 = P.get_y()/10.0;
	double z0 = P.get_z()/10.0;

	double R =0.0;

	double Rq;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double theta;
	double phi;

	double R2;

	while(incr < INCRLIMIT)
	{
		R2 = x*x +y*y + z*z;
		R = sqrt(R2);

		if(R>L2D)
		{
			break;
		}


		Rq = powf(R,S-1);//R^(q-1)

		DEfactor = S*Rq*DEfactor +1.0;

		Rq*=R;//R^(q)

		theta = acos(z/R);
		phi = atan2(y,x);

		theta *= S;
		phi *= S;


		x = Rq * sin(theta)*cos(phi) +x0;
		y = Rq * sin(theta)*sin(phi)+y0;	
		z = Rq * cos(theta)+z0;

		incr++;
	}

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}

__device__ int getIncrementMandelbulbDeux(float NearDist, const Coord &P, float &distance) 
{
	double x0 = P.get_x()/10.0;
	double y0 = P.get_y()/10.0;
	double z0 = P.get_z()/10.0;

	double R =0.0;

	double R2;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2,y2,z2;

	double tmp;

	while(incr <INCRLIMIT)
	{
		R2=x*x +y*y + z*z;
		R = sqrt(R2);

		if(R2>L2D)
		{
			break;
		}

		DEfactor = 2*R*DEfactor +1.0;


		x2 = x*x;	
		y2 = y*y;
		z2 = z*z;

		tmp = 1/sqrt(x2+y2);

		y = 4* x*y*z*tmp+y0;
		x= -2 * z *(y2*(y2+z2)-x2*(x2+z2))*tmp/R2+x0;

		z = z2-y2-x2+z0;

		incr++;
	}

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}

__device__ int getIncrementMandelbulbQuatre(float NearDist, const Coord &P, float &distance) 
{
	double x0 = P.get_x()/10.0;
	double y0 = P.get_y()/10.0;
	double z0 = P.get_z()/10.0;

	double R =0.0;

	double Rq;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;
	double z2;

	double k4,k3,k2,k1;

	double R2;
	while(incr < INCRLIMIT)
	{
		R2 =x*x +y*y + z*z;
		R = sqrt(R2);

		if(R2>L2D)
		{
			break;
		}


		Rq = R2*R;
		DEfactor = 4*Rq*DEfactor +1.0;


		x2 = x*x;
		y2 = y*y;
		z2 = z*z;

		k1=x2+y2;
		k4 =-4*(k1-z2)*z/pow((double)k1,1.5);
		k3 =x2-y2;
		k2=2*x*y;

		y = 2*k3*k2*k4+y0;	
		x = (k3+k2)*(k3-k2)*k4+x0;			
		z = k1*(k1-6*z2)+z2*z2+z0;

		incr++;
	}

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}

__device__ int getIncrementMandelbulbCinq(float NearDist, const Coord &P, float &distance) 
{
	double x0 = P.get_x()/10.0;
	double y0 = P.get_y()/10.0;
	double z0 = P.get_z()/10.0;

	double R2 =0.0;

	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;
	double z2;

	double k6,k5,k4,k3,k2,k1;

	while(incr < INCRLIMIT)
	{
		R2 = x*x +y*y + z*z;

		if(R2>L2D)
		{
			break;
		}

		DEfactor = 5*R2*R2*DEfactor +1.0;

		x2 = x*x;
		y2 = y*y;
		z2 = z*z;

		k4=x2+y2;
		k6=k4*k4;

		k1 = k6+5*z2*(z2-2*k4);
		k2 = 4*R2*(k4-z2);
		k3 = k6+4*y2*(y2-3*x2);
		k5=k1/k6;

		x = x*k3*k5 +x0;
		y = y*(k3+4*k4*(x2-y2))*k5+y0;	
		z = z*(k1+k2)+z0;

		incr++;
	}

	double R = sqrt(R2);

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}

__device__ int getIncrementMandelbulbSept(float NearDist, const Coord &P, float &distance) 
{
	double x0 = P.get_x()/10.0;
	double y0 = P.get_y()/10.0;
	double z0 = P.get_z()/10.0;

	double R2 =0.0;


	int incr = 0;

	double DEfactor = 1.0;

	double x = x0;
	double y = y0;
	double z = z0;

	double x2;
	double y2;  
	double z2; 

	double k1,k2,k3,k4;

	while(incr < INCRLIMIT)
	{
		R2 = x*x +y*y + z*z;

		if(R2>L2D)
		{
			break;
		}

		DEfactor = 7*R2*R2*R2*DEfactor +1.0;

		x2=x*x;
		y2=y*y;
		z2=z*z;



		k2 =x2+y2;
		k3 = k2*k2;
		k4= k3*k2;
		k1 = 7*z2*((k2*5-z2)*z2-k3*3)/k4+1;

		x = x*(7*y2*(3*x2*x2+y2*(y2-5*x2))-x2*x2*x2)*k1+x0;
		y = y*(y2*y2*y2-7*x2*(3*y2*y2+(x2-5*y2)*x2))*k1+y0;	
		z = z*(((z2-21*k2)*z2+35*k3)*z2-7*k4)+z0;




		incr++;
	}

	double R = sqrt(R2);

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}

__device__ int getIncrementMandelbulbHuit(float NearDist, const Coord &P, float &distance) 
{
	float x0 = P.get_x()/10.0;
	float y0 = P.get_y()/10.0;
	float z0 = P.get_z()/10.0;

	/*float theta;
	float phi;*/
	float R =0.0;

	float R8;
	float R2;

	int incr = 0;

	float DEfactor = 1.0;

	float x = x0;
	float y = y0;
	float z = z0;

	float x2;
	float x4;
	float y2; 
	float y4; 
	float z2; 
	float z4; 

	float k3;
	float k2; 
	float k1;
	float k4;

	while(incr < INCRLIMIT)
	{
		R2 = x*x +y*y + z*z;
		R = sqrt(R2);

		if(R2>L2D)
		{
			break;
		}

		R8 = R2*R2;//R4 pour le moment
		DEfactor = 8*R8*R2*R*DEfactor +1.0;

		R8 = R8*R8;//R8 maintenant

		x2 = x*x; 
		x4 = x2*x2; 
		y2 = y*y; 
		y4 = y2*y2;
		z2 = z*z; 
		z4 = z2*z2;

		k3 = y2 + x2;

		k2 = k3*k3;
		k2 = k2*k2*k2*k3; // k3^7
		k2 = 1/sqrt(k2);
		k1 = z4 +k3*(k3 - 6.0*z2);
		k4 = z*(k3 - z2);

		y = 64.0*y*x*(y2-x2)*k4*(y4-6.0*y2*x2+x4)*k1*k2+y0;  
		x = -8.0*k4*k1*k2*(y4*y4 - 28.0*y2*x2*(y4 + x4) + (70.0*y4 + x4)*x4)+x0;
		z = -16.0*k3*k4*k4 + k1*k1 +z0;

		incr++;
	}

	distance = 10*0.5 * R * log(R)/DEfactor;

	if(distance< NearDist)
	{
		incr = INCRLIMIT;
	}

	return incr;	
}
#endif
