#include "DoubleInfoSet.h"
#include <QLocale>
#include "WindowConst.h"
#include "WindowConst2.h"

DoubleInfoSet::DoubleInfoSet()
{

	int lim = 1000000;//devrait suffir

	infoText.setMinimum(-lim);
	infoText.setMaximum(lim);
	infoText.setDecimals(322);

	QLocale locale(QLocale::English);
	locale.setNumberOptions(QLocale::OmitGroupSeparator);
	setLocale(locale);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoSetColor));

	nameLabel.setFixedSize(100, 20);

	layout.addWidget(&nameLabel);
	layout.addWidget(&infoText);

	setMinimumSize(220, 26);
	setMaximumSize(220, 26);

	layout.setContentsMargins(10, 0, 10, 0);

	layout.setSpacing(10);

	setLayout(&layout);
}

void DoubleInfoSet::set_text(std::string name)
{
	nameLabel.setText(name.c_str());
}

double DoubleInfoSet::value()
{
	return infoText.value();
}

void DoubleInfoSet::set(double value)
{
	infoText.setValue(value);
}
