#ifndef COLOR_H
#define COLOR_H

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#else
#define __device__ 
#define __host__ 
#endif//USE_CUDA

class Color
{
public:

	int r;
	int g;
	int b;

	__host__ __device__ Color(int r, int g, int b);
	Color();
};

#endif//COLOR_H
