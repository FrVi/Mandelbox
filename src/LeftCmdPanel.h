#ifndef LEFTCMDPANEL_H
#define LEFTCMDPANEL_H

#include "CommandButton.h"

#include <QGridLayout>
#include <QCheckBox>

class LeftCmdPanel : public QWidget
{
public:
	LeftCmdPanel(PointOfView *pov);

private:
	CommandButton bouton1, bouton2, bouton3, bouton4, bouton5, bouton6;

	QCheckBox caseCocher;
	QGridLayout layout;
	Up up;
	Down down;
	Right right;
	Left left;
	Back back;
	Front front;
	Switch mvtSwitch;
};

#endif\\LEFTCMDPANEL_H
