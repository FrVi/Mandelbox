#ifndef MENU_H
#define MENU_H

#include <QMenuBar>

class Window;

class Menu : public QMenuBar
{
	Q_OBJECT

public:
	Menu(Window *window);
	virtual ~Menu();

	private slots:
	//ExampleWindow* window;
	void Save();
	void Load();
	void GenerateImage();
	void LoadVideo();
	void SaveVideo();
	void ResizeScreen();
	void FractalType();
	void AboutThis();
	void AboutUs();

private:
	Window *window;
	QMenu FileMenu;
	QAction Save_Menu;
	QAction Load_Menu; //LoadMenu est un define d'une autre lib
	QMenu ImageMenu;
	QAction GenerateImageMenu;
	QAction LoadVideoPointsMenu;
	QAction SaveVideoPointsMenu;
	QMenu OptionsMenu;
	QAction ResizeScrenMenu;
	QAction FractalTypeMenu;
	QMenu InformationMenu;
	QAction AboutUsMenu;
	QAction AboutThisMenu;
};


#endif//MENU_H
