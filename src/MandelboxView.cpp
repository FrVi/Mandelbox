#include "MandelboxView.h"	
#include "PointOfView.h"	
#include "Matrix.h"
#include "KernelConst.h"//pour les nouvelles couleurs

MandelboxView::MandelboxView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon) :FractalView(pov, screen, perfMon)
{
}

//rien besoin de faire pour le Mandelbox
void MandelboxView::define()
{

}

int MandelboxView::getIncrement(const Coord &P, Coord &P2, int i)
{
	double R2;
	P2 = P;
	double DEfactor = s;
	int incr = 0;
	while (incr < incrLimit) // Tant que la limite d'incrementation n'est pas atteinte
	{
		P2.bendThis();
		R2 = P2.norm2();
		if (R2 < mR2)
		{
			P2 *= fR2mR2;
			DEfactor *= fR2mR2;
		}
		else if (R2 < fR2)
		{
			P2 *= fR2 / R2;
			DEfactor *= fR2 / R2;
		}
		//P2 = P2*s +P
		P2 *= s;
		P2 += P;
		DEfactor *= s;
		R2 = P2.norm2();
		if (R2 > L*L)
		{
			distance[i] = P.norm() / abs(DEfactor);
			if (distance[i] <= pov->getNearDist())
				incr = incrLimit;
			return incr;
		}
		incr++;
	}
	distance[i] = P.norm() / abs(DEfactor);
	return incr;
}
