#ifndef INFPANEL_H
#define INFPANEL_H

#include <QHBoxLayout>

#include "RightCmdPanel.h"
#include "RightSCmdPanel.h"
#include "LeftCmdPanel.h"

class InfPanel : public 	QWidget
{
public:
	InfPanel(PointOfView *pov);

private:
	RightCmdPanel rightCmdPanel;
	RightSCmdPanel rightSCmdPanel;
	LeftCmdPanel leftCmdPanel;
	QHBoxLayout layout;
};

#endif//INFPANEL_H

