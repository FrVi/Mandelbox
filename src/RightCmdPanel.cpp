#include "RightCmdPanel.h"
#include "KernelConst.h"
#include "WindowConst.h"
#include "WindowConst2.h"

RightCmdPanel::RightCmdPanel(PointOfView *pov) : bouton1("images/ttop.png"), bouton2("images/tleft.png"), bouton3("images/tright.png"), bouton4("images/tbottom.png"), caseCocher("X5"), turnUp(pov, &R_x), turnDown(pov, &R_x_inv), turnRight(pov, &R_y_inv), turnLeft(pov, &R_y), rotSwitch(pov, factRotation)
{
	setMinimumSize(150, 150);
	setMaximumSize(150, 150);
	layout.setContentsMargins(0, 0, 0, 0);

	setAutoFillBackground(true);
	setPalette(QPalette(CenterPanelColor));
	layout.setSpacing(0);
	layout.addWidget(&caseCocher, 0, 0);
	layout.addWidget(&bouton1, 0, 1);
	layout.addWidget(&bouton2, 1, 0);
	layout.addWidget(&bouton3, 1, 2);
	layout.addWidget(&bouton4, 2, 1);

	setLayout(&layout);

	QObject::connect(&bouton1, SIGNAL(clicked()), &turnUp, SLOT(actionPerformed()));
	QObject::connect(&bouton2, SIGNAL(clicked()), &turnLeft, SLOT(actionPerformed()));
	QObject::connect(&bouton3, SIGNAL(clicked()), &turnRight, SLOT(actionPerformed()));
	QObject::connect(&bouton4, SIGNAL(clicked()), &turnDown, SLOT(actionPerformed()));
	QObject::connect(&caseCocher, SIGNAL(toggled(bool)), &rotSwitch, SLOT(actionPerformed()));


	bouton1.setShortcut(QKeySequence(TurnUp));
	bouton2.setShortcut(QKeySequence(TurnLeft));
	bouton3.setShortcut(QKeySequence(TurnRight));
	bouton4.setShortcut(QKeySequence(TurnDown));
}
