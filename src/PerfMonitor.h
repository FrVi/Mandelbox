#ifndef PERFMONITOR_H
#define PERFMONITOR_H

#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>

class PerfMonitor : public QWidget
{
	Q_OBJECT

public:
	PerfMonitor();
	void setLastTime(int time);

private:

	QLabel computeLabel;
	QHBoxLayout layout;
};

#endif\\PERFMONITOR_H
