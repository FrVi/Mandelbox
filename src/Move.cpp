#include "Move.h"
#include "Matrix3.h"
#include "PointOfView.h"
#include "ComponentStack.h"
#include "Window.h"
#include "Perfmonitor.h"

Turn::Turn(PointOfView *pov, const Matrix3 *M)
{
	this->M = M;
	this->pov = pov;
}

//	Definit le comportement de Turn. 
void Turn::actionPerformed()
{
	pov->rot(M);
	pov->update();
}

Left::Left(PointOfView *pov)
{
	this->pov = pov;
}

void Left::actionPerformed()
{
	pov->left();
	pov->update();
}

Right::Right(PointOfView *pov)
{
	this->pov = pov;
}

void Right::actionPerformed()
{
	pov->right();
	pov->update();
}

Up::Up(PointOfView *pov)
{
	this->pov = pov;
}

void Up::actionPerformed()
{
	pov->up();
	pov->update();
}

Front::Front(PointOfView *pov)
{
	this->pov = pov;
}

void Front::actionPerformed()
{
	pov->front();
	pov->update();
}

Down::Down(PointOfView *pov)
{
	this->pov = pov;
}

void Down::actionPerformed()
{
	pov->down();
	pov->update();
}

Back::Back(PointOfView *pov)
{
	this->pov = pov;
}

void Back::actionPerformed()
{
	pov->back();
	pov->update();
}

Switch::Switch(PointOfView *pov, int factor)
{
	this->pov = pov;
	this->factor = factor;
}

void Switch::actionPerformed()
{
	pov->switchFactor(factor);
}

SwitchRotation::SwitchRotation(PointOfView *pov, int factor)
{
	this->pov = pov;
	this->factor = factor;
}

void SwitchRotation::actionPerformed()
{
	pov->switchFactorRotation(factor);
}

Zoom::Zoom(PointOfView *pov, double factor)
{
	this->pov = pov;
	this->factor = factor;
}

void Zoom::actionPerformed()
{
	pov->zoom(factor);
	pov->update();
}

Add::Add(Window *window)
{
	this->window = window;
}

void Add::actionPerformed()
{
	printf("pas fini");
}

Compute::Compute(ComponentStack *pile)
{
	this->pile = pile;
}

void Compute::actionPerformed()
{
	printf("pas fini");
}

GenerateImage2::GenerateImage2(Window *window)
{
	this->window = window;
}

void GenerateImage2::actionPerformed()
{
	printf("pas fini");
}

Reset2::Reset2(Window *window)
{
	this->window = window;
}

void Reset2::actionPerformed()
{
	printf("pas fini");
}

Reset::Reset(PointOfView *pov)
{
	this->pov = pov;
}

void Reset::actionPerformed()
{
	pov->set();
	pov->update();
}

Delete::Delete(Window *window)
{
	this->window = window;
}

void Delete::actionPerformed()
{
	printf("pas fini");
}
