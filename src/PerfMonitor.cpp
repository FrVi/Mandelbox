#include "PerfMonitor.h"
#include "KernelConst.h"

#include "WindowConst.h"
#include "WindowConst2.h"
#include <sstream>

PerfMonitor::PerfMonitor() : computeLabel("Last computing time : 757 ms")
{
	setMinimumSize(422, 56);
	setMaximumSize(422, 56);
	layout.setContentsMargins(10, 0, 10, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(background_color));

	layout.addWidget(&computeLabel);

	setLayout(&layout);
}


void PerfMonitor::setLastTime(int time)
{
	std::string Str = "Last computing time : ";
	std::stringstream out;
	out << time;
	Str.append(out.str());
	Str.append(" ms");
	computeLabel.setText(QString(Str.c_str()));
	computeLabel.repaint();//force la mise a jour de l'affichage du champ de texte
}
