#include "InfPanel4.h"
#include "WindowConst.h"
#include "WindowConst2.h"
InfPanel4::InfPanel4(FractalView* fview) :colorPanel(fview)
{
	setMinimumSize(windows_x_ini, 300);
	setMaximumSize(windows_x_ini, 300);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	layout.addWidget(&colorPanel, Qt::AlignCenter);

	setLayout(&layout);
}

void InfPanel4::resetColors()
{
	colorPanel.resetColors();
}



