#ifndef RIGHTCMDPANEL_H
#define RIGHTCMDPANEL_H

#include "CommandButton.h"

#include <QGridLayout>
#include <QCheckBox>

class RightCmdPanel : public QWidget
{
public:
	RightCmdPanel(PointOfView *pov);

private:
	CommandButton bouton1, bouton2, bouton3, bouton4;
	QCheckBox caseCocher;
	QGridLayout layout;
	Turn turnUp;
	Turn turnDown;
	Turn turnRight;
	Turn turnLeft;
	SwitchRotation rotSwitch;
};

#endif//RIGHTCMDPANEL_H
