#ifndef __GLOBALS_H
#define __GLOBALS_H

//Error handling for cuda functions.
 // Reference: http://developer.nvidia.com/cuda-example-introduction-Generique-purpose-gpu-programming

static void HandleError( cudaError_t err,const char *file,int line ) 
{
    if (err != cudaSuccess)
	{
        printf( "%s (%d) in %s at line %d\n", cudaGetErrorString( err ), err, file, line );
        exit( EXIT_FAILURE );
    }
}

// macro for error handling cuda functions
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

#endif

