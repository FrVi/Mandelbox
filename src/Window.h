#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include "PointOfView.h"
#include "Screen.h"
#include "MandelboxView.h"
#include "MandelbulbView.h"

#include "TopMenu.h"
#include "Onglets.h"
#include "Menu.h"

#include <QVBoxLayout>
#include "ComponentStack.h"

#include "FractalThread.h"

class Window : public QWidget
{
	Q_OBJECT
public:
	void refresh();
	Window();
	~Window();

	void setCoord(int i, const Coord& val);
	void charger_config(QByteArray ba);
	void afficher_config(QByteArray ba);
	void setMandelbox();
	void setMandelbulb();
	void setScreensize(int valeur);
	void afficher_pile(FILE* fichier);
	void pile_setConfig(int i);
	void add_pile();
	void load_pile(FILE* fichier);
	void delete_pile();
	void reset_pile();
	int pile_getSize();
	void pile_set_time(int newTime);
	void compute_pile();
	void ScreenWriteImage(std::string name, std::string ext);
	Coord getCoord(int i);
	void init();
private:
	Semaphore semaphore;
	void calcul_pile(int i);
	TopMenu topMenu;
	PointOfView pov;
	Screen screen;
	FractalView *fview;
	QVBoxLayout layout;
	Onglets *onglets;
	Menu menu;
	ComponentStack pile;
};

#endif//WINDOW_H
