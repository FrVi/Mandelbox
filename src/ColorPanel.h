#ifndef COLORPANEL_H
#define COLORPANEL_H

#include <QGridLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>

#include <QGraphicsView>
#include <QColorDialog>
#include <QLabel>

class FractalView;

class ColorPanel : public QWidget
{
	Q_OBJECT

public:
	ColorPanel(FractalView* fview);
	void resetColors();

	public slots:
	void SelectComponent0(bool cond);
	void SelectComponent1(bool cond);
	void SelectComponent2(bool cond);
	void colorize();
	void colorChange();
private:
	QPushButton bouton1, bouton2;
	QGroupBox groupe;
	QRadioButton boutonRadio1, boutonRadio2, boutonRadio3;
	QGridLayout layout;
	QLabel color;
	QGridLayout box;
	FractalView* fview;
};

#endif//COLORPANEL_H
