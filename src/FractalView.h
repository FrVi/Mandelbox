#ifndef FRACTALVIEW_H
#define FRACTALVIEW_H

#ifdef USE_CUDA
#include <vector_types.h>
#endif//USE_CUDA

class Screen;
class PointOfView;
class Coord;
class PerfMonitor;
class Color;
class FractalThread;
class Semaphore;
class ThreadPanel;

class FractalView 
{
private:
	Semaphore* semaphore;

	PerfMonitor *perfMon;	//Moniteur de performances du programme.
#ifdef USE_CUDA
	void GPUCompute(uchar3 *devPtr);
#endif//

protected:

	Coord* tab_d;
	Coord(FractalView::*funPtr) (int);

	double correction;	//correction de l'estimateur de distance.

	bool HDmode;
	int colorA;
	int colorB;
	int colorC;

	int colorD;
	int colorE;
	int colorF;

	int colorG;
	int colorH;
	int colorI;

	void setColorNorm(int x, int y, const Coord &X, double c);
	void setColorNorm(const Coord &X, double c, Color &Y);
	void setColorNorm(int x, int y, const Coord &X, double c, const Color &Y);
	void setColorNorm(int x, int y, const Coord &X, double c, const Color &Y0, const Color &Y1, const Color &Y2, const Color &Y3, const Color &Y4);

	Screen *screen; 	//a�cran sur lequel on affiche la vue de fractale.

	void setColor();

	double* distance;	//Tableau contenant les distances courantes pour chaque thread.
	PointOfView *pov;	//Point de vue associe a� la vue de fractale.
	int incrLimit;		//Limite de profondeur de calcul.

	double s;
	double minRadius;
	double fixedRadius;
	double L;
	double fR2;
	double mR2;
	double fR2mR2;
#ifdef USE_CUDA
	void recolorize(uchar3 *devPtr);
	void recolorizeCPU(uchar3 *devPtr);
	void recolorizeGPU(uchar3 *devPtr);
#else
	void recolorizeCPU();
#endif//USE_CUDA

	void setRecolorizeMode(bool GPUmode);

	ThreadPanel *threadPanel;

	Coord* tab_h;

	Coord *horizontales;
	Coord *verticales;
	Coord *directions;
	Coord *positions;
	Coord *tab1;
	Coord *tab2;
	Coord *tab3;
	Coord *tab4;
	Coord *tab5;

	Color *ColorTab;

	Coord *normales;
	Coord *normales2;

	double *doubleTab;
	double *doubleTab2;

	Coord *normales_d;
	Coord *horizontales_d;
	Coord *verticales_d;
	Coord *directions_d;
	Coord *tab_2d1;
	Coord *tab_2d2;
	Coord *tab_2d3;
	Coord *tab_2d4;
	Coord *tab_2d5;
	Coord *normales2_d;
	Color *ColorTab_d;
	float *doubleTab_d;
	float *doubleTab2_d;

public:
	ThreadPanel* getThreadPanel();
	void setMode(bool gpu_mode);
	Coord getCoord(int i);
	Coord getCPUCoord(int i);
	Coord getGPUCoord(int i);


	void setCoord(int i, const Coord& val);
	void init();

	void setGPU_CPUmode(bool GPUmode);
	void reflectWork(int i, Coord* tab, int a, int b);
	void resizeGL();

	void setThreadPanel(ThreadPanel *threadPanel);

	void setSemaphore(Semaphore* semaphore);
	void relNormalSemaEnd(int idX);
	void acqNormalSemaStart(int idX);
	void relHighSemaEnd(int idX);
	void acqHighSemaStart(int idX);
	void transfertGPU_CPU(bool GPUmode);

	FractalView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon);
	~FractalView();
	virtual void define() = 0;

	void colorize();
	void initColors();

	Coord* getTab_d();

	void updateGL();
	void setColors(int a, int b, int c, int d, int e, int f, int g, int h, int i);
	void setColors(int i, int val);
	void colorize_function();

	int getColors(int i);

	void update();//Relance le calcul et actualise l'affichage de la vue sur l'ecran.*/

	int getThread_numbers();

	void compute();

	void iniTabGPU();
	void iniTabCPU();

	void deleteTabGPU();
	void deleteTabCPU();


	void GPU_const_init();
	int getIncrLimit();
	void setCorrection(double correction);
	double getCorrection();

	void redraw();
#ifdef USE_CUDA
	void gpu_work();
#endif//

	void highQualityWork(int i);
	void work(int i);
	void lowQuality();
	void highQuality();
	void setDrawQuality(bool low);

	void (FractalView::*drawQualityPtr) ();
#ifdef USE_CUDA
	void (FractalView::*RedrawModePtr) (uchar3 *);
#else
	void (FractalView::*RedrawModePtr) ();
#endif//USE_CUDA

	void closeThreads();
	void setS(double s);
	double getS();

	void setMinRadius(double  minRadius);
	double getMinRadius();
	void setFixedRadius(double fixedRadius);
	double getFixedRadius();
	void setL(double L);
	double getL();
	void setIncrLimit(int incrLimit);

	void cpu_work();

	void work();
	void setWorkMode(bool gpu_mode);

#ifdef USE_CUDA
	virtual void GPUComputeKernel(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d)=0;
	virtual void GPUComputeKernelLauncherHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5)=0;
#endif//USE_CUDA

protected:
	virtual int getIncrement(const Coord &P, Coord &P2, int i) = 0;

	void (FractalView::*workModePtr) (void);

};

#endif//FRACTALVIEW_H
