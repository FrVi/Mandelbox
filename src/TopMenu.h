#ifndef TOPMENU_H
#define TOPMENU_H

#include <QHBoxLayout>

#include "PerfMonitor.h"

class TopMenu : public 	QWidget
{
public:
	TopMenu();
	PerfMonitor* getPerfMonitor();
private:
	PerfMonitor perfMonitor;
	QHBoxLayout layout;
};

#endif //TOPMENU_H
