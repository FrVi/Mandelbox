#include "Window.h"

#include "WindowConst.h"
#include "WindowConst2.h"

#include <iostream>
#include <fstream>
#include <QMessageBox>

double Log2(double n)
{
	return log(n) / log(2.0);
}

void Window::setCoord(int i, const Coord& val)
{
	fview->setCoord(i, val);
}

void Window::refresh()
{
	fview->update();
	onglets->update();
	screen.updateGL();
}

Coord Window::getCoord(int i)
{
	return fview->getCoord(i);
}

Window::~Window()
{
	delete onglets;
	delete fview;
}

void Window::init()
{
	fview->init();
}

void Window::setScreensize(int valeur)
{
	int n = (int)Log2(valeur);
	valeur = (1 << n);

	pov.setScreensize(valeur);

	fview->deleteTabCPU();
	fview->deleteTabGPU();
	fview->iniTabCPU();
	fview->iniTabGPU();

	//		fview->GPU_const_init();

	screen.resize22(valeur, valeur);

	refresh();
}

Window::Window() :pov(this), screen(&pov, this), menu(this), pile(&pov)
{
	screen.resize(pov.getScreensize(), pov.getScreensize());
	fview = new MandelboxView(&pov, &screen, topMenu.getPerfMonitor());
	semaphore.set(fview);

	pile.setFractalView(fview);
	onglets = new Onglets(&pov, fview, this);

	setFixedSize(windows_x_ini, windows_y_ini);

	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(background_color));

	layout.addWidget(&menu, 0, Qt::AlignTop);
	layout.addWidget(&topMenu, 0, Qt::AlignTop);
	layout.addWidget(&screen, 1, Qt::AlignCenter);
	layout.addWidget(onglets, 0, Qt::AlignBottom);

	setLayout(&layout);
}

void Window::setMandelbox()
{
	ThreadPanel* threadPanel = fview->getThreadPanel();
	delete fview;
	fview = new MandelboxView(&pov, &screen, topMenu.getPerfMonitor());
	fview->init();
	pile.reset();
	pile.setFractalView(fview);
	onglets->setFractalView(fview);
	semaphore.destruct();
	semaphore.set(fview);
	pov.set();
	threadPanel->set(fview);
	refresh();
}

void Window::setMandelbulb()
{
	ThreadPanel* threadPanel = fview->getThreadPanel();
	delete fview;
	fview = new MandelbulbView(&pov, &screen, topMenu.getPerfMonitor());
	fview->init();
	pile.reset();
	pile.setFractalView(fview);
	onglets->setFractalView(fview);
	semaphore.destruct();
	semaphore.set(fview);
	pov.set();
	threadPanel->set(fview);
	refresh();
}

void Window::afficher_config(QByteArray ba)
{
	const char *c_str = ba.data();
	std::ofstream flux(c_str); //On essaye d'ouvrir le fichier

	if (flux)    //On teste si tout est OK.
	{
		Coord temp;
		double temp2;
		int temp3;

		//gaffe a la precision
		flux.precision(12);
		temp = pov.getPosition();			flux << "Coord@Position" << std::endl << "\t" << temp.get_x() << " " << temp.get_y() << " " << temp.get_z() << std::endl;
		temp = pov.getHorizontalRef();		flux << "Coord@Reference Horizontale" << std::endl << "\t" << temp.get_x() << " " << temp.get_y() << " " << temp.get_z() << std::endl;
		temp = pov.getVerticalRef();		flux << "Coord@Reference Verticale" << std::endl << "\t" << temp.get_x() << " " << temp.get_y() << " " << temp.get_z() << std::endl;
		temp = pov.getDepthRef();			flux << "Coord@Reference de Profondeur" << std::endl << "\t" << temp.get_x() << " " << temp.get_y() << " " << temp.get_z() << std::endl;

		temp2 = pov.getZoomFactor();		flux << "Double@Facteur de zoom" << std::endl << "\t" << temp2 << std::endl;
		temp2 = pov.getLimitDist();			flux << "Double@Limite de vision" << std::endl << "\t" << temp2 << std::endl;
		temp2 = pov.getNearDist();			flux << "Double@Limite de distinction" << std::endl << "\t" << temp2 << std::endl;
		temp2 = fview->getS();				flux << "Double@Parametre S" << std::endl << "\t" << temp2 << std::endl;
		temp2 = fview->getMinRadius();		flux << "Double@Rayon minimal" << std::endl << "\t" << temp2 << std::endl;
		temp2 = fview->getFixedRadius();	flux << "Double@Rayon fixe" << std::endl << "\t" << temp2 << std::endl;
		temp2 = fview->getL();				flux << "Double@Limite d'observation" << std::endl << "\t" << temp2 << std::endl;
		temp3 = fview->getIncrLimit();		flux << "Int@Increment limite" << std::endl << "\t" << temp2 << std::endl;
		temp2 = fview->getCorrection();		flux << "Double@Correction" << std::endl << "\t" << temp2 << std::endl;

		flux << "Color@ColorA" << std::endl << "\t" << fview->getColors(0) << " " << fview->getColors(1) << " " << fview->getColors(2) << std::endl;
		flux << "Color@ColorB" << std::endl << "\t" << fview->getColors(3) << " " << fview->getColors(4) << " " << fview->getColors(5) << std::endl;
		flux << "Color@ColorC" << std::endl << "\t" << fview->getColors(6) << " " << fview->getColors(7) << " " << fview->getColors(8) << std::endl;

		flux.close(); //On referme le fichier (fait tout seul a la fin du bloc en vrai...)
	}
	else
	{
		QMessageBox::critical(NULL, "Error", "File Error...");
	}
}

int check_error_bits(std::ifstream* f)
{
	int stop = 0;
	if (f->eof()) {
		perror("stream eofbit. error state");
		// EOF after std::getline() is not the criterion to stop processing
		// data: In case there is data between the last delimiter and EOF,
		// getline() extracts it and sets the eofbit.
		stop = 0;
	}
	if (f->fail()) {
		perror("stream failbit (or badbit). error state");
		stop = 1;
	}
	if (f->bad()) {
		perror("stream badbit. error state");
		stop = 1;
	}
	return stop;
}


void Window::charger_config(QByteArray ba)
{
	const char *c_str = ba.data();
	std::ifstream flux(c_str); //On essaye d'ouvrir le fichier

	if (flux)    //On teste si tout est OK.
	{

		int i = 0;
		std::string ligne;
		while (std::getline(flux, ligne))    //Tant qu'on n'est pas a la fin, on lit
		{
			i++;
		}

		if (i != 32)
		{
			QMessageBox::critical(NULL, "Error", "File Error : wrong number of lines");
			return;
		}


		flux.clear();//la fin de fichier positionne un bit d'erreur, on l'enleve
		flux.seekg(0, std::ios_base::beg);//retour au debut
		double temp, temp2, temp3;
		int temp4, temp5, temp6;
		Coord Temp;

		flux.precision(12);

		//pour le faire proprement, donner un pov.set(int,Coord) et faire des boucles

		std::getline(flux, ligne);//on peut tester que la ligne de texte est bien "Coord@Position"
		flux >> temp >> temp2 >> temp3;//on pourrait surcharger l'operateur pour Coord (fonction ami)
		flux.seekg(2, std::ios_base::cur);//on retourne a la ligne
		Temp.set(temp, temp2, temp3);
		pov.setPosition(Temp);
		std::getline(flux, ligne);
		flux >> temp >> temp2 >> temp3;
		flux.seekg(2, std::ios_base::cur);
		Temp.set(temp, temp2, temp3);
		pov.setHorizontalRef(Temp);
		std::getline(flux, ligne);
		flux >> temp >> temp2 >> temp3;
		flux.seekg(2, std::ios_base::cur);
		Temp.set(temp, temp2, temp3);
		pov.setVerticalRef(Temp);
		std::getline(flux, ligne);
		flux >> temp >> temp2 >> temp3;
		flux.seekg(2, std::ios_base::cur);
		Temp.set(temp, temp2, temp3);
		pov.setDepthRef(Temp);

		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		pov.setZoomFactor(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		pov.setLimitDist(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		pov.setNearDist(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		fview->setS(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		fview->setMinRadius(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		fview->setFixedRadius(temp);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		fview->setL(temp);
		std::getline(flux, ligne);
		flux >> temp4;
		flux.seekg(2, std::ios_base::cur);
		fview->setIncrLimit(temp4);
		std::getline(flux, ligne);
		flux >> temp;
		flux.seekg(2, std::ios_base::cur);
		fview->setCorrection(temp);

		std::getline(flux, ligne);
		flux >> temp4 >> temp5 >> temp6;
		flux.seekg(2, std::ios_base::cur);
		fview->setColors(0, temp4);
		fview->setColors(1, temp5);
		fview->setColors(2, temp6);

		std::getline(flux, ligne);
		flux >> temp4 >> temp5 >> temp6;
		flux.seekg(2, std::ios_base::cur);
		fview->setColors(3, temp4);
		fview->setColors(4, temp5);
		fview->setColors(5, temp6);

		std::getline(flux, ligne);
		flux >> temp4 >> temp5 >> temp6;
		flux.seekg(2, std::ios_base::cur);
		fview->setColors(6, temp4);
		fview->setColors(7, temp5);
		fview->setColors(8, temp6);

		flux.close(); //On referme le fichier (fait tout seul a la fin du bloc en vrai...)

		refresh();
	}
	else
	{
		QMessageBox::critical(NULL, "Error", "File Error...");
	}
}


void Window::afficher_pile(FILE* fichier)
{
	pile.afficher(fichier);
}

void Window::pile_setConfig(int i)
{
	pile.setConfig(i);
}

void Window::calcul_pile(int i)//sert ou pas?
{
	pile.set_time(i);
	pile.calcul();
}

void Window::add_pile()
{
	pile.add();
}

void Window::load_pile(FILE* fichier)
{
	Coord coords[4];
	double doubles[8];
	int ints[10];
	int size = -1;
	int i, j;

	double temp1, temp2, temp3;


	fscanf(fichier, "%ld\n", &size);//taille du fichier
	for (j = 0; j < size; j++)
	{
		for (i = 0; i < 4; i++)
		{
			fscanf(fichier, "%lf %lf %lf ", &temp1, &temp2, &temp3);
			coords[i].set_x(temp1);
			coords[i].set_y(temp2);
			coords[i].set_z(temp3);
		}

		for (i = 0; i < 8; i++)
		{
			fscanf(fichier, "%lf ", &(doubles[i]));
		}

		for (i = 0; i < 9; i++)
		{
			fscanf(fichier, "%ld ", &(ints[i]));
		}
		fscanf(fichier, "%ld\n", &(ints[9]));

		Component C(coords, doubles, ints);

		pile.add(C);
	}
}

void Window::delete_pile()
{
	pile.deleteElement();
}

int Window::pile_getSize()
{
	return pile.getSize();
}

void Window::reset_pile()
{
	pile.reset();
}

void Window::pile_set_time(int newTime)
{
	pile.set_time(newTime);
}

void Window::compute_pile()
{
	pile.calcul();
}

void Window::ScreenWriteImage(std::string name, std::string ext)
{
	screen.writeImage(name, ext);
}
