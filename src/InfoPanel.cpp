#include "InfoPanel.h"

#include "PointOfView.h"
#include "FractalView.h"

#include "WindowConst.h"
#include "WindowConst2.h"

InfoPanel::InfoPanel(PointOfView *pov, FractalView *fview)
{
	setFocusPolicy(Qt::ClickFocus);
	this->pov = pov;
	this->fview = fview;
	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	//set_size_request(840,440);
	coords[0].set_text("Position");
	coords[1].set_text("Reference Horizontale");
	coords[2].set_text("Reference Verticale");
	coords[3].set_text("Reference de profondeur");

	doubles[0].set_text("Facteur de zoom");
	doubles[1].set_text("Limite de vision");
	doubles[2].set_text("Limite de distinction");
	doubles[3].set_text("Parametre S");
	doubles[4].set_text("Rayon minimal");
	doubles[5].set_text("Rayon fixe");
	doubles[6].set_text("Limite d'observation");
	doubles[7].set_text("Increment limite");
	doubles[8].set_text("Correction");

	layout.setContentsMargins(30, 30, 30, 30);

	layout.setHorizontalSpacing(48);

	layout.addWidget(&coords[0], 0, 0);
	layout.addWidget(&coords[1], 1, 0);
	layout.addWidget(&coords[2], 2, 0);
	layout.addWidget(&coords[3], 3, 0);

	layout.addWidget(&doubles[0], 4, 0);
	layout.addWidget(&doubles[1], 5, 0);
	layout.addWidget(&doubles[2], 6, 0);

	layout.addWidget(&doubles[3], 0, 1);
	layout.addWidget(&doubles[4], 1, 1);
	layout.addWidget(&doubles[5], 2, 1);
	layout.addWidget(&doubles[6], 3, 1);
	layout.addWidget(&doubles[7], 4, 1);
	layout.addWidget(&doubles[8], 5, 1);

	setLayout(&layout);

	InfoPanel::setInformations();
}

void InfoPanel::getInformations()
{
	pov->setPosition(coords[0].get_coord());
	pov->setHorizontalRef(coords[1].get_coord());
	pov->setVerticalRef(coords[2].get_coord());
	pov->setDepthRef(coords[3].get_coord());
	pov->setZoomFactor(doubles[0].value());
	pov->setLimitDist(doubles[1].value());
	pov->setNearDist(doubles[2].value());

	fview->setS(doubles[3].value());
	fview->setMinRadius(doubles[4].value());
	fview->setFixedRadius(doubles[5].value());
	fview->setL(doubles[6].value());
	fview->setIncrLimit(doubles[7].value());
	fview->setCorrection(doubles[8].value());

	pov->update();
}

void InfoPanel::setInformations()
{
	coords[0].set(pov->getPosition());
	coords[1].set(pov->getHorizontalRef());
	coords[2].set(pov->getVerticalRef());
	coords[3].set(pov->getDepthRef());
	doubles[0].set(pov->getZoomFactor());
	doubles[1].set(pov->getLimitDist());
	doubles[2].set(pov->getNearDist());
	doubles[3].set(fview->getS());
	doubles[4].set(fview->getMinRadius());
	doubles[5].set(fview->getFixedRadius());
	doubles[6].set(fview->getL());
	doubles[7].set(fview->getIncrLimit());
	doubles[8].set(fview->getCorrection());
}


void InfoPanel::keyPressEvent(QKeyEvent *e)
{
	switch (e->key())
	{
	case Qt::Key_Return:
		getInformations();
		break;

	default:
		QWidget::keyPressEvent(e);
		break;
	}
}

void InfoPanel::setFractalView(FractalView* fview)
{
	this->fview = fview;
}
