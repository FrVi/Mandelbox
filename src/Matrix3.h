#ifndef MATRIX3_H
#define MATRIX3_H

#include "Coord.h"

class Matrix3
{
private:
	Coord *tab;

public:
	Matrix3(double a, double b, double c, double d, double e, double f, double g, double h, double i);
	Matrix3();
	Matrix3(const Matrix3 &M);
	Matrix3(const Coord &A, const Coord &B, const Coord &C);
	Matrix3 operator-() const;

	~Matrix3();

	void operator=(const Matrix3 &M);
	void oppThis();
	void set(int i, int j, double val);
	void afficher() const;

	double get(int i, int j) const;
	double determinant() const;

	friend Coord operator*(const Matrix3 &M, const Coord &X);
	friend Matrix3 operator*(const Matrix3 &M, const Matrix3 &A);
};

Coord operator*(const Matrix3 &M, const Coord &X);
Matrix3 operator*(const Matrix3 &M, const Matrix3 &A);

#endif//MATRIX3_H

