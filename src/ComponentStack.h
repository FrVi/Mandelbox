#ifndef COMPONENTSTACK_H
#define COMPONENTSTACK_H

#include <vector>
#include "Coord.h"
#include "Component.h"
#include "PointOfView.h"
#include "Matrix.h"

class FractalView;

class ComponentStack
{
private:
	std::vector< std::vector<Coord*> > coords;
	std::vector< std::vector<Coord*> > coords_a;
	std::vector< std::vector<Coord*> > coords_b;
	std::vector< std::vector<double> > doubles_a;
	std::vector< std::vector<double> > doubles_b;

	std::vector< std::vector<double> > ints_a;
	std::vector< std::vector<double> > ints_b;

	Matrix A;

	std::vector< std::vector<double> > doubles;

	std::vector< std::vector<int> > ints;

	PointOfView *pov;
	FractalView *fview;

	int size;

	std::vector<Component*> resultat;

	int duree;

	void reset_partiel();
	void initCoord(int j);
	void initdouble(int j);
	void initInt(int j);
	Component compute(double t);
	double getdouble(double t, int i);
	int getInt(double t, int i);
	Coord compute(double t, int i);	//t dans [0;1]


public:
	void afficher(FILE* fichier);
	void set_time(int duree);
	void setFractalView(FractalView *fview);
	ComponentStack(PointOfView *pov/*,Window w*/);
	~ComponentStack();
	void add();
	void deleteElement();
	void calcul();
	int getSize();
	void setConfig(int i);
	void add(const Component &C);
	void reset();
};

#endif//COMPONENTSTACK_H
