#ifndef MANDELBOXVIEW_H
#define MANDELBOXVIEW_H

#include "FractalView.h"
#include "Coord.h"

class MandelboxView : public FractalView
{

public:
	MandelboxView(PointOfView *pov, Screen *screen, PerfMonitor *perfMon);
	void define();//rien besoin de faire pour le Mandelbox
	Coord q;
#ifdef USE_CUDA
	void GPUComputeKernel(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord H,Coord V,Coord D,Coord pos,Coord* tab_d);
	void GPUComputeKernelLauncherHD(dim3 numBlocks,dim3 threadsPerBlock,float NearDist,Coord* horizontales_d,Coord* verticales_d,Coord* directions_d,Coord* tab_d,Coord* tab_2d1,Coord* tab_2d2,Coord* tab_2d3,Coord* tab_2d4,Coord* tab_2d5);
#endif//

protected:
	//void test();
	int getIncrement(const Coord &P, Coord &P2, int i);//Applique des iterations du processus de la Mandelbox sur un point donne jusqu'aux conditions limites.

};

#endif//MANDELBOXVIEW_H
