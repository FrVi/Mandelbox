#ifndef INFPANEL2_H
#define INFPANEL2_H

#include <QHBoxLayout>

#include "InfoPanel.h"

class PointOfView;

class InfPanel2 : public QWidget
{
public:
	InfPanel2(PointOfView* pov, FractalView* fview);
	void update();
	void setFractalView(FractalView* fview);
private:
	InfoPanel infoPanel;
	QHBoxLayout layout;
};

#endif//INFPANEL2_H
