#ifndef MOVE_H
#define MOVE_H

#include <QObject>

class Matrix3;
class PointOfView;
class ComponentStack;
class Window;
class PerfMonitor;

class Turn : public  QObject
{
	Q_OBJECT

public:
	Turn(PointOfView *pov, const Matrix3 *M);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
	const Matrix3* M;
};


class Left : public  QObject
{
	Q_OBJECT
public:
	Left(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Right : public  QObject
{
	Q_OBJECT
public:
	Right(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Up : public  QObject
{
	Q_OBJECT
public:
	Up(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Front : public  QObject
{
	Q_OBJECT
public:
	Front(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Down : public  QObject
{
	Q_OBJECT
public:
	Down(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Back : public  QObject
{
	Q_OBJECT
public:
	Back(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
};

class Switch : public  QObject
{
	Q_OBJECT
public:
	Switch(PointOfView *P, int factor);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
	int factor;
};

class SwitchRotation : public  QObject
{
	Q_OBJECT
public:
	SwitchRotation(PointOfView *P, int factor);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
	int factor;
};

class Zoom : public  QObject
{
	Q_OBJECT
public:
	Zoom(PointOfView *P, double factor);

	public slots:
	void actionPerformed();

private:
	PointOfView* pov;
	double factor;
};

class Add : public  QObject
{
	Q_OBJECT
public:
	Add(Window *window);

	public slots:
	void actionPerformed();

private:
	Window *window;
};

class Compute : public  QObject
{
	Q_OBJECT
public:
	Compute(ComponentStack *pile);

	public slots:
	void actionPerformed();

private:
	ComponentStack *pile;
};

class GenerateImage2 : public  QObject
{
	Q_OBJECT
public:
	GenerateImage2(Window *window);

	public slots:
	void actionPerformed();

private:
	Window *window;
};

class Reset2 : public  QObject
{
	Q_OBJECT
public:
	Reset2(Window *window);

	public slots:
	void actionPerformed();

private:
	Window *window;
};

class Reset : public  QObject
{
	Q_OBJECT
public:
	Reset(PointOfView *pov);

	public slots:
	void actionPerformed();

private:
	PointOfView *pov;
};

class Delete : public  QObject
{
	Q_OBJECT
public:
	Delete(Window *window);

	public slots:
	void actionPerformed();

private:
	Window *window;
};

#endif//MOVE_H
