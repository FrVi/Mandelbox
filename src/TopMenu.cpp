#include "TopMenu.h"
#include "WindowConst.h"
#include "WindowConst2.h"

TopMenu::TopMenu()
{
	setMinimumSize(windows_x_ini, 56);
	setMaximumSize(windows_x_ini, 56);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(background_color));

	layout.addWidget(&perfMonitor);

	setLayout(&layout);
}

PerfMonitor* TopMenu::getPerfMonitor()
{
	return &perfMonitor;
}
