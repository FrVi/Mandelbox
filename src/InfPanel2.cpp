#include "InfPanel2.h"
#include "WindowConst.h"
#include "WindowConst2.h"

InfPanel2::InfPanel2(PointOfView* pov, FractalView* fview) :infoPanel(pov, fview)
{
	setMinimumSize(windows_x_ini, 300);
	setMaximumSize(windows_x_ini, 300);
	layout.setContentsMargins(0, 0, 0, 0);
	layout.setSpacing(0);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoPanelColor));

	layout.addWidget(&infoPanel);

	setLayout(&layout);
}

void InfPanel2::update()
{
	infoPanel.setInformations();
}

void InfPanel2::setFractalView(FractalView* fview)
{
	infoPanel.setFractalView(fview);
}

