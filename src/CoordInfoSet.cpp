#include "CoordInfoSet.h"
#include <QLocale>
#include "WindowConst.h"
#include "WindowConst2.h"

CoordInfoSet::CoordInfoSet()
{
	QLocale locale(QLocale::English);
	locale.setNumberOptions(QLocale::OmitGroupSeparator);
	setLocale(locale);

	nameLabel.setMinimumSize(150, 20);
	infoText1.setMaximumSize(106, 20);
	infoText2.setMaximumSize(106, 20);
	infoText3.setMaximumSize(106, 20);

	setAutoFillBackground(true);
	setPalette(QPalette(InfoSetColor));

	int lim = 100000;//devrait suffir

	infoText1.setMinimum(-lim);
	infoText1.setMaximum(lim);
	infoText1.setDecimals(322);

	infoText2.setMinimum(-lim);
	infoText2.setMaximum(lim);
	infoText2.setDecimals(322);

	infoText3.setMinimum(-lim);
	infoText3.setMaximum(lim);
	infoText3.setDecimals(322);

	layout.addWidget(&nameLabel);
	layout.addWidget(&infoText1);
	layout.addWidget(&infoText2);
	layout.addWidget(&infoText3);

	setLayout(&layout);

	setFixedSize(500, 26);

	layout.setContentsMargins(10, 0, 10, 0);

	layout.setSpacing(10);
}

void CoordInfoSet::set_text(std::string name)
{
	nameLabel.setText(name.c_str());
}

Coord CoordInfoSet::get_coord()
{
	Coord tmp;
	tmp.set(infoText1.value(), infoText2.value(), infoText3.value());
	return tmp;
}

void CoordInfoSet::set(Coord C)
{
	infoText1.setValue(C.get_x());
	infoText2.setValue(C.get_y());
	infoText3.setValue(C.get_z());
}
