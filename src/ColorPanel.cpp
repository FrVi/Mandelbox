#include "ColorPanel.h"
#include "WindowConst.h"
#include "WindowConst2.h"

#include "FractalView.h"

ColorPanel::ColorPanel(FractalView* fview) :bouton1("recolorize"), bouton2("Choose Color"), boutonRadio1("X Color"), boutonRadio2("Y Color"), boutonRadio3("Z Color")
{
	this->fview = fview;

	setMinimumSize(500, 100);
	setMaximumSize(500, 100);

	bouton1.setMinimumSize(94, 26);
	bouton1.setMaximumSize(94, 26);

	bouton2.setMinimumSize(120, 26);
	bouton2.setMaximumSize(120, 26);

	color.setMinimumSize(10, 80);
	color.setMaximumSize(10, 80);

	color.setAutoFillBackground(true);

	QColor col(fview->getColors(0), fview->getColors(1), fview->getColors(2));
	color.setPalette(QPalette(col));

	setAutoFillBackground(true);
	setPalette(QPalette(CenterPanelColor));

	layout.setSpacing(0);

	box.addWidget(&boutonRadio1, 0, 0);
	box.addWidget(&boutonRadio2, 1, 0);
	box.addWidget(&boutonRadio3, 2, 0);

	boutonRadio1.setChecked(true);

	groupe.setLayout(&box);

	groupe.setMaximumSize(94, 80);
	groupe.setMinimumSize(94, 80);

	layout.addWidget(&bouton1, 0, 0);
	layout.addWidget(&bouton2, 0, 1);
	layout.addWidget(&groupe, 0, 2);
	layout.addWidget(&color, 0, 3);

	setLayout(&layout);

	connect(&boutonRadio1, SIGNAL(toggled(bool)), this, SLOT(SelectComponent0(bool)));
	connect(&boutonRadio2, SIGNAL(toggled(bool)), this, SLOT(SelectComponent1(bool)));
	connect(&boutonRadio3, SIGNAL(toggled(bool)), this, SLOT(SelectComponent2(bool)));

	connect(&bouton1, SIGNAL(clicked()), this, SLOT(colorize()));
	connect(&bouton2, SIGNAL(clicked()), this, SLOT(colorChange()));
}

void ColorPanel::resetColors()
{
	QColor col((int)colorX_x, (int)colorX_y, (int)colorX_z);
	color.setPalette(QPalette(col));
}

void ColorPanel::SelectComponent0(bool cond)
{
	if (cond)
	{
		QColor col(fview->getColors(0), fview->getColors(1), fview->getColors(2));
		color.setPalette(QPalette(col));
	}
}

void ColorPanel::SelectComponent1(bool cond)
{
	if (cond)
	{
		QColor col(fview->getColors(3), fview->getColors(4), fview->getColors(5));
		color.setPalette(QPalette(col));
	}
}

void ColorPanel::SelectComponent2(bool cond)
{
	if (cond)
	{
		QColor col(fview->getColors(6), fview->getColors(7), fview->getColors(8));
		color.setPalette(QPalette(col));
	}
}

void ColorPanel::colorize()
{
	fview->colorize();
}

void ColorPanel::colorChange()
{
	if (boutonRadio1.isChecked())
	{
		QColor tmp(fview->getColors(0), fview->getColors(1), fview->getColors(2));
		QColor col = QColorDialog::getColor(tmp, this);

		if (col.isValid())
		{
			color.setPalette(QPalette(col));

			fview->setColors(0, col.red());
			fview->setColors(1, col.green());
			fview->setColors(2, col.blue());
		}
	}
	else if (boutonRadio2.isChecked())
	{
		QColor tmp(fview->getColors(3), fview->getColors(4), fview->getColors(5));
		QColor col = QColorDialog::getColor(tmp, this);

		if (col.isValid())
		{
			color.setPalette(QPalette(col));
			fview->setColors(3, col.red());
			fview->setColors(4, col.green());
			fview->setColors(5, col.blue());
		}
	}
	else
	{
		QColor tmp(fview->getColors(6), fview->getColors(7), fview->getColors(8));
		QColor col = QColorDialog::getColor(tmp, this);

		if (col.isValid())
		{
			color.setPalette(QPalette(col));
			fview->setColors(6, col.red());
			fview->setColors(7, col.green());
			fview->setColors(8, col.blue());
		}
	}
}
